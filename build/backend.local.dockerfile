FROM golang:alpine AS builder

WORKDIR /build

ADD go.mod .

COPY . .

RUN go mod download && go mod vendor

RUN go build -o main cmd/sso/main.go

RUN go test ./...

CMD ["./main"]

FROM alpine

WORKDIR /build

COPY /config/local.yaml config/conf.yaml
COPY --from=builder /build/main /build/main

ENV CONFIG_PATH=config/conf.yaml

CMD ["./main"]