package http

import (
	"context"
	"fmt"
	"log/slog"
	"net/http"
	"time"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	_ "github.com/swaggo/gin-swagger"
	ginSwagger "github.com/swaggo/gin-swagger"

	apidocs "sso/api"
	"sso/internal/config"
	"sso/internal/pkg/logger/sl"
	srv "sso/internal/services"
	"sso/internal/services/auth"
	apphttp "sso/internal/transport/http"
)

type httpAPI struct {
	action     *apphttp.ActionServer
	object     *apphttp.ObjectServer
	permission *apphttp.PermissionServer
	resource   *apphttp.ResourceServer
	role       *apphttp.RoleServer
	user       *apphttp.UserServer
	auth       *apphttp.AuthServer
}

type Services struct {
	Auth       *auth.Auth
	Token      *auth.Tokens
	Action     *srv.Action
	Object     *srv.Object
	Permission *srv.Permission
	Profile    *srv.Profile
	Resource   *srv.Resource
	Role       *srv.Role
	Session    *srv.Session
	User       *srv.User
}

// App структура для запуска HTTP сервера
type App struct {
	log         *slog.Logger
	environment config.EnvType
	router      *gin.Engine
	server      *http.Server
	services    *Services
	api         *httpAPI
}

func newHTTPApi(services *Services) *httpAPI {
	return &httpAPI{
		action:     apphttp.NewActionServer(services.Action),
		object:     apphttp.NewObjectServer(services.Object),
		permission: apphttp.NewPermissionServer(services.Permission),
		resource:   apphttp.NewResourceServer(services.Resource),
		role:       apphttp.NewRoleServer(services.Role),
		user:       apphttp.NewUserServer(services.User),
		auth:       apphttp.NewAuthServer(services.Auth),
	}
}

// New создает экземпляр структуры App
func New(log *slog.Logger, address string, env config.EnvType, services *Services) *App {
	switch env {
	case config.EnvLocal, config.EnvDev:
		gin.SetMode(gin.DebugMode)
	case config.EnvProd:
		gin.SetMode(gin.ReleaseMode)
	}

	router := gin.Default()

	server := &http.Server{
		Addr:    address,
		Handler: router,
	}

	return &App{
		log:         log,
		environment: env,
		router:      router,
		server:      server,
		services:    services,
		api:         newHTTPApi(services),
	}
}

// Init app api handlers
//
//	@contact.name				API Support
//	@contact.url				http://www.swagger.io/support
//	@contact.email				support@swagger.io
//
//	@license.name				Apache 2.0
//	@license.url				http://www.apache.org/licenses/LICENSE-2.0.html
//
//	@securityDefinitions.basic	Bearer
func (a *App) Init() {
	const op = "app.http.init"
	//var originsCORS = []string{"127.0.0.1", "*.my-awesome-domain.ru"}

	apidocs.SwaggerInfo.Title = "NextSSO Swagger API"
	apidocs.SwaggerInfo.Description = "Documentation of NextSSO Swagger API"
	apidocs.SwaggerInfo.Version = "1.0"
	apidocs.SwaggerInfo.Host = fmt.Sprintf("127.0.0.1%s", a.server.Addr)
	//apidocs.SwaggerInfo.BasePath = "/v2"
	apidocs.SwaggerInfo.Schemes = []string{"http", "https"}

	a.log.With("op", op).Debug("set CORS")

	a.router.Use(cors.New(cors.Config{
		//AllowOrigins:    originsCORS,
		AllowAllOrigins: true,
		AllowOriginFunc: func(origin string) bool {
			return true
		},
	}))

	a.log.With("op", op).Info("registering services routs")

	adminGroup := a.router.Group("/admin")
	{
		a.api.action.RegisterAdminRouts(adminGroup)
		a.api.object.RegisterAdminRouts(adminGroup)
		a.api.permission.RegisterAdminRouts(adminGroup)
		a.api.resource.RegisterAdminRouts(adminGroup)
		a.api.role.RegisterAdminRouts(adminGroup)
		a.api.user.RegisterAdminRouts(adminGroup)
	}

	publicGroup := a.router.Group("/api")
	{
		a.api.auth.RegisterPublicRouts(publicGroup)
		a.api.user.RegisterPublicRouts(publicGroup)
	}

	{
		a.api.auth.RegisterStaticRouts(a.router)
	}

	if a.environment != config.EnvProd {
		a.router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	}

	a.log.With("op", op).Debug("routs successful registered")
}

// Run
// Запуск HTTP сервера.
func (a *App) Run() error {
	const op = "app.http.Run"
	log := a.log.With(
		slog.String("op", op),
	)

	a.Init()

	log.With("address", a.server.Addr).Info("server is running...")

	if err := a.server.ListenAndServe(); err != nil {
		log.Error("failed to run HTTP server", sl.Err(err))
		return err
	}

	return nil
}

// MustRun
// Обязательный запуск HTTP сервера.
// В случае возникновения ошибки при запуске вызвывает панику (panic).
func (a *App) MustRun() {
	if err := a.Run(); err != nil {
		panic(err)
	}
}

// Stop
// Остановка HTTP сервера
func (a *App) Stop() {
	const op = "app.http.Stop"
	log := a.log.With(
		slog.String("op", op),
	)

	log.Info("stopping grpc server")

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	if err := a.server.Shutdown(ctx); err != nil {
		log.Error("failed to stop server", sl.Err(err))
	}
}
