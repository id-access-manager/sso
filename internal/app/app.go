package app

import (
	"gorm.io/gorm"
	"log/slog"
	"sso/internal/app/http"
	"sso/internal/config"
	"sso/internal/database"
	"sso/internal/models"
	tokenmodels "sso/internal/models/token"
	"sso/internal/pkg/logger/sl"
	srv "sso/internal/services"
	"sso/internal/services/auth"
	tokensrv "sso/internal/services/token"
	logic2 "sso/internal/storages"
	token2 "sso/internal/storages/token"
	"time"
)

type tokenStorage struct {
	access   *token2.Access
	identity *token2.Identity
	refresh  *token2.Refresh
}

type storages struct {
	token      *tokenStorage
	action     *logic2.Action
	object     *logic2.Object
	permission *logic2.Permission
	profile    *logic2.Profile
	resource   *logic2.Resource
	role       *logic2.Role
	session    *logic2.Session
	user       *logic2.User
}

// An App is an implementation for run main app
type App struct {
	HTTP     *http.App
	driver   database.Driver
	database *gorm.DB
}

// New is a constructor of the App struct
func New(log *slog.Logger, cfg *config.Config) *App {
	const op = "app.app.New"
	var appModels = []any{
		&tokenmodels.Access{},
		&tokenmodels.Identity{},
		&tokenmodels.Refresh{},
		&models.Action{},
		&models.Object{},
		&models.Permission{},
		//&models.Profile{},
		&models.Resource{},
		&models.Role{},
		&models.Session{},
		&models.User{},
	}

	appLog := log.With(slog.String("op", op))
	appLog.Info("creating new app")

	appLog.Debug("connecting to database ...")
	driver := database.New(log, cfg.Database.Name, cfg.Database.DSN, cfg.Env)
	db := driver.MustConnect()
	appLog.Debug("successfully connected to database")

	appLog.Debug("applying database migrations ...")
	if err := db.AutoMigrate(appModels...); err != nil {
		appLog.Error("failed to migrate database", sl.Err(err))
		return nil
	}
	appLog.Debug("successfully migrate database")

	appLog.Debug("creating storages")
	storages := createStorages(log, db)
	appLog.Debug("successfully created storages")

	appLog.Debug("creating services")
	services := createServices(log, storages, cfg.TimeToLive.Session, cfg.TimeToLive.Token.Access,
		cfg.TimeToLive.Token.Refresh, cfg.TimeToLive.Token.Identity)
	appLog.Debug("successfully created storages")

	appLog.Info("creating server")
	svr := http.New(log, cfg.HTTP.GetAddress(), cfg.Env, services)
	appLog.Debug("successfully created server")

	appLog.Info("successfully created app")

	return &App{
		HTTP:     svr,
		driver:   driver,
		database: db,
	}
}

// Stop application
func (a *App) Stop() {
	a.driver.Stop(a.database)
	a.HTTP.Stop()
}

func createStorages(log *slog.Logger, db *gorm.DB) *storages {
	return &storages{
		token: &tokenStorage{
			access:   token2.NewAccess(log, db),
			identity: token2.NewIdentity(log, db),
			refresh:  token2.NewRefresh(log, db),
		},
		action:     logic2.NewAction(log, db),
		object:     logic2.NewObject(log, db),
		permission: logic2.NewPermission(log, db),
		profile:    logic2.NewProfile(log, db),
		resource:   logic2.NewResource(log, db),
		role:       logic2.NewRole(log, db),
		session:    logic2.NewSession(log, db),
		user:       logic2.NewUser(log, db),
	}
}

func createServices(log *slog.Logger, storages *storages, sessionTTL time.Duration, accessTokenTTL time.Duration,
	refreshTokenTTL time.Duration, identityTokenTTL time.Duration) *http.Services {
	actionSrv := srv.NewAction(log, storages.action)
	objectSrv := srv.NewObject(log, storages.object)
	permissionSrv := srv.NewPermission(log, storages.permission, actionSrv, objectSrv)

	profileSrv := srv.NewProfile(log, storages.profile)
	roleSrv := srv.NewRole(log, storages.role, permissionSrv)
	resourceSrv := srv.NewResource(log, storages.resource, roleSrv)

	sessionSrv := srv.NewSession(log, storages.session, sessionTTL)
	userSrv := srv.NewUser(log, storages.user)
	tokensSrv := auth.NewTokens(log,
		tokensrv.NewAccess(log, storages.token.access, accessTokenTTL),
		tokensrv.NewRefresh(log, storages.token.refresh, refreshTokenTTL),
		tokensrv.NewIdentity(log, storages.token.identity, identityTokenTTL),
	)
	authSrv := auth.New(log, sessionSrv, userSrv, tokensSrv)

	return &http.Services{
		Auth:       authSrv,
		Token:      tokensSrv,
		Action:     actionSrv,
		Object:     objectSrv,
		Permission: permissionSrv,
		Profile:    profileSrv,
		Resource:   resourceSrv,
		Role:       roleSrv,
		Session:    sessionSrv,
		User:       userSrv,
	}
}
