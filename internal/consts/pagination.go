package consts

const (
	DefaultPageSize = 20
	DefaultPageNum  = 1
	DefaultPageSort = "Id desc"
)
