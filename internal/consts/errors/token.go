package errors

import "errors"

var (
	ErrTokenInvalid          = errors.New("token invalid")
	ErrTokenExpired          = errors.New("token expired")
	ErrTokenSignatureInvalid = errors.New("token signature invalid")
	ErrTokenNotFound         = errors.New("token not found")
)
