package errors

import "errors"

var (
	ErrInvalidID = errors.New("id is invalid")
)

var (
	ErrIDRequired          = errors.New("id is required")
	ErrNameRequired        = errors.New("name is required")
	ErrDescriptionRequired = errors.New("description is requires")
)

var (
	ErrCreateResource  = errors.New("failed to create resource")
	ErrGetResourceList = errors.New("failed to get resource list")
)
