package errors

import "errors"

var (
	ErrProfileNotFound = errors.New("profile not found") // Find profile error (404 mock_services code)
)
