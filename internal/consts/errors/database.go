package errors

import "errors"

var ErrUnknownDriverName = errors.New("unknown driver name")

var (
	ErrResourceNotFound = errors.New("resource not found")
)
