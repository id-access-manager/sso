package errors

import "errors"

var (
	ErrInvalidUsernameOrPassword = errors.New("invalid username or password")
)
