package errors

import "errors"

var (
	ErrActionNotFound    = errors.New("action not found")
	ErrUnknownActionType = errors.New("unknown action type")
)
