package errors

import "errors"

var (
	ErrInvalidIDType      = errors.New("invalid id type")
	ErrInvalidQueryParams = errors.New("invalid query params")
)
