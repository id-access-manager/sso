package utils

import "github.com/gin-gonic/gin"

// NewError example
func NewError(ctx *gin.Context, status int, err error) {
	httpErr := HTTPError{
		Message: err.Error(),
	}
	ctx.JSON(status, httpErr)
}

// HTTPError example
type HTTPError struct {
	Message string `json:"message" example:"status bad request"`
}
