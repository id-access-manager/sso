package jwt

import (
	"errors"
	jwtlib "github.com/golang-jwt/jwt/v5"
	"github.com/google/uuid"
	apperr "sso/internal/consts/errors"
	"sso/internal/models"
	"sso/internal/pkg/jwt/claims"
	"time"
)

// NewRefreshToken generate AccessToken
func NewRefreshToken(user models.User, authAt time.Time, expiredAt time.Time, tokenID uuid.UUID,
	sessionID uuid.UUID) (string, error) {
	token := jwtlib.NewWithClaims(signingMethod, claims.RefreshTokenClaims{
		RegisteredClaims: jwtlib.RegisteredClaims{
			Issuer:    "",
			Audience:  nil, // TODO add token aud
			ExpiresAt: jwtlib.NewNumericDate(expiredAt),
			IssuedAt:  jwtlib.NewNumericDate(authAt),
			ID:        tokenID.String(),
		},
		Session: sessionID.String(),
		UserID:  user.ID.String(),
	})

	tokenString, err := token.SignedString([]byte("qwerty"))
	if err != nil {
		return "", err
	}

	return tokenString, nil
}

// ValidateRefreshToken is verifying access JWT token and get claims
func ValidateRefreshToken(refreshToken string) (*claims.RefreshTokenClaims, error) {
	token, err := jwtlib.ParseWithClaims(refreshToken, &claims.RefreshTokenClaims{}, func(token *jwtlib.Token) (interface{},
		error) {
		_, ok := token.Method.(*jwtlib.SigningMethodHMAC)
		if !ok {
			return nil, apperr.ErrTokenInvalid
		}
		return token, nil
	})

	switch {
	case errors.Is(err, jwtlib.ErrTokenExpired):
		return nil, apperr.ErrTokenExpired
	case errors.Is(err, jwtlib.ErrSignatureInvalid):
		return nil, apperr.ErrTokenSignatureInvalid
	}

	claims, ok := token.Claims.(*claims.RefreshTokenClaims)

	if !(ok || token.Valid) {
		return nil, apperr.ErrTokenInvalid
	}

	return claims, nil
}
