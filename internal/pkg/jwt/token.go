package jwt

import jwtlib "github.com/golang-jwt/jwt/v5"

var (
	tokenKey      = []byte("qwerty")
	signingMethod = jwtlib.SigningMethodHS256
)
