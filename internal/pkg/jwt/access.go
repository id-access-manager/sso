package jwt

import (
	"errors"
	jwtlib "github.com/golang-jwt/jwt/v5"
	"github.com/google/uuid"
	apperr "sso/internal/consts/errors"
	"sso/internal/models"
	"sso/internal/pkg/jwt/claims"
	"time"
)

// NewAccessToken generate AccessToken
func NewAccessToken(user models.User, authAt time.Time, expiredAt time.Time, tokenID uuid.UUID,
	sessionID uuid.UUID) (string, error) {
	var token = jwtlib.NewWithClaims(signingMethod, claims.AccessTokenClaims{
		RegisteredClaims: jwtlib.RegisteredClaims{
			Issuer:    "",
			Audience:  nil, // TODO add token aud
			ExpiresAt: jwtlib.NewNumericDate(expiredAt),
			IssuedAt:  jwtlib.NewNumericDate(authAt),
			ID:        tokenID.String(),
		},
		Session:         sessionID.String(),
		User:            user.ID.String(),
		ResourcesAccess: nil, // TODO add resources access
	})

	tokenString, err := token.SignedString(tokenKey)
	if err != nil {
		return "", err
	}

	return tokenString, nil
}

// VerifyAccessToken is verifying access JWT token and get claims
func VerifyAccessToken(accessToken string) (*claims.AccessTokenClaims, error) {
	token, err := jwtlib.ParseWithClaims(accessToken, &claims.AccessTokenClaims{}, func(token *jwtlib.Token) (interface{},
		error) {
		_, ok := token.Method.(*jwtlib.SigningMethodHMAC)
		if !ok {
			return nil, apperr.ErrTokenInvalid
		}
		return token, nil
	})

	switch {
	case errors.Is(err, jwtlib.ErrTokenExpired):
		return nil, apperr.ErrTokenExpired
	case errors.Is(err, jwtlib.ErrSignatureInvalid):
		return nil, apperr.ErrTokenSignatureInvalid
	}

	claims, ok := token.Claims.(*claims.AccessTokenClaims)

	if !(ok || token.Valid) {
		return nil, apperr.ErrTokenInvalid
	}

	return claims, nil
}
