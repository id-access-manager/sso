package claims

import jwtlib "github.com/golang-jwt/jwt/v5"

// AccessTokenClaims is a structure od access JWT token
type AccessTokenClaims struct {
	jwtlib.RegisteredClaims
	Session         string              `json:"sid,omitempty"`              // The `sid` (Session id) claims.
	User            string              `json:"uid,omitempty"`              // The `uid` (User id) claims.
	ResourcesAccess jwtlib.ClaimStrings `json:"resources_access,omitempty"` // The `resources_access` claims.
}

// GetSession implements the jwtlib.Claims interfaces.
func (c AccessTokenClaims) GetSession() (string, error) {
	return c.Session, nil
}

// GetUser implements the jwtlib.Claims interfaces.
func (c AccessTokenClaims) GetUser() (string, error) {
	return c.User, nil
}

// GetResources implements the jwtlib.Claims interfaces.
func (c AccessTokenClaims) GetResources() (jwtlib.ClaimStrings, error) {
	return c.ResourcesAccess, nil
}
