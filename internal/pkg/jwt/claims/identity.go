package claims

import jwtlib "github.com/golang-jwt/jwt/v5"

type IdentityTokenClaims struct {
	jwtlib.RegisteredClaims
	Session string `json:"sid,omitempty"` // The `sid` (Session id) claims.
	UserID  string `json:"uid,omitempty"` // The `uid` (UserID id) claims.
}

// GetSession implements the jwtlib.Claims interfaces.
func (c IdentityTokenClaims) GetSession() (string, error) {
	return c.Session, nil
}

// GetUser implements the jwtlib.Claims interfaces.
func (c IdentityTokenClaims) GetUser() (string, error) {
	return c.UserID, nil
}
