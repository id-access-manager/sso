package pg

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestNewMeta(t *testing.T) {
	tests := []struct {
		name          string
		req           Request
		objTotal      int64
		wantPageCount int
	}{
		{
			name: "valid page",
			req: Request{
				Page:     2,
				PageSize: 10,
			},
			objTotal:      2,
			wantPageCount: 1,
		},
		{
			name: "invalid page",
			req: Request{
				Page:     2,
				PageSize: 10,
			},
			objTotal:      0,
			wantPageCount: 0,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			meta := newMeta(0, test.objTotal, &test.req)
			assert.Equal(t, test.wantPageCount, meta.PagesCount)
		})
	}
}

//func TestNewResponse(t *testing.T) {
//	tests := []struct {
//		name       string
//		req Request
//		itemsCount int64
//		totalItems int64
//	}{
//		{
//			name:       "valid page",
//			page:       1,
//			items:      []any{1, 2, 3, 4, 5, 6, 7, 8, 9},
//			totalItems: 20,
//		},
//	}
//
//	for _, test := range tests {
//		t.Run(test.name, func(t *testing.T) {
//			response := NewResponse(test.items, test.totalItems, test.page)
//			assert.Equal(t, int64(len(test.items)), response.Meta.ObjectsCount)
//			assert.Equal(t, int(math.Ceil(float64(test.totalItems)/float64(len(test.items)))), response.Meta.PagesCount)
//		})
//	}
//}
