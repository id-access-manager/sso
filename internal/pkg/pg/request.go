package pg

import (
	"sso/internal/consts"
)

type Request struct {
	PageSize int    `form:"page_size" json:"page_size,omitempty"`
	Page     int    `form:"page" json:"page,omitempty"`
	Sort     string `form:"sort" json:"sort,omitempty"`
}

func NewRequest(pageSize int, page int, sort string) *Request {
	return &Request{
		PageSize: pageSize,
		Page:     page,
		Sort:     sort,
	}
}

func (p *Request) GetOffset() int {
	return (p.GetPage() - 1) * p.GetLimit()
}

func (p *Request) GetLimit() int {
	if p.PageSize == 0 {
		p.PageSize = consts.DefaultPageSize
	}
	return p.PageSize
}

func (p *Request) GetPage() int {
	if p.Page == 0 {
		p.Page = consts.DefaultPageNum
	}
	return p.Page
}

func (p *Request) GetSort() string {
	if p.Sort == "" {
		p.Sort = consts.DefaultPageSort
	}
	return p.Sort
}
