package pg

import (
	"math"
)

type meta struct {
	Page         int   `json:"page,omitempty"`
	PagesCount   int   `json:"pages_count"`
	ObjectsCount int64 `json:"objects_count"`
	ObjectsTotal int64 `json:"objects_total"`
}

type Response struct {
	Meta  meta `json:"meta"`
	Items any  `json:"items"`
}

func newMeta(objCount int64, objTotal int64, req *Request) *meta {
	var pageCount int

	if objTotal == 0 && objCount == 0 {
		pageCount = 0
	} else {
		pageCount = int(math.Ceil(float64(objTotal) / float64(req.GetLimit())))
	}

	return &meta{
		Page:         req.Page,
		PagesCount:   pageCount,
		ObjectsCount: objCount,
		ObjectsTotal: objTotal,
	}
}

func NewResponse(items any, itemsCount int64, totalItems int64, req *Request) *Response {
	return &Response{
		Meta:  *newMeta(itemsCount, totalItems, req),
		Items: items,
	}
}
