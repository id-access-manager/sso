package pg

import (
	"github.com/stretchr/testify/assert"
	"sso/internal/consts"
	"testing"
)

func TestRequest_GetLimit(t *testing.T) {
	tests := []struct {
		name string
		req  Request
	}{
		{
			name: "with limit",
			req: Request{
				PageSize: 10,
			},
		},
		{
			name: "without limit",
			req:  Request{},
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			limit := test.req.GetLimit()
			if test.req.PageSize == 0 {
				assert.Equal(t, consts.DefaultPageSize, limit)
			} else {
				assert.Equal(t, test.req.PageSize, limit)
			}
		})
	}
}

func TestRequest_GetSort(t *testing.T) {
	tests := []struct {
		name   string
		req    Request
		offset int
	}{
		{
			name: "with page",
			req: Request{
				Page: 1,
			},
		},
		{
			name: "without page",
			req:  Request{},
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			page := test.req.GetPage()
			if test.req.Page == 0 {
				assert.Equal(t, consts.DefaultPageNum, page)
			} else {
				assert.Equal(t, test.req.Page, page)
			}
		})
	}
}

func TestRequest_GetOffset(t *testing.T) {
	tests := []struct {
		name   string
		req    Request
		offset int
	}{
		{
			name: "full data",
			req: Request{
				Page:     2,
				PageSize: 10,
			},
			offset: 10,
		},
		{
			name: "without limit",
			req: Request{
				Page: 2,
			},
			offset: 20,
		},
		{
			name: "without page",
			req: Request{
				PageSize: 10,
			},
			offset: 0,
		},
		{
			name:   "default data",
			req:    Request{},
			offset: 0,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			offset := test.req.GetOffset()
			assert.Equal(t, test.offset, offset)
		})
	}
}
