package models

import (
	"github.com/google/uuid"
	"gorm.io/gorm"
	"time"
)

// SQLModel base sql model for declaring database models
type SQLModel struct {
	ID        uuid.UUID `json:"id" gorm:"primaryKey; type:uuid" json:"id"`            // Unique uuid.UUID ID
	CreatedAt time.Time `json:"-" gorm:"default:CURRENT_TIMESTAMP" json:"created_at"` // Date and Time of object create
	UpdatedAt time.Time `json:"-"`                                                    // Date and time of object update
}

// BeforeCreate generate unique row uuid.UUID ID
func (m *SQLModel) BeforeCreate(tx *gorm.DB) error {
	m.ID = uuid.New()
	m.CreatedAt = time.Now()
	return nil
}

func (m *SQLModel) BeforeUpdate(tx *gorm.DB) error {
	m.UpdatedAt = time.Now()
	return nil
}
