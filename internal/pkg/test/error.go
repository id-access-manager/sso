package test

import (
	"encoding/json"
	"github.com/stretchr/testify/assert"
	"net/http/httptest"
	"sso/internal/pkg/utils"
	"testing"
)

func AssertHTTPError(t *testing.T, statusCode int, err error, r *httptest.ResponseRecorder) {
	httpErr := utils.HTTPError{Message: err.Error()}
	jsonErr, _ := json.Marshal(httpErr)

	assert.Equal(t, statusCode, r.Code)
	assert.Equal(t, jsonErr, r.Body.Bytes())
}
