package test

import "github.com/gin-gonic/gin"

func GetRouter() *gin.Engine {
	ginMode := gin.ReleaseMode
	gin.SetMode(ginMode)
	router := gin.New()
	router.Use(gin.Recovery())
	return router
}
