package dto

type Tokens struct {
	Access   string `json:"access"`
	Refresh  string `json:"refresh"`
	Identity string `json:"identity"`
}
