package storages

import (
	"github.com/google/uuid"
	"gorm.io/gorm"
	"log/slog"
	"sso/internal/models"
)

type Role struct {
	log      *slog.Logger
	database *gorm.DB
}

func NewRole(log *slog.Logger, database *gorm.DB) *Role {
	return &Role{
		log:      log,
		database: database,
	}
}

// Create logic

func (r *Role) Create(role *models.Role) error {
	return r.database.Create(role).Error
}

func (r *Role) AddPermissions(role *models.Role, permissions []models.Permission) error {
	return r.database.Model(role).Association("Permissions").Append(permissions)
}

// Getting logic

func (r *Role) GetByID(role *models.Role, id uuid.UUID) error {
	return r.database.First(role, id).Error
}

func (r *Role) GetAll(roles *[]models.Role, limit int, offset int, sort string) error {
	return r.database.Find(&roles).Limit(limit).Offset(offset).Order(sort).Error
}

func (r *Role) GetByIDs(resources *[]models.Role, ids []uuid.UUID) error {
	//const op = "db.resource.Resource.GetByIDs"
	return r.database.Model(resources).Where("id IN (?)", ids).Find(resources).Error
}

func (r *Role) Count(count *int64) error {
	return r.database.Model(&models.Role{}).Count(count).Error
}

// Updating logic

func (r *Role) Update(role *models.Role, data map[string]any) error {
	return r.database.Model(role).Updates(data).Error
}

func (r *Role) ReplacePermissions(role *models.Role, permissions []models.Permission) error {
	return r.database.Model(role).Association("Permissions").Replace(permissions)
}

// Deleting logic

func (r *Role) Delete(role *models.Role) error {
	if err := r.database.Delete(role).Association("Resources").Clear(); err != nil {
		return err
	}

	if err := r.database.Delete(role).Association("Users").Clear(); err != nil {
		return err
	}

	if err := r.database.Delete(role).Association("Permissions").Clear(); err != nil {
		return err
	}

	return r.database.Delete(role).Error
}

func (r *Role) RemovePermissions(role *models.Role, permissions []models.Permission) error {
	return r.database.Model(role).Association("Permissions").Delete(permissions)
}
