package storages

import (
	"github.com/google/uuid"
	"gorm.io/gorm"
	"log/slog"
	"sso/internal/models"
)

type User struct {
	log      *slog.Logger
	database *gorm.DB
}

func NewUser(log *slog.Logger, database *gorm.DB) *User {
	return &User{
		log:      log,
		database: database,
	}
}

func (u *User) Save(user *models.User) error {
	return u.database.Save(user).Error
}

func (u *User) Create(user *models.User) error {
	return u.database.Create(user).Error
}

func (r *Role) AddResources(role *models.Role, resources []models.Resource) error {
	return r.database.Model(role).Association("Resources").Append(resources)
}

func (u *User) Update(user *models.User, data map[string]any) error {
	return u.database.Model(user).Updates(data).Error
}

func (u *User) GetByID(user *models.User, id uuid.UUID) error {
	return u.database.First(user, id).Error
}

func (u *User) GetByEmail(user *models.User, email string) error {
	return u.database.First(user, "email = ?", email).Error
}

func (u *User) GetAll(users *[]models.User, limit int, offset int, sort string) error {
	return u.database.Limit(limit).Offset(offset).Order(sort).Find(users).Error
}

func (u *User) Count(count *int64) error {
	return u.database.Model(&models.User{}).Count(count).Error
}

func (u *User) GetByUsername(user *models.User, username string) error {
	return u.database.First(user, "username = ?", username).Error
}

func (u *User) IsExist(id uuid.UUID) (isExist bool, err error) {
	err = u.database.Model(&models.User{}).Select("count(*) > 0").Where("id = ?", id).Find(&isExist).Error
	return isExist, err
}

func (u *User) Delete(user *models.User) error {
	return u.database.Delete(user).Error
}
