package storages

import (
	"github.com/google/uuid"
	"gorm.io/gorm"
	"log/slog"
	"sso/internal/models"
)

type Object struct {
	log      *slog.Logger
	database *gorm.DB
}

func NewObject(log *slog.Logger, database *gorm.DB) *Object {
	return &Object{
		log:      log,
		database: database,
	}
}

func (o *Object) Insert(obj *models.Object) error {
	//const op = "db.storages.Obj.Insert"
	return o.database.Save(obj).Error
}

func (o *Object) Create(obj *models.Object) error {
	const op = "db.storages.Obj.Create"
	return o.database.Create(obj).Error
}

func (o *Object) Update(obj *models.Object, data map[string]any) error {
	//const op = "db.storages.Obj.Update"
	return o.database.Model(obj).Updates(data).Error
}

func (o *Object) Delete(obj *models.Object) error {
	//const op = "db.mock_storages.Obj.Delete"
	return o.database.Delete(obj).Error
}

func (o *Object) Get(obj *models.Object, id uuid.UUID) error {
	//const op = "db.storages.Obj.Get"
	return o.database.First(obj, "id = ?", id).Error
}

func (o *Object) GetAll(objs *[]models.Object, limit int, offset int, sort string) error {
	//const op = "db.storages.Obj.GetAll"
	return o.database.Limit(limit).Offset(offset).Order(sort).Find(objs).Error
}

func (o *Object) Count(total *int64) error {
	//const op = "db.storages.Obj.Count"
	return o.database.Model(&models.Object{}).Count(total).Error
}
