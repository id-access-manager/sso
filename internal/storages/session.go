package storages

import (
	"github.com/google/uuid"
	"gorm.io/gorm"
	"log/slog"
	"sso/internal/models"
	"time"
)

type Session struct {
	log      *slog.Logger
	database *gorm.DB
}

func NewSession(log *slog.Logger, database *gorm.DB) *Session {
	return &Session{
		log:      log,
		database: database,
	}
}

func (s *Session) Create(session *models.Session) error {
	//const op = "db.mock_storages.Session.Create"
	return s.database.Create(session).Error
}

func (s *Session) GetByID(session *models.Session, id uuid.UUID) error {
	//const op = "db.mock_storages.Session.GetByID"
	return s.database.First(session, id).Error
}

func (s *Session) GetValidByID(session *models.Session, id uuid.UUID) error {
	//const op = "db.mock_storages.Session.GetValidByID"
	return s.database.Where("expired_at > ?", time.Now()).First(session, id).Error
}

func (s *Session) Update(session *models.Session, data map[string]any) error {
	//const op = "db.mock_storages.Session.Update"
	return s.database.Model(session).Updates(data).Error
}

func (s *Session) Delete(session *models.Session) error {
	//const op = "db.mock_storages.Session.Delete"
	return s.database.Delete(session).Error
}
