package storages

import (
	"github.com/google/uuid"
	"gorm.io/gorm"
	"log/slog"
	"sso/internal/models"
)

type Resource struct {
	log      *slog.Logger
	database *gorm.DB
}

func NewResource(log *slog.Logger, database *gorm.DB) *Resource {
	return &Resource{
		log:      log,
		database: database,
	}
}

// Create logic

func (r *Resource) Create(resource *models.Resource) error {
	const op = "db.resource.Resource.Create"
	return r.database.Create(&resource).Error
}

func (r *Resource) AddRoles(resource *models.Resource, roles *[]models.Role) error {
	const op = "db.resource.AddRole"
	return r.database.Model(resource).Association("Roles").Append(roles)
}

// Get logic

func (r *Resource) GetByID(resource *models.Resource, id uuid.UUID) error {
	const op = "db.resource.Resource.GetByID"
	return r.database.First(&resource, "id = ?", id).Error
}

func (r *Resource) GetAll(resources *[]models.Resource, limit int, offset int, sort string) error {
	const op = "db.resource.Resource.GetAll"
	return r.database.Model(resources).Limit(limit).Offset(offset).Order(sort).Find(resources).Error
}

func (r *Resource) GetByIDs(resources *[]models.Resource, ids []uuid.UUID) error {
	const op = "db.resource.Resource.GetByIDs"
	return r.database.Model(resources).Where("id IN (?)", ids).Find(resources).Error
}

func (r *Resource) GetCount(count *int64) error {
	const op = "db.resource.Resource.GetCount"
	return r.database.Model(&models.Resource{}).Count(count).Error
}

func (r *Resource) LoadRoles(resource *models.Resource) error {
	const op = "db.resource.LoadRoles"
	return r.database.Model(resource).Preload("Roles").First(resource).Error
}

// Update logic

func (r *Resource) Update(resource *models.Resource, data map[string]any) error {
	const op = "db.resource.Resource.Update"
	return r.database.Model(resource).Updates(data).Error
}

func (r *Resource) ReplaceRoles(resource *models.Resource, roles *[]models.Role) error {
	const op = "db.resource.ReplaceRoles"
	return r.database.Model(resource).Association("Roles").Replace(roles)
}

// Delete logic

func (r *Resource) Delete(resource *models.Resource) error {
	const op = "db.resource.Resource.Delete"

	if err := r.database.Model(&models.Resource{}).Association("Users").Clear(); err != nil {
		return err
	}

	if err := r.database.Model(&models.Resource{}).Association("Roles").Clear(); err != nil {
		return err
	}

	if err := r.database.Delete(resource).Error; err != nil {
		return err
	}

	return nil
}

func (r *Resource) DeleteRoles(resource *models.Resource, roles *[]models.Role) error {
	const op = "db.resource.DeleteRoles"
	return r.database.Model(resource).Association("Roles").Clear()
}
