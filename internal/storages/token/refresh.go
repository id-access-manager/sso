package token

import (
	"github.com/google/uuid"
	"gorm.io/gorm"
	"log/slog"
	"sso/internal/models/token"
)

type Refresh struct {
	log      *slog.Logger
	database *gorm.DB
}

func NewRefresh(log *slog.Logger, database *gorm.DB) *Refresh {
	return &Refresh{
		log:      log,
		database: database,
	}
}

func (r *Refresh) Save(token *token.Refresh) error {
	return r.database.Save(token).Error
}

func (r *Refresh) Create(token *token.Refresh) error {
	return r.database.Create(token).Error
}

func (r *Refresh) GetByID(token *token.Refresh, id uuid.UUID) error {
	return r.database.First(token, id).Error
}

func (r *Refresh) GetByUserID(token *token.Refresh, userID uuid.UUID) error {
	return r.database.First(token, "user_id", userID).Error
}

func (r *Refresh) Delete(token *token.Refresh) error {
	return r.database.Delete(token).Error
}
