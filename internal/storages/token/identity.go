package token

import (
	"github.com/google/uuid"
	"gorm.io/gorm"
	"log/slog"
	"sso/internal/models/token"
)

type Identity struct {
	log      *slog.Logger
	database *gorm.DB
}

func NewIdentity(log *slog.Logger, database *gorm.DB) *Identity {
	return &Identity{
		log:      log,
		database: database,
	}
}

func (i *Identity) Save(token *token.Identity) error {
	return i.database.Save(token).Error
}

func (i *Identity) Create(token *token.Identity) error {
	return i.database.Create(token).Error
}

func (i *Identity) GetByID(token *token.Identity, id uuid.UUID) error {
	return i.database.First(token, id).Error
}

func (i *Identity) GetByUserID(token *token.Identity, userID uuid.UUID) error {
	return i.database.First(token, "user_id", userID).Error
}

func (i *Identity) GetBySessionID(token *token.Identity, id uuid.UUID) error {
	return i.database.First(token, "session_id", id).Error
}

func (i *Identity) Delete(token *token.Identity) error {
	return i.database.Delete(token).Error
}
