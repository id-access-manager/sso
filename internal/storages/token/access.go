package token

import (
	"github.com/google/uuid"
	"gorm.io/gorm"
	"log/slog"
	"sso/internal/models/token"
)

type Access struct {
	log      *slog.Logger
	database *gorm.DB
}

func NewAccess(log *slog.Logger, database *gorm.DB) *Access {
	return &Access{
		log:      log,
		database: database,
	}
}

func (a *Access) Save(token *token.Access) error {
	return a.database.Save(token).Error
}

func (a *Access) Create(token *token.Access) error {
	return a.database.Create(token).Error
}

func (a *Access) GetByID(token *token.Access, id uuid.UUID) error {
	return a.database.First(token, id).Error
}

func (a *Access) GetByUserID(token *token.Access, userID uuid.UUID) error {
	return a.database.First(token, "user_id", userID).Error
}

func (a *Access) Delete(token *token.Access) error {
	return a.database.Delete(token).Error
}
