package storages

import (
	"github.com/google/uuid"
	"gorm.io/gorm"
	"log/slog"
	"sso/internal/models"
)

type Profile struct {
	log      *slog.Logger
	database *gorm.DB
}

func NewProfile(log *slog.Logger, database *gorm.DB) *Profile {
	return &Profile{
		log:      log,
		database: database,
	}
}

func (p *Profile) Create(profile *models.Profile) error {
	const op = "db.mock_storages.Profile.Create"
	return p.database.Create(profile).Error
}
func (p *Profile) GetByID(profile *models.Profile, id uuid.UUID) error {
	const op = "db.mock_storages.Profile.GetByID"
	return p.database.First(profile, id).Error
}
func (p *Profile) GetByUserId(profile *models.Profile, id uuid.UUID) error {
	const op = "db.mock_storages.Profile.getByUserId"
	return p.database.First(profile, "user_id", id).Error
}
func (p *Profile) GetList(profiles *[]models.Profile, offset int, limit int, sort string) error {
	const op = "db.mock_storages.Profile.GetList"
	return p.database.Find(&profiles).Offset(offset).Limit(limit).Order(sort).Error
}
func (p *Profile) Update(profile *models.Profile, data map[string]any) error {
	const op = "db.mock_storages.Profile.Update"
	return p.database.Model(profile).Updates(data).Error
}
func (p *Profile) Delete(profile *models.Profile) error {
	const op = "db.mock_storages.Profile.Delete"
	return p.database.Delete(profile).Error
}
