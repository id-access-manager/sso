package storages

import (
	"github.com/google/uuid"
	"gorm.io/gorm"
	"log/slog"
	"sso/internal/models"
)

type Permission struct {
	log      *slog.Logger
	database *gorm.DB
}

func NewPermission(log *slog.Logger, database *gorm.DB) *Permission {
	return &Permission{
		log:      log,
		database: database,
	}
}

func (p *Permission) Create(permission *models.Permission) error {
	//const op = "db.mock_storages.permission.Create"
	return p.database.Create(permission).Error
}

func (p *Permission) Update(permission *models.Permission, data map[string]any) error {
	//const op = "db.mock_storages.permission.Update"
	return p.database.Model(&permission).Updates(data).Error
}

func (p *Permission) GetByID(permission *models.Permission, id uuid.UUID) error {
	//const op = "db.mock_storages.permission.GetByID"
	return p.database.First(&permission, id).Error
}

func (r *Permission) GetByIDs(permissions *[]models.Permission, ids []uuid.UUID) error {
	const op = "db.resource.Resource.GetByIDs"
	return r.database.Model(&models.Permission{}).Where("id IN (?)", ids).Find(permissions).Error
}

func (p *Permission) GetList(permissions *[]models.Permission, limit int, offset int, sort string) error {
	//const op = "db.mock_storages.permission.GetList"
	return p.database.Find(permissions).Limit(limit).Offset(offset).Order(sort).Error
}

func (p *Permission) Delete(permission *models.Permission) error {
	//const op = "db.mock_storages.permission.Delete"
	return p.database.Delete(permission).Error
}

func (p *Permission) Count(total *int64) error {
	//const op = "db.storages.Permission.Count"
	return p.database.Model(&models.Permission{}).Count(total).Error
}
