package storages

import (
	"github.com/google/uuid"
	"gorm.io/gorm"
	"log/slog"
	"sso/internal/models"
)

type Action struct {
	log      *slog.Logger
	database *gorm.DB
}

func NewAction(log *slog.Logger, db *gorm.DB) *Action {
	return &Action{
		log:      log,
		database: db,
	}
}

func (a *Action) Create(action *models.Action) error {
	//const op = "db.mock_storages.Action.Create"
	return a.database.Create(&action).Error
}

func (a *Action) GetByID(action *models.Action, id uuid.UUID) error {
	//const op = "db.mock_storages.Action.GetByID"
	return a.database.First(&action, id).Error
}

func (a *Action) GetAll(actions *[]models.Action, limit int, offset int, sort string) error {
	//const op = "db.mock_storages.Action.GetAll"
	return a.database.Find(&actions).Offset(offset).Limit(limit).Order(sort).Error
}

func (a *Action) Update(action *models.Action, data map[string]any) error {
	//const op = "db.mock_storages.Action.Update"
	return a.database.Model(&action).Updates(data).Error
}

func (a *Action) Delete(action *models.Action) error {
	//const op = "db.mock_storages.Action.Delete"
	return a.database.Delete(&action).Error
}

func (a *Action) Count(total *int64) error {
	//const op = "db.storages.Action.Count"
	return a.database.Model(&models.Action{}).Count(total).Error
}
