// Code generated by MockGen. DO NOT EDIT.
// Source: action.go

// Package storages is a generated GoMock package.
package storages

import (
	reflect "reflect"
	models "sso/internal/models"

	gomock "github.com/golang/mock/gomock"
	uuid "github.com/google/uuid"
)

// MockactionStorage is a mock of actionStorage interfaces.
type MockactionStorage struct {
	ctrl     *gomock.Controller
	recorder *MockactionStorageMockRecorder
}

// MockactionStorageMockRecorder is the mock recorder for MockactionStorage.
type MockactionStorageMockRecorder struct {
	mock *MockactionStorage
}

// NewMockactionStorage creates a new mock instance.
func NewMockactionStorage(ctrl *gomock.Controller) *MockactionStorage {
	mock := &MockactionStorage{ctrl: ctrl}
	mock.recorder = &MockactionStorageMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockactionStorage) EXPECT() *MockactionStorageMockRecorder {
	return m.recorder
}

// Count mocks base method.
func (m *MockactionStorage) Count(total *int64) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Count", total)
	ret0, _ := ret[0].(error)
	return ret0
}

// Count indicates an expected call of Count.
func (mr *MockactionStorageMockRecorder) Count(total interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Count", reflect.TypeOf((*MockactionStorage)(nil).Count), total)
}

// Create mocks base method.
func (m *MockactionStorage) Create(action *models.Action) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Create", action)
	ret0, _ := ret[0].(error)
	return ret0
}

// Create indicates an expected call of Create.
func (mr *MockactionStorageMockRecorder) Create(action interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Create", reflect.TypeOf((*MockactionStorage)(nil).Create), action)
}

// Delete mocks base method.
func (m *MockactionStorage) Delete(action *models.Action) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Delete", action)
	ret0, _ := ret[0].(error)
	return ret0
}

// Delete indicates an expected call of Delete.
func (mr *MockactionStorageMockRecorder) Delete(action interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Delete", reflect.TypeOf((*MockactionStorage)(nil).Delete), action)
}

// GetAll mocks base method.
func (m *MockactionStorage) GetAll(actions *[]models.Action, limit, offset int, sort string) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetAll", actions, limit, offset, sort)
	ret0, _ := ret[0].(error)
	return ret0
}

// GetAll indicates an expected call of GetAll.
func (mr *MockactionStorageMockRecorder) GetAll(actions, limit, offset, sort interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetAll", reflect.TypeOf((*MockactionStorage)(nil).GetAll), actions, limit, offset, sort)
}

// GetByID mocks base method.
func (m *MockactionStorage) GetByID(action *models.Action, id uuid.UUID) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetByID", action, id)
	ret0, _ := ret[0].(error)
	return ret0
}

// GetByID indicates an expected call of GetByID.
func (mr *MockactionStorageMockRecorder) GetByID(action, id interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetByID", reflect.TypeOf((*MockactionStorage)(nil).GetByID), action, id)
}

// Update mocks base method.
func (m *MockactionStorage) Update(action *models.Action, data map[string]any) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Update", action, data)
	ret0, _ := ret[0].(error)
	return ret0
}

// Update indicates an expected call of Update.
func (mr *MockactionStorageMockRecorder) Update(action, data interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Update", reflect.TypeOf((*MockactionStorage)(nil).Update), action, data)
}

// MockactionCreator is a mock of actionCreator interfaces.
type MockactionCreator struct {
	ctrl     *gomock.Controller
	recorder *MockactionCreatorMockRecorder
}

// MockactionCreatorMockRecorder is the mock recorder for MockactionCreator.
type MockactionCreatorMockRecorder struct {
	mock *MockactionCreator
}

// NewMockactionCreator creates a new mock instance.
func NewMockactionCreator(ctrl *gomock.Controller) *MockactionCreator {
	mock := &MockactionCreator{ctrl: ctrl}
	mock.recorder = &MockactionCreatorMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockactionCreator) EXPECT() *MockactionCreatorMockRecorder {
	return m.recorder
}

// Create mocks base method.
func (m *MockactionCreator) Create(action *models.Action) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Create", action)
	ret0, _ := ret[0].(error)
	return ret0
}

// Create indicates an expected call of Create.
func (mr *MockactionCreatorMockRecorder) Create(action interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Create", reflect.TypeOf((*MockactionCreator)(nil).Create), action)
}

// MockactionGetter is a mock of actionGetter interfaces.
type MockactionGetter struct {
	ctrl     *gomock.Controller
	recorder *MockactionGetterMockRecorder
}

// MockactionGetterMockRecorder is the mock recorder for MockactionGetter.
type MockactionGetterMockRecorder struct {
	mock *MockactionGetter
}

// NewMockactionGetter creates a new mock instance.
func NewMockactionGetter(ctrl *gomock.Controller) *MockactionGetter {
	mock := &MockactionGetter{ctrl: ctrl}
	mock.recorder = &MockactionGetterMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockactionGetter) EXPECT() *MockactionGetterMockRecorder {
	return m.recorder
}

// Count mocks base method.
func (m *MockactionGetter) Count(total *int64) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Count", total)
	ret0, _ := ret[0].(error)
	return ret0
}

// Count indicates an expected call of Count.
func (mr *MockactionGetterMockRecorder) Count(total interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Count", reflect.TypeOf((*MockactionGetter)(nil).Count), total)
}

// GetAll mocks base method.
func (m *MockactionGetter) GetAll(actions *[]models.Action, limit, offset int, sort string) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetAll", actions, limit, offset, sort)
	ret0, _ := ret[0].(error)
	return ret0
}

// GetAll indicates an expected call of GetAll.
func (mr *MockactionGetterMockRecorder) GetAll(actions, limit, offset, sort interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetAll", reflect.TypeOf((*MockactionGetter)(nil).GetAll), actions, limit, offset, sort)
}

// GetByID mocks base method.
func (m *MockactionGetter) GetByID(action *models.Action, id uuid.UUID) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetByID", action, id)
	ret0, _ := ret[0].(error)
	return ret0
}

// GetByID indicates an expected call of GetByID.
func (mr *MockactionGetterMockRecorder) GetByID(action, id interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetByID", reflect.TypeOf((*MockactionGetter)(nil).GetByID), action, id)
}

// MockactionUpdater is a mock of actionUpdater interfaces.
type MockactionUpdater struct {
	ctrl     *gomock.Controller
	recorder *MockactionUpdaterMockRecorder
}

// MockactionUpdaterMockRecorder is the mock recorder for MockactionUpdater.
type MockactionUpdaterMockRecorder struct {
	mock *MockactionUpdater
}

// NewMockactionUpdater creates a new mock instance.
func NewMockactionUpdater(ctrl *gomock.Controller) *MockactionUpdater {
	mock := &MockactionUpdater{ctrl: ctrl}
	mock.recorder = &MockactionUpdaterMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockactionUpdater) EXPECT() *MockactionUpdaterMockRecorder {
	return m.recorder
}

// Update mocks base method.
func (m *MockactionUpdater) Update(action *models.Action, data map[string]any) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Update", action, data)
	ret0, _ := ret[0].(error)
	return ret0
}

// Update indicates an expected call of Update.
func (mr *MockactionUpdaterMockRecorder) Update(action, data interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Update", reflect.TypeOf((*MockactionUpdater)(nil).Update), action, data)
}

// MockactionDeleter is a mock of actionDeleter interfaces.
type MockactionDeleter struct {
	ctrl     *gomock.Controller
	recorder *MockactionDeleterMockRecorder
}

// MockactionDeleterMockRecorder is the mock recorder for MockactionDeleter.
type MockactionDeleterMockRecorder struct {
	mock *MockactionDeleter
}

// NewMockactionDeleter creates a new mock instance.
func NewMockactionDeleter(ctrl *gomock.Controller) *MockactionDeleter {
	mock := &MockactionDeleter{ctrl: ctrl}
	mock.recorder = &MockactionDeleterMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockactionDeleter) EXPECT() *MockactionDeleterMockRecorder {
	return m.recorder
}

// Delete mocks base method.
func (m *MockactionDeleter) Delete(action *models.Action) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Delete", action)
	ret0, _ := ret[0].(error)
	return ret0
}

// Delete indicates an expected call of Delete.
func (mr *MockactionDeleterMockRecorder) Delete(action interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Delete", reflect.TypeOf((*MockactionDeleter)(nil).Delete), action)
}
