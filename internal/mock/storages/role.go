// Code generated by MockGen. DO NOT EDIT.
// Source: role.go

// Package storages is a generated GoMock package.
package storages

import (
	reflect "reflect"
	models "sso/internal/models"

	gomock "github.com/golang/mock/gomock"
	uuid "github.com/google/uuid"
)

// MockroleStorage is a mock of roleStorage interface.
type MockroleStorage struct {
	ctrl     *gomock.Controller
	recorder *MockroleStorageMockRecorder
}

// MockroleStorageMockRecorder is the mock recorder for MockroleStorage.
type MockroleStorageMockRecorder struct {
	mock *MockroleStorage
}

// NewMockroleStorage creates a new mock instance.
func NewMockroleStorage(ctrl *gomock.Controller) *MockroleStorage {
	mock := &MockroleStorage{ctrl: ctrl}
	mock.recorder = &MockroleStorageMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockroleStorage) EXPECT() *MockroleStorageMockRecorder {
	return m.recorder
}

// AddPermissions mocks base method.
func (m *MockroleStorage) AddPermissions(role *models.Role, permissions []models.Permission) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "AddPermissions", role, permissions)
	ret0, _ := ret[0].(error)
	return ret0
}

// AddPermissions indicates an expected call of AddPermissions.
func (mr *MockroleStorageMockRecorder) AddPermissions(role, permissions interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "AddPermissions", reflect.TypeOf((*MockroleStorage)(nil).AddPermissions), role, permissions)
}

// Count mocks base method.
func (m *MockroleStorage) Count(count *int64) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Count", count)
	ret0, _ := ret[0].(error)
	return ret0
}

// Count indicates an expected call of Count.
func (mr *MockroleStorageMockRecorder) Count(count interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Count", reflect.TypeOf((*MockroleStorage)(nil).Count), count)
}

// Create mocks base method.
func (m *MockroleStorage) Create(role *models.Role) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Create", role)
	ret0, _ := ret[0].(error)
	return ret0
}

// Create indicates an expected call of Create.
func (mr *MockroleStorageMockRecorder) Create(role interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Create", reflect.TypeOf((*MockroleStorage)(nil).Create), role)
}

// Delete mocks base method.
func (m *MockroleStorage) Delete(role *models.Role) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Delete", role)
	ret0, _ := ret[0].(error)
	return ret0
}

// Delete indicates an expected call of Delete.
func (mr *MockroleStorageMockRecorder) Delete(role interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Delete", reflect.TypeOf((*MockroleStorage)(nil).Delete), role)
}

// GetAll mocks base method.
func (m *MockroleStorage) GetAll(roles *[]models.Role, limit, offset int, sort string) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetAll", roles, limit, offset, sort)
	ret0, _ := ret[0].(error)
	return ret0
}

// GetAll indicates an expected call of GetAll.
func (mr *MockroleStorageMockRecorder) GetAll(roles, limit, offset, sort interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetAll", reflect.TypeOf((*MockroleStorage)(nil).GetAll), roles, limit, offset, sort)
}

// GetByID mocks base method.
func (m *MockroleStorage) GetByID(role *models.Role, id uuid.UUID) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetByID", role, id)
	ret0, _ := ret[0].(error)
	return ret0
}

// GetByID indicates an expected call of GetByID.
func (mr *MockroleStorageMockRecorder) GetByID(role, id interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetByID", reflect.TypeOf((*MockroleStorage)(nil).GetByID), role, id)
}

// GetByIDs mocks base method.
func (m *MockroleStorage) GetByIDs(resources *[]models.Role, ids []uuid.UUID) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetByIDs", resources, ids)
	ret0, _ := ret[0].(error)
	return ret0
}

// GetByIDs indicates an expected call of GetByIDs.
func (mr *MockroleStorageMockRecorder) GetByIDs(resources, ids interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetByIDs", reflect.TypeOf((*MockroleStorage)(nil).GetByIDs), resources, ids)
}

// RemovePermissions mocks base method.
func (m *MockroleStorage) RemovePermissions(role *models.Role, permissions []models.Permission) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "RemovePermissions", role, permissions)
	ret0, _ := ret[0].(error)
	return ret0
}

// RemovePermissions indicates an expected call of RemovePermissions.
func (mr *MockroleStorageMockRecorder) RemovePermissions(role, permissions interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "RemovePermissions", reflect.TypeOf((*MockroleStorage)(nil).RemovePermissions), role, permissions)
}

// ReplacePermissions mocks base method.
func (m *MockroleStorage) ReplacePermissions(role *models.Role, permissions []models.Permission) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "ReplacePermissions", role, permissions)
	ret0, _ := ret[0].(error)
	return ret0
}

// ReplacePermissions indicates an expected call of ReplacePermissions.
func (mr *MockroleStorageMockRecorder) ReplacePermissions(role, permissions interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "ReplacePermissions", reflect.TypeOf((*MockroleStorage)(nil).ReplacePermissions), role, permissions)
}

// Update mocks base method.
func (m *MockroleStorage) Update(role *models.Role, data map[string]any) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Update", role, data)
	ret0, _ := ret[0].(error)
	return ret0
}

// Update indicates an expected call of Update.
func (mr *MockroleStorageMockRecorder) Update(role, data interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Update", reflect.TypeOf((*MockroleStorage)(nil).Update), role, data)
}

// MockroleCreator is a mock of roleCreator interface.
type MockroleCreator struct {
	ctrl     *gomock.Controller
	recorder *MockroleCreatorMockRecorder
}

// MockroleCreatorMockRecorder is the mock recorder for MockroleCreator.
type MockroleCreatorMockRecorder struct {
	mock *MockroleCreator
}

// NewMockroleCreator creates a new mock instance.
func NewMockroleCreator(ctrl *gomock.Controller) *MockroleCreator {
	mock := &MockroleCreator{ctrl: ctrl}
	mock.recorder = &MockroleCreatorMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockroleCreator) EXPECT() *MockroleCreatorMockRecorder {
	return m.recorder
}

// AddPermissions mocks base method.
func (m *MockroleCreator) AddPermissions(role *models.Role, permissions []models.Permission) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "AddPermissions", role, permissions)
	ret0, _ := ret[0].(error)
	return ret0
}

// AddPermissions indicates an expected call of AddPermissions.
func (mr *MockroleCreatorMockRecorder) AddPermissions(role, permissions interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "AddPermissions", reflect.TypeOf((*MockroleCreator)(nil).AddPermissions), role, permissions)
}

// Create mocks base method.
func (m *MockroleCreator) Create(role *models.Role) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Create", role)
	ret0, _ := ret[0].(error)
	return ret0
}

// Create indicates an expected call of Create.
func (mr *MockroleCreatorMockRecorder) Create(role interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Create", reflect.TypeOf((*MockroleCreator)(nil).Create), role)
}

// MockroleGetter is a mock of roleGetter interface.
type MockroleGetter struct {
	ctrl     *gomock.Controller
	recorder *MockroleGetterMockRecorder
}

// MockroleGetterMockRecorder is the mock recorder for MockroleGetter.
type MockroleGetterMockRecorder struct {
	mock *MockroleGetter
}

// NewMockroleGetter creates a new mock instance.
func NewMockroleGetter(ctrl *gomock.Controller) *MockroleGetter {
	mock := &MockroleGetter{ctrl: ctrl}
	mock.recorder = &MockroleGetterMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockroleGetter) EXPECT() *MockroleGetterMockRecorder {
	return m.recorder
}

// Count mocks base method.
func (m *MockroleGetter) Count(count *int64) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Count", count)
	ret0, _ := ret[0].(error)
	return ret0
}

// Count indicates an expected call of Count.
func (mr *MockroleGetterMockRecorder) Count(count interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Count", reflect.TypeOf((*MockroleGetter)(nil).Count), count)
}

// GetAll mocks base method.
func (m *MockroleGetter) GetAll(roles *[]models.Role, limit, offset int, sort string) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetAll", roles, limit, offset, sort)
	ret0, _ := ret[0].(error)
	return ret0
}

// GetAll indicates an expected call of GetAll.
func (mr *MockroleGetterMockRecorder) GetAll(roles, limit, offset, sort interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetAll", reflect.TypeOf((*MockroleGetter)(nil).GetAll), roles, limit, offset, sort)
}

// GetByID mocks base method.
func (m *MockroleGetter) GetByID(role *models.Role, id uuid.UUID) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetByID", role, id)
	ret0, _ := ret[0].(error)
	return ret0
}

// GetByID indicates an expected call of GetByID.
func (mr *MockroleGetterMockRecorder) GetByID(role, id interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetByID", reflect.TypeOf((*MockroleGetter)(nil).GetByID), role, id)
}

// GetByIDs mocks base method.
func (m *MockroleGetter) GetByIDs(resources *[]models.Role, ids []uuid.UUID) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetByIDs", resources, ids)
	ret0, _ := ret[0].(error)
	return ret0
}

// GetByIDs indicates an expected call of GetByIDs.
func (mr *MockroleGetterMockRecorder) GetByIDs(resources, ids interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetByIDs", reflect.TypeOf((*MockroleGetter)(nil).GetByIDs), resources, ids)
}

// MockroleUpdater is a mock of roleUpdater interface.
type MockroleUpdater struct {
	ctrl     *gomock.Controller
	recorder *MockroleUpdaterMockRecorder
}

// MockroleUpdaterMockRecorder is the mock recorder for MockroleUpdater.
type MockroleUpdaterMockRecorder struct {
	mock *MockroleUpdater
}

// NewMockroleUpdater creates a new mock instance.
func NewMockroleUpdater(ctrl *gomock.Controller) *MockroleUpdater {
	mock := &MockroleUpdater{ctrl: ctrl}
	mock.recorder = &MockroleUpdaterMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockroleUpdater) EXPECT() *MockroleUpdaterMockRecorder {
	return m.recorder
}

// ReplacePermissions mocks base method.
func (m *MockroleUpdater) ReplacePermissions(role *models.Role, permissions []models.Permission) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "ReplacePermissions", role, permissions)
	ret0, _ := ret[0].(error)
	return ret0
}

// ReplacePermissions indicates an expected call of ReplacePermissions.
func (mr *MockroleUpdaterMockRecorder) ReplacePermissions(role, permissions interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "ReplacePermissions", reflect.TypeOf((*MockroleUpdater)(nil).ReplacePermissions), role, permissions)
}

// Update mocks base method.
func (m *MockroleUpdater) Update(role *models.Role, data map[string]any) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Update", role, data)
	ret0, _ := ret[0].(error)
	return ret0
}

// Update indicates an expected call of Update.
func (mr *MockroleUpdaterMockRecorder) Update(role, data interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Update", reflect.TypeOf((*MockroleUpdater)(nil).Update), role, data)
}

// MockroleDeleter is a mock of roleDeleter interface.
type MockroleDeleter struct {
	ctrl     *gomock.Controller
	recorder *MockroleDeleterMockRecorder
}

// MockroleDeleterMockRecorder is the mock recorder for MockroleDeleter.
type MockroleDeleterMockRecorder struct {
	mock *MockroleDeleter
}

// NewMockroleDeleter creates a new mock instance.
func NewMockroleDeleter(ctrl *gomock.Controller) *MockroleDeleter {
	mock := &MockroleDeleter{ctrl: ctrl}
	mock.recorder = &MockroleDeleterMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockroleDeleter) EXPECT() *MockroleDeleterMockRecorder {
	return m.recorder
}

// Delete mocks base method.
func (m *MockroleDeleter) Delete(role *models.Role) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Delete", role)
	ret0, _ := ret[0].(error)
	return ret0
}

// Delete indicates an expected call of Delete.
func (mr *MockroleDeleterMockRecorder) Delete(role interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Delete", reflect.TypeOf((*MockroleDeleter)(nil).Delete), role)
}

// RemovePermissions mocks base method.
func (m *MockroleDeleter) RemovePermissions(role *models.Role, permissions []models.Permission) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "RemovePermissions", role, permissions)
	ret0, _ := ret[0].(error)
	return ret0
}

// RemovePermissions indicates an expected call of RemovePermissions.
func (mr *MockroleDeleterMockRecorder) RemovePermissions(role, permissions interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "RemovePermissions", reflect.TypeOf((*MockroleDeleter)(nil).RemovePermissions), role, permissions)
}
