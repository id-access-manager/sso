package storages

import (
	"github.com/golang/mock/gomock"
	"github.com/google/uuid"
	"github.com/mitchellh/mapstructure"
	"sso/internal/mock/gen"
	"sso/internal/models"
)

func SimulateAction(controller *gomock.Controller, err error) *MockactionStorage {
	mockStorage := NewMockactionStorage(controller)
	{
		mockStorage.EXPECT().Create(
			gomock.AssignableToTypeOf(&models.Action{}),
		).Do(
			func(action *models.Action) {
				_ = action.BeforeCreate(nil)
			}).Return(err).AnyTimes()

		mockStorage.EXPECT().GetByID(
			gomock.AssignableToTypeOf(&models.Action{}), gomock.AssignableToTypeOf(uuid.UUID{}),
		).Do(
			func(action *models.Action, id uuid.UUID) {
				action.ID = id
			}).Return(err).AnyTimes()

		mockStorage.EXPECT().Update(
			gomock.AssignableToTypeOf(&models.Action{}), gomock.AssignableToTypeOf(map[string]any{}),
		).DoAndReturn(
			func(action *models.Action, data map[string]any) {
				_ = mapstructure.Decode(data, action)
				_ = action.BeforeUpdate(nil)
			}).Return(err).AnyTimes()

		mockStorage.EXPECT().Delete(gomock.AssignableToTypeOf(&models.Action{})).Return(err).AnyTimes()

		mockStorage.EXPECT().GetAll(
			gomock.AssignableToTypeOf(&[]models.Action{}), gomock.Any(), gomock.Any(), gomock.Any(),
		).Do(
			func(actions *[]models.Action, limit int, offset int, sort string) {
				for i := 0; i < limit; i++ {
					*actions = append(*actions, gen.GenerateAction())
				}
			}).Return(err).AnyTimes()
	}
	return mockStorage
}

func SimulateObj(controller *gomock.Controller, err error) *MockobjStorage {
	mockStorage := NewMockobjStorage(controller)
	{
		mockStorage.EXPECT().Create(
			gomock.Any(),
		).Do(
			func(obj *models.Object) {
				_ = obj.BeforeCreate(nil)
			}).Return(err).AnyTimes()

		mockStorage.EXPECT().Get(
			gomock.Any(), gomock.Any(),
		).Do(
			func(obj *models.Object, id uuid.UUID) {
				obj.ID = id
			}).Return(err).AnyTimes()

		mockStorage.EXPECT().Update(
			gomock.Any(), gomock.Any(),
		).Do(
			func(obj *models.Object, data map[string]any) {
				_ = obj.BeforeUpdate(nil)
				_ = mapstructure.Decode(data, &obj)
			}).Return(err).AnyTimes()

		mockStorage.EXPECT().Delete(gomock.Any()).Return(err).AnyTimes()

		mockStorage.EXPECT().GetAll(
			gomock.AssignableToTypeOf(&[]models.Object{}), gomock.Any(), gomock.Any(), gomock.Any(),
		).Do(
			func(actions *[]models.Object, limit int, offset int, sort string) {
				for i := 0; i < limit; i++ {
					*actions = append(*actions, gen.GenerateObject())
				}
			}).Return(err).AnyTimes()
	}

	return mockStorage
}

func SimulatePermission(controller *gomock.Controller, err error) *MockpermissionStorage {
	mockStorage := NewMockpermissionStorage(controller)
	{
		mockStorage.EXPECT().Create(
			gomock.AssignableToTypeOf(&models.Permission{}),
		).Do(func(permission *models.Permission) {
			_ = permission.BeforeCreate(nil)
		}).Return(err).AnyTimes()

		mockStorage.EXPECT().GetByID(
			gomock.AssignableToTypeOf(&models.Permission{}), gomock.AssignableToTypeOf(uuid.UUID{}),
		).DoAndReturn(func(permission *models.Permission, id uuid.UUID) {
			permission.ID = id
		}).Return(err).AnyTimes()

		mockStorage.EXPECT().Update(
			gomock.AssignableToTypeOf(&models.Permission{}), gomock.AssignableToTypeOf(map[string]any{}),
		).Do(func(permission *models.Permission, data map[string]any) {
			_ = mapstructure.Decode(data, &permission)
			_ = permission.BeforeUpdate(nil)
		}).Return(err).AnyTimes()

		mockStorage.EXPECT().Delete(
			gomock.AssignableToTypeOf(&models.Permission{}),
		).Return(err).AnyTimes()

		mockStorage.EXPECT().GetList(
			gomock.AssignableToTypeOf(&[]models.Permission{}), gomock.Any(), gomock.Any(), gomock.Any(),
		).DoAndReturn(
			func(permissions *[]models.Permission, limit int, offset int, sort string) error {
				for i := 0; i < limit; i++ {
					*permissions = append(*permissions, gen.GeneratePermission())
				}
				return err
			}).
			AnyTimes()

		mockStorage.EXPECT().GetByIDs(
			gomock.AssignableToTypeOf(&[]models.Permission{}),
			gomock.AssignableToTypeOf([]uuid.UUID{}),
		).DoAndReturn(
			func(permissions *[]models.Permission, ids []uuid.UUID) error {
				for i := 0; i <= len(ids); i++ {
					*permissions = append(*permissions, gen.GeneratePermission())
				}
				return err
			}).AnyTimes()
	}
	return mockStorage
}

//func simulateProfile(controller *gomock.Controller, err error) *MockprofileStorage {
//	mockStorage := NewMockprofileStorage(controller)
//	return mockStorage
//}

func SimulateResource(controller *gomock.Controller, err error) *MockresourceStorage {
	mockStorage := NewMockresourceStorage(controller)
	{
		mockStorage.EXPECT().Create(
			gomock.AssignableToTypeOf(&models.Resource{}),
		).DoAndReturn(
			func(resource *models.Resource) error {
				_ = resource.BeforeCreate(nil)
				return err
			}).AnyTimes()

		mockStorage.EXPECT().AddRoles(
			gomock.AssignableToTypeOf(&models.Resource{}),
			gomock.AssignableToTypeOf(&[]models.Role{}),
		).DoAndReturn(
			func(resource *models.Resource, roles *[]models.Role) error {
				resource.Roles = *roles
				return err
			}).AnyTimes()

		mockStorage.EXPECT().GetByID(
			gomock.AssignableToTypeOf(&models.Resource{}),
			gomock.AssignableToTypeOf(uuid.UUID{}),
		).DoAndReturn(
			func(resource *models.Resource, id uuid.UUID) error {
				res := gen.GenerateResource()
				resource.ID = id
				resource.Name = res.Name
				resource.Description = res.Description
				return err
			}).AnyTimes()

		mockStorage.EXPECT().GetByIDs(
			gomock.AssignableToTypeOf(&[]models.Resource{}),
			gomock.AssignableToTypeOf([]uuid.UUID{}),
		).DoAndReturn(
			func(resource *[]models.Resource, ids []uuid.UUID) error {
				for i := 0; i < len(ids); i++ {
					*resource = append(*resource, gen.GenerateResource())
				}
				return err
			}).AnyTimes()

		mockStorage.EXPECT().GetAll(
			gomock.AssignableToTypeOf(&[]models.Resource{}), gomock.Any(), gomock.Any(), gomock.Any(),
		).DoAndReturn(
			func(role *[]models.Resource, limit int, offset int, sort string) error {
				for i := 0; i <= limit; i++ {
					*role = append(*role, gen.GenerateResource())
				}
				return err
			}).AnyTimes()

		mockStorage.EXPECT().Update(
			gomock.AssignableToTypeOf(&models.Resource{}),
			gomock.AssignableToTypeOf(map[string]any{}),
		).DoAndReturn(
			func(role *models.Resource, data map[string]any) error {
				_ = mapstructure.Decode(data, &role)
				_ = role.BeforeUpdate(nil)
				return err
			}).AnyTimes()

		mockStorage.EXPECT().ReplaceRoles(
			gomock.AssignableToTypeOf(&models.Resource{}),
			gomock.AssignableToTypeOf(&[]models.Role{}),
		).DoAndReturn(
			func(resource *models.Resource, permissions *[]models.Role) error {
				resource.Roles = *permissions
				return err
			}).
			AnyTimes()

		mockStorage.EXPECT().Delete(gomock.AssignableToTypeOf(&models.Resource{})).Return(err).AnyTimes()
	}
	return mockStorage
}

func SimulateRole(controller *gomock.Controller, err error) *MockroleStorage {
	mockStorage := NewMockroleStorage(controller)
	{
		mockStorage.EXPECT().Create(gomock.AssignableToTypeOf(&models.Role{})).
			DoAndReturn(
				func(role *models.Role) error {
					_ = role.BeforeCreate(nil)
					return err
				}).
			AnyTimes()

		mockStorage.EXPECT().AddPermissions(
			gomock.AssignableToTypeOf(&models.Role{}),
			gomock.AssignableToTypeOf([]models.Permission{}),
		).DoAndReturn(
			func(role *models.Role, permissions []models.Permission) error {
				role.Permissions = permissions
				return err
			}).
			AnyTimes()

		mockStorage.EXPECT().GetByID(
			gomock.AssignableToTypeOf(&models.Role{}),
			gomock.AssignableToTypeOf(uuid.UUID{}),
		).DoAndReturn(
			func(role *models.Role, id uuid.UUID) error {
				role.ID = id
				return err
			}).
			AnyTimes()

		mockStorage.EXPECT().GetAll(
			gomock.AssignableToTypeOf(&[]models.Role{}), gomock.Any(), gomock.Any(), gomock.Any(),
		).DoAndReturn(
			func(role *[]models.Role, limit int, offset int, sort string) error {
				for i := 0; i < limit; i++ {
					*role = append(*role, gen.GenerateRole())
				}
				return err
			}).
			AnyTimes()

		mockStorage.EXPECT().GetByIDs(
			gomock.AssignableToTypeOf(&[]models.Role{}),
			gomock.AssignableToTypeOf([]uuid.UUID{}),
		).DoAndReturn(
			func(role *[]models.Role, ids []uuid.UUID) error {
				for i := 0; i < len(ids); i++ {
					*role = append(*role, gen.GenerateRole())
				}
				return err
			}).AnyTimes()

		mockStorage.EXPECT().Update(
			gomock.AssignableToTypeOf(&models.Role{}),
			gomock.AssignableToTypeOf(map[string]any{}),
		).DoAndReturn(
			func(role *models.Role, data map[string]any) error {
				_ = mapstructure.Decode(data, &role)
				_ = role.BeforeUpdate(nil)
				return err
			}).
			AnyTimes()

		mockStorage.EXPECT().ReplacePermissions(
			gomock.AssignableToTypeOf(&models.Role{}),
			gomock.AssignableToTypeOf([]models.Permission{}),
		).DoAndReturn(
			func(role *models.Role, permissions []models.Permission) error {
				role.Permissions = permissions
				return err
			}).
			AnyTimes()

		mockStorage.EXPECT().Delete(gomock.AssignableToTypeOf(&models.Role{})).Return(err).AnyTimes()

	}
	return mockStorage
}

func SimulateSession(controller *gomock.Controller, err error) *MocksessionStorage {
	mockStorage := NewMocksessionStorage(controller)
	{
		mockStorage.
			EXPECT().
			Create(gomock.AssignableToTypeOf(&models.Session{})).
			Do(func(session *models.Session) {
				_ = session.BeforeCreate(nil)
			}).
			Return(err).
			AnyTimes()

		mockStorage.
			EXPECT().
			GetByID(gomock.AssignableToTypeOf(&models.Session{}), gomock.AssignableToTypeOf(uuid.UUID{})).
			Do(
				func(session *models.Session, id uuid.UUID) {
					session.ID = id
				}).
			Return(err).
			AnyTimes()

		mockStorage.
			EXPECT().
			GetValidByID(gomock.AssignableToTypeOf(&models.Session{}), gomock.AssignableToTypeOf(uuid.UUID{})).
			Do(
				func(session *models.Session, id uuid.UUID) {
					session.ID = id
				}).
			Return(err).
			AnyTimes()

		mockStorage.
			EXPECT().
			Update(gomock.AssignableToTypeOf(&models.Session{}), gomock.AssignableToTypeOf(map[string]any{})).
			Do(
				func(session *models.Session, data map[string]any) {
					_ = mapstructure.Decode(data, &session)
					_ = session.BeforeUpdate(nil)
				}).
			Return(err).
			AnyTimes()

		mockStorage.
			EXPECT().
			Delete(gomock.AssignableToTypeOf(&models.Session{})).
			Return().
			AnyTimes()
	}
	return mockStorage
}

func simulateUser(controller *gomock.Controller, err error) *MockuserStorage {
	mockStorage := NewMockuserStorage(controller)
	return mockStorage
}
