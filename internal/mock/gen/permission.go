package gen

import (
	"sso/internal/models"
)

func GeneratePermission() models.Permission {
	action := GenerateAction()
	object := GenerateObject()
	permission := models.Permission{
		Name:     GenerateString(),
		ActionID: action.ID,
		Action:   action,
		ObjectID: object.ID,
		Object:   object,
	}
	_ = permission.BeforeCreate(nil)
	return permission
}
