package gen

import "sso/internal/models"

func GenerateRole() models.Role {
	var permissions []models.Permission

	for i := 0; i < 5; i++ {
		permissions = append(permissions, GeneratePermission())
	}

	role := models.Role{
		Name: GenerateString(),
	}

	_ = role.BeforeCreate(nil)

	return role
}
