package gen

import "sso/internal/models"

func GenerateResource() models.Resource {
	resource := models.Resource{
		Name:        GenerateString(),
		Description: GenerateString() + GenerateString(),
	}
	_ = resource.BeforeCreate(nil)
	return resource
}
