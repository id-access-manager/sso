package gen

import (
	"sso/internal/models"
)

func GenerateObject() models.Object {
	obj := models.Object{
		Name: GenerateString(),
	}
	_ = obj.BeforeCreate(nil)
	return obj
}
