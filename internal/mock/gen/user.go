package gen

import "sso/internal/models"

func GenerateUser() models.User {
	user := models.User{
		Email:    models.Email(GenerateString()),
		Username: GenerateString(),
	}
	_ = user.BeforeCreate(nil)
	return user
}
