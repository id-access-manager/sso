package gen

import (
	"math/rand"
	"sso/internal/models"
	"time"
)

func getRandomAction() models.ActionType {
	rand.New(rand.NewSource(time.Now().UnixNano()))
	actions := []models.ActionType{models.CreateAction, models.UpdateAction, models.ReadAction, models.DeleteAction}
	return actions[rand.Intn(len(actions))]
}

func GenerateAction() models.Action {
	obj := models.Action{
		Name:       GenerateString(),
		ActionType: getRandomAction(),
	}
	_ = obj.BeforeCreate(nil)
	return obj
}
