package gen

import (
	"sso/internal/dto"
	"sso/internal/models"
	"sso/internal/models/token"
)

func GenerateTokens() dto.Tokens {
	user := GenerateUser()
	return dto.Tokens{
		Access:   generateAccessToken(user, models.Session{}).Token,
		Refresh:  generateRefreshToken(user, models.Session{}).Token,
		Identity: generateIdentityToken(user, models.Session{}).Token,
	}
}

func generateAccessToken(user models.User, session models.Session) token.Access {
	t := token.Access{
		UserID:    user.ID,
		User:      user,
		SessionID: session.ID,
		Session:   session,
	}
	_ = t.BeforeCreate(nil)
	return t
}

func generateRefreshToken(user models.User, session models.Session) token.Refresh {
	t := token.Refresh{
		UserID:    user.ID,
		User:      user,
		SessionID: session.ID,
		Session:   session,
	}
	_ = t.BeforeCreate(nil)
	return t
}

func generateIdentityToken(user models.User, session models.Session) token.Identity {
	t := token.Identity{
		UserID:    user.ID,
		User:      user,
		SessionID: session.ID,
		Session:   session,
	}
	_ = t.BeforeCreate(nil)
	return t
}
