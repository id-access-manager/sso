package services

import (
	"github.com/golang/mock/gomock"
	"sso/internal/dto"
	"sso/internal/mock/gen"
)

func AuthSimulation(controller *gomock.Controller, err error) *MockauthService {
	mockService := NewMockauthService(controller)
	{
		mockService.
			EXPECT().
			Login(gomock.Any(), gomock.Any()).
			DoAndReturn(
				func(username string, password string) (dto.Tokens, error) {
					return gen.GenerateTokens(), err
				},
			).
			AnyTimes()

		mockService.
			EXPECT().
			Refresh(gomock.Any()).
			DoAndReturn(
				func(refreshToken string) (dto.Tokens, error) {
					return gen.GenerateTokens(), err
				},
			).
			AnyTimes()
	}
	return mockService
}
