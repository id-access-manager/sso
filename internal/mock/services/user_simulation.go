package services

import (
	"github.com/golang/mock/gomock"
	"github.com/google/uuid"
	"math/rand"
	"sso/internal/mock/gen"
	"sso/internal/models"
	"time"
)

func SimulateUser(controller *gomock.Controller, err error) *MockuserService {
	mockService := NewMockuserService(controller)
	{
		mockService.
			EXPECT().
			GetById(gomock.AssignableToTypeOf(uuid.UUID{})).
			Return(gen.GenerateUser(), err).
			AnyTimes()

		mockService.
			EXPECT().
			GetAll(gomock.Any(), gomock.Any(), gomock.Any()).
			DoAndReturn(
				func(limit int, offset int, sort string) ([]models.Role, error) {
					var roles []models.Role
					for i := 0; i < limit; i++ {
						roles = append(roles, gen.GenerateRole())
					}
					return roles, err
				}).
			AnyTimes()

		mockService.
			EXPECT().
			Count().
			DoAndReturn(
				func() (uint64, error) {
					rand.New(rand.NewSource(time.Now().UnixNano()))
					return rand.Uint64(), err
				}).
			AnyTimes()
	}
	return mockService
}
