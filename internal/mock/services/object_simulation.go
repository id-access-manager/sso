package services

import (
	"github.com/golang/mock/gomock"
	"github.com/google/uuid"
	"math/rand"
	"sso/internal/mock/gen"
	"sso/internal/models"
	"time"
)

func SimulateObject(controller *gomock.Controller, err error) *MockobjectService {
	mockService := NewMockobjectService(controller)
	{
		mockService.
			EXPECT().
			Create(gomock.Any()).
			Return(gen.GenerateObject(), err).
			AnyTimes()

		mockService.
			EXPECT().
			GetByID(gomock.AssignableToTypeOf(uuid.UUID{})).
			Return(gen.GenerateObject(), err).
			AnyTimes()

		mockService.
			EXPECT().
			GetObjectTotal().
			DoAndReturn(
				func() (uint64, error) {
					rand.New(rand.NewSource(time.Now().UnixNano()))
					return rand.Uint64(), nil
				}).
			AnyTimes()

		mockService.
			EXPECT().
			GetList(gomock.Any(), gomock.Any(), gomock.Any()).
			DoAndReturn(
				func(limit int, offset int, sort string) ([]models.Object, error) {
					var objs []models.Object
					for i := 0; i < limit; i++ {
						objs = append(objs, gen.GenerateObject())
					}
					return objs, err
				}).
			AnyTimes()

		mockService.
			EXPECT().
			UpdateByID(gomock.AssignableToTypeOf(uuid.UUID{}), gomock.Any()).
			DoAndReturn(
				func(id uuid.UUID, name string) (models.Object, error) {
					obj := gen.GenerateObject()
					obj.ID = id
					obj.Name = name
					return obj, err
				}).
			AnyTimes()

		mockService.
			EXPECT().
			DeleteByID(gomock.AssignableToTypeOf(uuid.UUID{})).
			Return(err).
			AnyTimes()
	}
	return mockService
}
