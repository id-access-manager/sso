package services

import (
	"github.com/golang/mock/gomock"
	"github.com/google/uuid"
	"math/rand"
	"sso/internal/mock/gen"
	"sso/internal/models"
	"time"
)

func SimulateAction(controller *gomock.Controller, err error) *MockactionService {
	actionService := NewMockactionService(controller)
	{
		var action models.Action
		actionService.
			EXPECT().
			GetByID(gomock.AssignableToTypeOf(uuid.UUID{})).
			Return(gen.GenerateAction(), err).
			AnyTimes()

		actionService.
			EXPECT().
			Create(gomock.Any(), gomock.Any()).
			Return(gen.GenerateAction(), err).
			AnyTimes()

		actionService.
			EXPECT().
			UpdateByID(gomock.AssignableToTypeOf(uuid.UUID{}), gomock.Any(), gomock.Any()).
			Do(
				func(id uuid.UUID, name string, actionType models.ActionType) {
					action = gen.GenerateAction()
					action.ID = id
					action.Name = name
					action.ActionType = actionType
				}).
			Return(action, err).
			AnyTimes()

		actionService.
			EXPECT().
			DeleteByID(gomock.AssignableToTypeOf(uuid.UUID{})).
			Return(err).
			AnyTimes()

		actionService.
			EXPECT().
			GetList(gomock.Any(), gomock.Any(), gomock.Any()).
			DoAndReturn(
				func(limit int, offset int, sort string) ([]models.Action, error) {
					var objs []models.Action
					for i := 0; i < limit; i++ {
						objs = append(objs, gen.GenerateAction())
					}
					return objs, err
				}).
			AnyTimes()

		actionService.
			EXPECT().
			GetActionsTotal().
			DoAndReturn(
				func() (uint64, error) {
					rand.New(rand.NewSource(time.Now().UnixNano()))
					return rand.Uint64(), nil
				}).
			AnyTimes()
	}
	return actionService
}
