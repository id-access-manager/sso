package services

import (
	"github.com/golang/mock/gomock"
	"github.com/google/uuid"
	"math/rand"
	"sso/internal/mock/gen"
	"sso/internal/models"
	"time"
)

func SimulateResource(controller *gomock.Controller, err error) *MockresourceService {
	mockService := NewMockresourceService(controller)
	{
		mockService.
			EXPECT().
			Create(gomock.Any(), gomock.Any(), gomock.AssignableToTypeOf([]uuid.UUID{})).
			DoAndReturn(
				func(name string, description string, ids []uuid.UUID) (models.Resource, error) {
					var roles []models.Role

					resource := gen.GenerateResource()
					resource.Name = name
					resource.Description = description

					for i := 0; i < len(ids); i++ {
						roles = append(roles, gen.GenerateRole())
					}

					resource.Roles = roles

					return resource, err
				}).
			AnyTimes()

		mockService.
			EXPECT().
			GetByID(gomock.AssignableToTypeOf(uuid.UUID{})).
			DoAndReturn(
				func(id uuid.UUID) (models.Resource, error) {
					resource := gen.GenerateResource()
					resource.ID = id
					return resource, err
				}).
			AnyTimes()

		mockService.
			EXPECT().
			DeleteByID(gomock.AssignableToTypeOf(uuid.UUID{})).
			Return(err).
			AnyTimes()

		mockService.
			EXPECT().
			UpdateByID(
				gomock.AssignableToTypeOf(uuid.UUID{}),
				gomock.Any(), gomock.Any(),
				gomock.AssignableToTypeOf([]uuid.UUID{}),
			).
			DoAndReturn(
				func(id uuid.UUID, name string, description string, rolesIDs []uuid.UUID) (models.Resource, error) {
					var roles []models.Role

					resource := gen.GenerateResource()
					resource.ID = id
					resource.Name = name
					resource.Description = description

					for i := 0; i < len(rolesIDs); i++ {
						roles = append(roles, gen.GenerateRole())
					}

					resource.Roles = roles

					return resource, err
				}).
			AnyTimes()

		mockService.
			EXPECT().
			Count().
			DoAndReturn(
				func() (uint64, error) {
					rand.New(rand.NewSource(time.Now().UnixNano()))
					return rand.Uint64(), nil
				}).
			AnyTimes()

		mockService.
			EXPECT().
			GetList(gomock.Any(), gomock.Any(), gomock.Any()).
			DoAndReturn(
				func(limit int, offset int, sort string) ([]models.Resource, error) {
					var resources []models.Resource
					for i := 0; i < limit; i++ {
						resources = append(resources, gen.GenerateResource())
					}
					return resources, err
				}).
			AnyTimes()
	}
	return mockService
}
