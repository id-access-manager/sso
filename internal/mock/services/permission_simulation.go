package services

import (
	"github.com/golang/mock/gomock"
	"github.com/google/uuid"
	"math/rand"
	"sso/internal/mock/gen"
	"sso/internal/models"
	"time"
)

func SimulatePermission(controller *gomock.Controller, err error) *MockpermissionService {
	mockService := NewMockpermissionService(controller)
	{
		mockService.
			EXPECT().
			Create(gomock.Any(), gomock.AssignableToTypeOf(uuid.UUID{}), gomock.AssignableToTypeOf(uuid.UUID{})).
			DoAndReturn(
				func(name string, actionID uuid.UUID, objectID uuid.UUID) (models.Permission, error) {
					permission := gen.GeneratePermission()
					permission.Name = name
					permission.ActionID = actionID
					permission.ObjectID = objectID
					return permission, err
				}).
			AnyTimes()

		mockService.
			EXPECT().
			GetByID(gomock.AssignableToTypeOf(uuid.UUID{})).
			DoAndReturn(
				func(id uuid.UUID) (models.Permission, error) {
					permission := gen.GeneratePermission()
					permission.ID = id
					return permission, err
				}).
			AnyTimes()

		mockService.
			EXPECT().
			GetList(gomock.Any(), gomock.Any(), gomock.Any()).
			DoAndReturn(
				func(limit int, offset int, sort string) ([]models.Permission, error) {
					var perms []models.Permission
					for i := 0; i < limit; i++ {
						perms = append(perms, gen.GeneratePermission())
					}
					return perms, err
				}).
			AnyTimes()

		mockService.
			EXPECT().
			GetPermissionsTotal().
			DoAndReturn(
				func() (uint64, error) {
					rand.New(rand.NewSource(time.Now().UnixNano()))
					return rand.Uint64(), nil
				}).
			AnyTimes()

		mockService.
			EXPECT().
			UpdateByID(
				gomock.AssignableToTypeOf(uuid.UUID{}),
				gomock.Any(),
				gomock.AssignableToTypeOf(uuid.UUID{}),
				gomock.AssignableToTypeOf(uuid.UUID{})).
			DoAndReturn(
				func(id uuid.UUID, name string, actionID uuid.UUID, objectID uuid.UUID) (models.Permission, error) {
					permission := gen.GeneratePermission()
					permission.ID = id
					permission.Name = name
					permission.ActionID = actionID
					permission.ObjectID = objectID
					return permission, err
				}).
			AnyTimes()

		mockService.
			EXPECT().
			DeleteByID(gomock.AssignableToTypeOf(uuid.UUID{})).
			Return(err).
			AnyTimes()
	}
	return mockService
}
