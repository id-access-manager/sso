package services

import (
	"github.com/golang/mock/gomock"
	"github.com/google/uuid"
	"math/rand"
	"sso/internal/mock/gen"
	"sso/internal/models"
	"time"
)

func SimulateRole(controller *gomock.Controller, err error) *MockroleService {
	mockService := NewMockroleService(controller)
	{
		mockService.EXPECT().Create(gomock.Any(), gomock.AssignableToTypeOf([]uuid.UUID{})).DoAndReturn(
			func(name string, ids []uuid.UUID) (models.Role, error) {
				role := gen.GenerateRole()
				for i := 0; i < len(ids); i++ {
					role.Permissions = append(role.Permissions, gen.GeneratePermission())
				}
				return role, err
			}).AnyTimes()

		mockService.EXPECT().GetByID(gomock.AssignableToTypeOf(uuid.UUID{})).DoAndReturn(
			func(id uuid.UUID) (models.Role, error) {
				role := gen.GenerateRole()
				role.ID = id
				return role, err
			}).AnyTimes()

		mockService.
			EXPECT().
			Count().
			DoAndReturn(
				func() (uint64, error) {
					rand.New(rand.NewSource(time.Now().UnixNano()))
					return rand.Uint64(), err
				}).
			AnyTimes()

		mockService.
			EXPECT().
			GetAll(gomock.Any(), gomock.Any(), gomock.Any()).
			DoAndReturn(
				func(limit int, offset int, sort string) ([]models.Role, error) {
					var roles []models.Role
					for i := 0; i < limit; i++ {
						roles = append(roles, gen.GenerateRole())
					}
					return roles, err
				}).
			AnyTimes()

		mockService.
			EXPECT().
			UpdateByID(
				gomock.AssignableToTypeOf(uuid.UUID{}),
				gomock.Any(),
				gomock.AssignableToTypeOf([]uuid.UUID{}),
			).
			DoAndReturn(
				func(id uuid.UUID, name string, rolesIDs []uuid.UUID) (models.Role, error) {
					var permissions []models.Permission
					role := gen.GenerateRole()
					role.ID = id
					role.Name = name

					for i := 0; i < len(rolesIDs); i++ {
						permissions = append(permissions, gen.GeneratePermission())
					}

					role.Permissions = permissions

					return role, err
				}).
			AnyTimes()

		mockService.EXPECT().DeleteByID(gomock.AssignableToTypeOf(uuid.UUID{})).Return(err).AnyTimes()
	}
	return mockService
}
