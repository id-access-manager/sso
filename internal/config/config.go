// Package config is provided configuration data for app
package config

import (
	"flag"
	"fmt"
	"github.com/ilyakaznacheev/cleanenv"
	"os"
	"time"
)

// DatabaseName of using in app database
type DatabaseName string

const (
	SQLiteDBName   DatabaseName = "sqlite"   // use sqlite database in app
	PostgresDBName DatabaseName = "postgres" // use postgres database in app
)

// EnvType of app environment type
type EnvType string

const (
	EnvLocal EnvType = "local"      // app running in local machine
	EnvDev   EnvType = "develop"    // app running in development environment
	EnvProd  EnvType = "production" // app running in production environment
)

// Config application configuration
type Config struct {
	Env        EnvType        `yaml:"env" env-default:"local"`          // application environment
	Database   DatabaseConfig `yaml:"database" env-required:"true"`     // database configuration
	HTTP       HTTPConfig     `yaml:"http"`                             // HTTP server configuration
	TimeToLive TimeToLive     `yaml:"time_to_live" env-required:"true"` // TTL of entities with limited time to live
}

// DatabaseConfig database configuration
type DatabaseConfig struct {
	DSN  string       `yaml:"dsn" env-required:"true"`
	Name DatabaseName `yaml:"name" env-required:"true"`
}

// HTTPConfig app gRPC server configuration
type HTTPConfig struct {
	Host    string        `yaml:"host" env-default:"127.0.0.1"`
	Port    int           `yaml:"port" env-default:"5001"`
	Timeout time.Duration `yaml:"timeout"`
}

type TimeToLive struct {
	Token struct {
		Access   time.Duration `yaml:"access"`   // TTL for access token
		Identity time.Duration `yaml:"identity"` // TTL for identity token
		Refresh  time.Duration `yaml:"refresh"`  // TTL for refresh token
	} `yaml:"token"` // TTL for tokens
	Session time.Duration `yaml:"session"` // TTL for user session
}

func (c HTTPConfig) GetAddress() string {
	//return fmt.Sprintf("%s:%d", c.Host, c.Port)
	return fmt.Sprintf(":%d", c.Port)
}

// MustLoad required loading app configs
//
// Panic if loading was failed
func MustLoad() *Config {
	path := fetchConfigPath()

	if path == "" {
		panic("config path is empty")
	}

	if _, err := os.Stat(path); os.IsNotExist(err) {
		panic("config path file does not exist: " + path)
	}

	var cfg Config

	if err := cleanenv.ReadConfig(path, &cfg); err != nil {
		panic("failed to read config: " + err.Error())
	}

	return &cfg
}

// fetchConfigPath
//
// Priority: flag > env > default
func fetchConfigPath() string {
	var res string

	flag.StringVar(&res, "config", "", "path to config file")
	flag.Parse()

	if res == "" {
		res = os.Getenv("CONFIG_PATH")
	}

	return res
}
