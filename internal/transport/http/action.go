//go:generate mockgen -source=action.go -destination=../../mock/services/action.go -package=services
//go:generate swag fmt && swag init -o ./api -g internal/transport/http/resource.go
package http

import (
	"errors"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"net/http"
	apperr "sso/internal/consts/errors"
	"sso/internal/models"
	"sso/internal/pkg/pg"
	"sso/internal/pkg/utils"
	"sso/internal/transport/http/schemas"
)

type actionService interface {
	Create(name string, actionName models.ActionType) (action models.Action, err error)
	GetByID(id uuid.UUID) (action models.Action, err error)
	GetList(limit int, offset int, sort string) (actions []models.Action, err error)
	GetActionsTotal() (int64, error)
	UpdateByID(id uuid.UUID, name string, actionName models.ActionType) (action models.Action, err error)
	DeleteByID(id uuid.UUID) error
}

type ActionServer struct {
	service actionService
}

func NewActionServer(service actionService) *ActionServer {
	return &ActionServer{
		service: service,
	}
}

func (as *ActionServer) RegisterAdminRouts(adminGroup *gin.RouterGroup) {
	actionAdminRouts := adminGroup.Group("/action")
	{
		actionAdminRouts.POST("/", as.create)
		actionAdminRouts.GET("/:action_id", as.get)
		actionAdminRouts.GET("/list", as.list)
		actionAdminRouts.PATCH("/:action_id", as.update)
		actionAdminRouts.DELETE("/:action_id", as.delete)
	}
}

// get godoc
//
//	@Summary		Получить действие
//	@Description	Получение действия по иентификатору
//	@Tags			action
//	@Accept			json
//	@Produce		json
//
//	@Param			action_id	path		string	true	"Action ID"
//
//	@Success		200			{object}	models.Action
//	@Failure		400			{object}	utils.HTTPError
//	@Failure		404			{object}	utils.HTTPError
//	@Failure		500			{object}	utils.HTTPError
//
//	@Router			/admin/action/{action_id} [get]
func (as *ActionServer) get(c *gin.Context) {
	var action models.Action

	actionId, err := uuid.Parse(c.Param("action_id"))
	if err != nil {
		utils.NewError(c, http.StatusBadRequest, apperr.ErrInvalidIDType)
		return
	}

	action, err = as.service.GetByID(actionId)
	if err != nil {
		utils.NewError(c, http.StatusNotFound, apperr.ErrActionNotFound)
		return
	}

	c.JSON(http.StatusOK, action)
}

// create godoc
//
//	@Summary		Создание действия
//	@Description	Создание действия
//	@Tags			action
//	@Accept			json
//	@Produce		json
//
//	@Param			Action	body		actionRequest	true	"Action"
//
//	@Success		200		{object}	models.Action
//	@Failure		400		{object}	utils.HTTPError
//	@Failure		404		{object}	utils.HTTPError
//	@Failure		500		{object}	utils.HTTPError
//
//	@Router			/admin/action/ [post]
func (as *ActionServer) create(c *gin.Context) {
	var req schemas.ActionRequest
	var action models.Action

	if err := c.ShouldBindJSON(&req); err != nil {
		utils.NewError(c, http.StatusBadRequest, err)
		return
	}

	if err := req.Action.Validate(); err != nil {
		utils.NewError(c, http.StatusBadRequest, err)
		return
	}

	action, err := as.service.Create(req.Name, req.Action)
	if err != nil {
		utils.NewError(c, http.StatusBadRequest, err)
		return
	}

	c.JSON(http.StatusCreated, action)
	return
}

// update godoc
//
//	@Summary		Обновить действия
//	@Description	Создание действия по идентификатору
//	@Tags			action
//	@Accept			json
//	@Produce		json
//
//	@Param			action_id	path		string			true	"Action ID"
//	@Param			Action		body		actionRequest	true	"Action"
//
//	@Success		200			{object}	models.Action
//	@Failure		400			{object}	utils.HTTPError
//	@Failure		404			{object}	utils.HTTPError
//	@Failure		500			{object}	utils.HTTPError
//
//	@Router			/admin/action/{action_id} [patch]
func (as *ActionServer) update(c *gin.Context) {
	var req schemas.ActionRequest
	var action models.Action

	id, err := uuid.Parse(c.Param("action_id"))
	if err != nil {
		utils.NewError(c, http.StatusBadRequest, apperr.ErrInvalidIDType)
		return
	}

	if err := c.ShouldBindJSON(&req); err != nil {
		utils.NewError(c, http.StatusBadRequest, err)
		return
	}

	if err := req.Action.Validate(); err != nil {
		utils.NewError(c, http.StatusBadRequest, err)
		return
	}

	action, err = as.service.UpdateByID(id, req.Name, req.Action)
	if err != nil {
		utils.NewError(c, http.StatusBadRequest, err)
		return
	}

	c.JSON(http.StatusOK, action)
	return
}

// delete godoc
//
//	@Summary		Удалить действие
//	@Description	Создание действия
//	@Tags			action
//	@Accept			json
//	@Produce		json
//
//	@Param			action_id	path		string	true	"Action ID"
//
//	@Success		200			{object}	models.Action
//	@Failure		400			{object}	utils.HTTPError
//	@Failure		404			{object}	utils.HTTPError
//	@Failure		500			{object}	utils.HTTPError
//
//	@Router			/admin/action/{action_id} [delete]
func (as *ActionServer) delete(c *gin.Context) {
	id, err := uuid.Parse(c.Param("action_id"))
	if err != nil {
		utils.NewError(c, http.StatusBadRequest, apperr.ErrInvalidIDType)
		return
	}

	if err := as.service.DeleteByID(id); err != nil {

		if errors.Is(err, apperr.ErrActionNotFound) {
			utils.NewError(c, http.StatusNotFound, apperr.ErrActionNotFound)
			return
		}

		utils.NewError(c, http.StatusBadRequest, err)
		return
	}

	c.JSON(http.StatusNoContent, nil)
	return
}

// list godoc
//
//	@Summary		Получить все действия
//	@Description	Создание действия
//	@Tags			action
//	@Accept			json
//	@Produce		json
//
//	@Param			page		query		int	false	"Page Number"
//	@Param			page_size	query		int	false	"Page Size"
//
//	@Success		200			{object}	models.Action
//	@Failure		400			{object}	utils.HTTPError
//	@Failure		404			{object}	utils.HTTPError
//	@Failure		500			{object}	utils.HTTPError
//
//	@Router			/admin/action/list [get]
func (as *ActionServer) list(c *gin.Context) {
	var req pg.Request

	if err := c.ShouldBind(&req); err != nil {
		utils.NewError(c, http.StatusBadRequest, err)
		return
	}

	objects, err := as.service.GetList(req.GetLimit(), req.GetOffset(), req.GetSort())
	if err != nil {
		utils.NewError(c, http.StatusBadRequest, err)
		return
	}

	objectTotal, err := as.service.GetActionsTotal()
	if err != nil {
		utils.NewError(c, http.StatusBadRequest, err)
		return
	}

	res := pg.NewResponse(objects, int64(len(objects)), objectTotal, &req)
	c.JSON(http.StatusOK, res)

	return
}
