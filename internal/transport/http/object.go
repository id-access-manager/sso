//go:generate mockgen -source=object.go -destination=../../mock/services/object.go -package=services
//go:generate swag fmt && swag init -o ./api -g internal/transport/http/resource.go
package http

import (
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"net/http"
	apperr "sso/internal/consts/errors"
	"sso/internal/models"
	"sso/internal/pkg/pg"
	"sso/internal/pkg/utils"
	"sso/internal/transport/http/schemas"
)

type objectService interface {
	Create(name string) (object models.Object, err error)
	GetByID(id uuid.UUID) (object models.Object, err error)
	GetList(limit int, offset int, sort string) (objects []models.Object, err error)
	GetObjectTotal() (int64, error)
	UpdateByID(id uuid.UUID, name string) (object models.Object, err error)
	DeleteByID(id uuid.UUID) error
}

type ObjectServer struct {
	service objectService
}

func NewObjectServer(service objectService) *ObjectServer {
	return &ObjectServer{
		service: service,
	}
}

func (srv *ObjectServer) RegisterAdminRouts(adminGroup *gin.RouterGroup) {
	objectAdminGroup := adminGroup.Group("/object")
	{
		objectAdminGroup.POST("/", srv.create)
		objectAdminGroup.GET("/:obj_id", srv.get)
		objectAdminGroup.GET("/list", srv.list)
		objectAdminGroup.PATCH("/:obj_id", srv.update)
		objectAdminGroup.DELETE("/:obj_id", srv.delete)
	}
}

// get godoc
//
//	@Summary	Создать объект
//	@Tags		object
//	@Accept		json
//	@Produce	json
//
//	@Param		object	body		objectRequest	true	"Object"
//
//	@Success	200		{object}	models.Object
//	@Failure	400		{object}	utils.HTTPError
//	@Failure	404		{object}	utils.HTTPError
//	@Failure	500		{object}	utils.HTTPError
//
//	@Router		/admin/object/ [post]
func (srv *ObjectServer) create(c *gin.Context) {
	var (
		req schemas.ObjectRequest
		obj models.Object
		err error
	)

	if err = c.ShouldBindQuery(&req); err != nil {
		utils.NewError(c, http.StatusBadRequest, err)
		return
	}

	if obj, err = srv.service.Create(req.Name); err != nil {
		utils.NewError(c, http.StatusBadRequest, err)
		return
	}

	c.JSON(http.StatusCreated, obj)
	return
}

// get godoc
//
//	@Summary	Получить объект
//	@Tags		object
//	@Accept		json
//	@Produce	json
//
//	@Param		obj_id	path		string	true	"Object ID"
//
//	@Success	200		{object}	models.Object
//	@Failure	400		{object}	utils.HTTPError
//	@Failure	404		{object}	utils.HTTPError
//	@Failure	500		{object}	utils.HTTPError
//
//	@Router		/admin/object/{obj_id} [get]
func (srv *ObjectServer) get(c *gin.Context) {
	var obj models.Object

	objID, err := uuid.Parse(c.Param("obj_id"))
	if err != nil {
		utils.NewError(c, http.StatusBadRequest, apperr.ErrInvalidIDType)
		return
	}

	obj, err = srv.service.GetByID(objID)
	if err != nil {
		utils.NewError(c, http.StatusNotFound, err)
		return
	}

	c.JSON(http.StatusOK, obj)
	return
}

// list godoc
//
//	@Summary	Получить список объектов
//	@Tags		object
//	@Accept		json
//	@Produce	json
//
//	@Param		limit	query		int	false	"Page size"
//	@Param		page	query		int	false	"Page"
//
//	@Success	200		{object}	models.Object
//	@Failure	400		{object}	utils.HTTPError
//	@Failure	404		{object}	utils.HTTPError
//	@Failure	500		{object}	utils.HTTPError
//
//	@Router		/admin/object/list [get]
func (srv *ObjectServer) list(c *gin.Context) {
	var req pg.Request

	if err := c.ShouldBind(&req); err != nil {
		utils.NewError(c, http.StatusBadRequest, err)
		return
	}

	objects, err := srv.service.GetList(req.GetLimit(), req.GetOffset(), req.GetSort())
	if err != nil {
		utils.NewError(c, http.StatusBadRequest, err)
		return
	}

	objectTotal, err := srv.service.GetObjectTotal()
	if err != nil {
		utils.NewError(c, http.StatusBadRequest, err)
		return
	}

	res := pg.NewResponse(objects, int64(len(objects)), objectTotal, &req)
	c.JSON(http.StatusOK, res)

	return
}

// update godoc
//
//	@Summary	Обновить объект
//	@Tags		object
//	@Accept		json
//	@Produce	json
//
//	@Param		obj_id	path		string			true	"Object ID"
//	@Param		object	body		objectRequest	true	"Object"
//
//	@Success	200		{object}	models.Object
//	@Failure	400		{object}	utils.HTTPError
//	@Failure	404		{object}	utils.HTTPError
//	@Failure	500		{object}	utils.HTTPError
//
//	@Router		/admin/object/{obj_id} [patch]
func (srv *ObjectServer) update(c *gin.Context) {
	var req schemas.ObjectRequest

	objectID, err := uuid.Parse(c.Param("obj_id"))
	if err != nil {
		utils.NewError(c, http.StatusBadRequest, apperr.ErrInvalidIDType)
		return
	}

	if err := c.ShouldBindQuery(&req); err != nil {
		utils.NewError(c, http.StatusBadRequest, err)
		return
	}

	object, err := srv.service.UpdateByID(objectID, req.Name)
	if err != nil {
		utils.NewError(c, http.StatusNotFound, apperr.ErrObjectNotFound)
		return
	}

	c.JSON(http.StatusOK, object)
	return
}

// delete godoc
//
//	@Summary	Удалить объект
//	@Tags		object
//	@Accept		json
//	@Produce	json
//
//	@Param		obj_id	path		string	true	"Object ID"
//
//	@Success	200		{object}	models.Object
//	@Failure	400		{object}	utils.HTTPError
//	@Failure	404		{object}	utils.HTTPError
//	@Failure	500		{object}	utils.HTTPError
//
//	@Router		/admin/object/{obj_id} [delete]
func (srv *ObjectServer) delete(c *gin.Context) {
	objID, err := uuid.Parse(c.Param("obj_id"))
	if err != nil {
		utils.NewError(c, http.StatusBadRequest, apperr.ErrInvalidIDType)
		return
	}

	if err = srv.service.DeleteByID(objID); err != nil {
		utils.NewError(c, http.StatusNotFound, err)
		return
	}

	c.JSON(http.StatusNoContent, nil)
	return
}
