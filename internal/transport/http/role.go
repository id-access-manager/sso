//go:generate mockgen -source=role.go -destination=../../mock/services/role.go -package=services
//go:generate swag fmt && swag init -o ./api -g internal/transport/http/role.go
package http

import (
	"errors"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"net/http"
	apperr "sso/internal/consts/errors"
	"sso/internal/models"
	"sso/internal/pkg/pg"
	"sso/internal/pkg/utils"
	"sso/internal/transport/http/schemas"
)

type roleService interface {
	Create(name string, permissionIDs []uuid.UUID) (models.Role, error)
	GetByID(id uuid.UUID) (role models.Role, err error)
	GetAll(limit int, offset int, sort string) (roles []models.Role, err error)
	Count() (int64, error)
	UpdateByID(id uuid.UUID, name string, permissionIDs []uuid.UUID) (models.Role, error)
	DeleteByID(id uuid.UUID) error
}

type RoleServer struct {
	service roleService
}

func NewRoleServer(service roleService) *RoleServer {
	return &RoleServer{
		service: service,
	}
}

func (srv *RoleServer) RegisterAdminRouts(adminGroup *gin.RouterGroup) {
	roleAdminRouter := adminGroup.Group("/role")
	{
		roleAdminRouter.POST("/", srv.create)
		roleAdminRouter.GET("/:role_id", srv.get)
		roleAdminRouter.GET("/list", srv.list)
		roleAdminRouter.PATCH("/:role_id", srv.update)
		roleAdminRouter.DELETE("/:role_id", srv.delete)
	}
}

// create godoc
//
//	@Summary	Создать роль
//	@Tags		role
//	@Accept		json
//	@Produce	json
//
//	@Param		role	body		roleRequest	true	"Role"
//
//	@Success	200		{object}	models.Role
//	@Failure	400		{object}	utils.HTTPError
//	@Failure	404		{object}	utils.HTTPError
//	@Failure	500		{object}	utils.HTTPError
//
//	@Router		/admin/role/ [post]
func (srv *RoleServer) create(c *gin.Context) {
	var req schemas.RoleRequest
	if err := c.ShouldBind(&req); err != nil {
		utils.NewError(c, http.StatusBadRequest, err)
		return
	}

	role, err := srv.service.Create(req.Name, req.Permissions)
	if err != nil {
		utils.NewError(c, http.StatusBadRequest, err)
		return
	}

	c.JSON(http.StatusCreated, role)
	return
}

// get godoc
//
//	@Summary	Получить роль
//	@Tags		role
//	@Accept		json
//	@Produce	json
//
//	@Param		role_id	path		string	true	"Role ID"
//
//	@Success	200		{object}	models.Role
//	@Failure	400		{object}	utils.HTTPError
//	@Failure	404		{object}	utils.HTTPError
//	@Failure	500		{object}	utils.HTTPError
//
//	@Router		/admin/role/{role_id} [get]
func (srv *RoleServer) get(c *gin.Context) {
	id, err := uuid.Parse(c.Param("role_id"))
	if err != nil {
		utils.NewError(c, http.StatusBadRequest, apperr.ErrInvalidIDType)
		return
	}

	role, err := srv.service.GetByID(id)
	if err != nil {
		utils.NewError(c, http.StatusNotFound, err)
		return
	}

	c.JSON(http.StatusOK, role)
	return
}

// list godoc
//
//	@Summary	Получить список ролей
//	@Tags		role
//	@Accept		json
//	@Produce	json
//
//	@Param		page		query		string	true	"Page number"
//	@Param		page_size	query		string	true	"Page Size"
//
//	@Success	200			{object}	models.Role
//	@Failure	400			{object}	utils.HTTPError
//	@Failure	404			{object}	utils.HTTPError
//	@Failure	500			{object}	utils.HTTPError
//
//	@Router		/admin/role/list [get]
func (srv *RoleServer) list(c *gin.Context) {
	var request pg.Request

	if err := c.ShouldBindQuery(&request); err != nil {
		utils.NewError(c, http.StatusBadRequest, err)
		return
	}

	permissions, err := srv.service.GetAll(request.GetLimit(), request.GetOffset(), request.GetSort())
	if err != nil {
		utils.NewError(c, http.StatusInternalServerError, err)
		return
	}

	total, err := srv.service.Count()
	if err != nil {
		utils.NewError(c, http.StatusInternalServerError, err)
		return
	}

	response := pg.NewResponse(permissions, int64(len(permissions)), total, &request)

	c.JSON(http.StatusOK, response)
	return
}

// update godoc
//
//	@Summary	Обновить роль
//	@Tags		role
//	@Accept		json
//	@Produce	json
//
//	@Param		role	body		roleRequest	true	"Role"
//	@Param		role_id	path		string		true	"Role ID"
//
//	@Success	200		{object}	models.Role
//	@Failure	400		{object}	utils.HTTPError
//	@Failure	404		{object}	utils.HTTPError
//	@Failure	500		{object}	utils.HTTPError
//
//	@Router		/admin/role/{role_id} [patch]
func (srv *RoleServer) update(c *gin.Context) {
	var req schemas.RoleRequest

	id, err := uuid.Parse(c.Param("role_id"))
	if err != nil {
		utils.NewError(c, http.StatusBadRequest, apperr.ErrInvalidIDType)
		return
	}

	if err := c.ShouldBindJSON(&req); err != nil {
		utils.NewError(c, http.StatusBadRequest, err)
		return
	}

	resource, err := srv.service.UpdateByID(id, req.Name, req.Permissions)
	if err != nil {
		if errors.Is(err, apperr.ErrRoleNotFound) {
			utils.NewError(c, http.StatusNotFound, err)
			return
		}
		utils.NewError(c, http.StatusBadRequest, err)
		return
	}

	c.JSON(http.StatusOK, resource)
	return
}

// delete godoc
//
//	@Summary	Удалить роль
//	@Tags		role
//	@Accept		json
//	@Produce	json
//
//	@Param		role_id	path		string	true	"Role ID"
//
//	@Success	200		{object}	models.Role
//	@Failure	400		{object}	utils.HTTPError
//	@Failure	404		{object}	utils.HTTPError
//	@Failure	500		{object}	utils.HTTPError
//
//	@Router		/admin/role/{role_id} [delete]
func (srv *RoleServer) delete(c *gin.Context) {
	id, err := uuid.Parse(c.Param("role_id"))
	if err != nil {
		utils.NewError(c, http.StatusBadRequest, apperr.ErrInvalidIDType)
		return
	}

	if err := srv.service.DeleteByID(id); err != nil {
		utils.NewError(c, http.StatusNotFound, err)
		return
	}

	c.JSON(http.StatusNoContent, nil)
	return
}
