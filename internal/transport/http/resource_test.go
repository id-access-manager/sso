package http

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/golang/mock/gomock"
	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"gorm.io/gorm"
	"net/http"
	"net/http/httptest"
	apperr "sso/internal/consts/errors"
	"sso/internal/mock/gen"
	msrv "sso/internal/mock/services"
	"sso/internal/models"
	"sso/internal/pkg/pg"
	testpkg "sso/internal/pkg/test"
	"sso/internal/transport/http/schemas"
	"strconv"
	"testing"
)

func initResourceServer(router *gin.Engine, service resourceService) {
	s := NewResourceServer(service)
	adminGroup := router.Group("/admin")
	s.RegisterAdminRouts(adminGroup)
}

func TestResourceServer_create(t *testing.T) {
	const target = "/admin/resource/"
	const method = http.MethodPost

	ctr := gomock.NewController(t)
	defer ctr.Finish()

	tests := []struct {
		name     string
		service  resourceService
		data     schemas.ResourceRequest
		wantCode int
		wantErr  bool
		err      error
	}{
		{
			name:    "success",
			service: msrv.SimulateResource(ctr, nil),
			data: schemas.ResourceRequest{
				Name:        gen.GenerateString(),
				Description: gen.GenerateString(),
			},
			wantCode: http.StatusCreated,
			wantErr:  false,
		},
		{
			name:    "db err",
			service: msrv.SimulateResource(ctr, gorm.ErrInvalidDB),
			data: schemas.ResourceRequest{
				Name:        gen.GenerateString(),
				Description: gen.GenerateString(),
			},
			wantCode: http.StatusBadRequest,
			wantErr:  true,
			err:      gorm.ErrInvalidDB,
		},
	}

	for _, test := range tests {
		t.Run(test.name,
			func(t *testing.T) {
				router := testpkg.GetRouter()
				w := httptest.NewRecorder()
				initResourceServer(router, test.service)

				jsonData, _ := json.Marshal(test.data)
				req, _ := http.NewRequest(method, target, bytes.NewBuffer(jsonData))
				router.ServeHTTP(w, req)

				res := w.Result()

				assert.Equal(t, test.wantCode, res.StatusCode)

				if test.wantErr {
					testpkg.AssertHTTPError(t, test.wantCode, test.err, w)
				} else {
					var resource models.Resource
					_ = json.Unmarshal(w.Body.Bytes(), &resource)

					assert.NotEmpty(t, resource)
					assert.NotEmpty(t, resource.ID)

					assert.Equal(t, test.data.Name, resource.Name)
					assert.Equal(t, test.data.Description, resource.Description)
				}
			},
		)
	}
}

func TestResourceServer_get(t *testing.T) {
	const target = "/admin/resource"
	const method = http.MethodGet

	ctr := gomock.NewController(t)
	defer ctr.Finish()

	tests := []struct {
		name     string
		service  resourceService
		id       string
		wantCode int
		wantErr  bool
		err      error
	}{
		{
			name:     "exist resource",
			service:  msrv.SimulateResource(ctr, nil),
			id:       uuid.NewString(),
			wantCode: http.StatusOK,
			wantErr:  false,
		},
		{
			name:     "invalid id",
			service:  msrv.SimulateResource(ctr, nil),
			id:       gen.GenerateString(),
			wantCode: http.StatusBadRequest,
			wantErr:  true,
			err:      apperr.ErrInvalidIDType,
		},
		{
			name:     "not found",
			service:  msrv.SimulateResource(ctr, apperr.ErrResourceNotFound),
			id:       uuid.NewString(),
			wantCode: http.StatusNotFound,
			wantErr:  true,
			err:      apperr.ErrResourceNotFound,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			router := testpkg.GetRouter()
			w := httptest.NewRecorder()

			initResourceServer(router, test.service)

			req, _ := http.NewRequest(method, fmt.Sprintf("%s/%s", target, test.id), nil)
			router.ServeHTTP(w, req)

			res := w.Result()

			assert.Equal(t, test.wantCode, res.StatusCode)

			if test.wantErr {
				testpkg.AssertHTTPError(t, test.wantCode, test.err, w)
			} else {
				var resource models.Resource
				_ = json.Unmarshal(w.Body.Bytes(), &resource)

				assert.NotEmpty(t, resource)
				assert.Equal(t, test.id, resource.ID.String())
			}
		})
	}
}

func TestResourceServer_list(t *testing.T) {
	const target = "/admin/resource/list"
	const method = http.MethodGet

	ctr := gomock.NewController(t)
	defer ctr.Finish()

	tests := []struct {
		name           string
		service        resourceService
		statusCode     int
		withPagination bool
		pagination     pg.Request
		wantRes        pg.Response
	}{
		{
			name:           "without pagination",
			service:        msrv.SimulateResource(ctr, nil),
			statusCode:     http.StatusOK,
			withPagination: false,
		},
		{
			name:           "with pagination 1",
			service:        msrv.SimulateResource(ctr, nil),
			statusCode:     http.StatusOK,
			withPagination: true,
			pagination: pg.Request{
				Page:     1,
				PageSize: 1,
			},
		},
		{
			name:           "with pagination 2",
			service:        msrv.SimulateResource(ctr, nil),
			statusCode:     http.StatusOK,
			withPagination: true,
			pagination: pg.Request{
				Page:     0,
				PageSize: 0,
			},
		},
		{
			name:           "with pagination 3",
			service:        msrv.SimulateResource(ctr, nil),
			statusCode:     http.StatusOK,
			withPagination: true,
			pagination: pg.Request{
				Page:     12,
				PageSize: 0,
			},
		},
		{
			name:           "with pagination 4",
			service:        msrv.SimulateResource(ctr, nil),
			statusCode:     http.StatusOK,
			withPagination: true,
			pagination: pg.Request{
				Page:     2,
				PageSize: 10,
			},
		},
	}

	for _, test := range tests {
		t.Run(test.name,
			func(t *testing.T) {
				query := ""
				router := testpkg.GetRouter()
				w := httptest.NewRecorder()

				initResourceServer(router, test.service)

				if test.withPagination {
					queryKeys := make(map[string]string, 2)
					if test.pagination.Page != 0 {
						queryKeys["page"] = strconv.Itoa(test.pagination.Page)
					}
					if test.pagination.PageSize != 0 {
						queryKeys["page_size"] = strconv.Itoa(test.pagination.PageSize)
					}
					query += "?"

					for key, value := range queryKeys {
						query += key + "=" + value + "&"
					}
				}

				req := httptest.NewRequest(method, fmt.Sprintf("%s%s", target, query), nil)
				router.ServeHTTP(w, req)

				assert.Equal(t, test.statusCode, w.Code)

				res := pg.Response{}
				_ = json.Unmarshal(w.Body.Bytes(), &res)

				if test.pagination.PageSize > 0 {
					assert.Equal(t, int64(test.pagination.GetLimit()), res.Meta.ObjectsCount)
				}

				assert.Equal(t, test.pagination.GetPage(), res.Meta.Page)

				//if tt.pagination.PageSize != 0 {
				//	assert.Equal(t, int64(0), res.Meta.ObjectsCount)
				//}
			})
	}
}

func TestResourceServer_update(t *testing.T) {
	const target = "/admin/resource"
	const method = http.MethodPatch

	ctr := gomock.NewController(t)
	defer ctr.Finish()

	tests := []struct {
		name       string
		service    resourceService
		id         string
		data       schemas.ResourceRequest
		statusCode int
		wantErr    bool
		err        error
	}{
		{
			name:    "existing",
			service: msrv.SimulateResource(ctr, nil),
			id:      uuid.NewString(),
			data: schemas.ResourceRequest{
				Name: "test permission",
				Description: "Повседневная практика показывает, " +
					"что дальнейшее развитие различных форм деятельности требуют определения и уточнения новых" +
					" предложений. Идейные соображения высшего порядка, " +
					"а также рамки и место обучения кадров влечет  за собой процесс внедрения и модернизации позиций," +
					"  занимаемых участниками в отношении поставленных задач. " +
					"Равным образом реализация намеченных  плановых заданий обеспечивает широкому кругу (" +
					"специалистов)  участие в формировании систем массового участия. " +
					"Товарищи! постоянный количественный  рост и сфера нашей активности представляет собой интересный эксперимент проверки новых предложений.",
				Roles: []uuid.UUID{uuid.New(), uuid.New()},
			},
			statusCode: http.StatusOK,
			wantErr:    false,
		},
		{
			name:    "not exists",
			service: msrv.SimulateResource(ctr, apperr.ErrResourceNotFound),
			id:      uuid.NewString(),
			data: schemas.ResourceRequest{
				Name: "test permission",
				Description: "Повседневная практика показывает, " +
					"что дальнейшее развитие различных форм деятельности требуют определения и уточнения новых" +
					" предложений. Идейные соображения высшего порядка, " +
					"а также рамки и место обучения кадров влечет  за собой процесс внедрения и модернизации позиций," +
					"  занимаемых участниками в отношении поставленных задач. " +
					"Равным образом реализация намеченных  плановых заданий обеспечивает широкому кругу (" +
					"специалистов)  участие в формировании систем массового участия. " +
					"Товарищи! постоянный количественный  рост и сфера нашей активности представляет собой интересный эксперимент проверки новых предложений.",
				Roles: []uuid.UUID{uuid.New(), uuid.New()},
			},
			statusCode: http.StatusNotFound,
			wantErr:    true,
			err:        apperr.ErrResourceNotFound,
		},
		{
			name:    "invalid id",
			service: msrv.SimulateResource(ctr, nil),
			id:      "qwerty",
			data: schemas.ResourceRequest{
				Name: "test permission",
				Description: "Повседневная практика показывает, " +
					"что дальнейшее развитие различных форм деятельности требуют определения и уточнения новых" +
					" предложений. Идейные соображения высшего порядка, " +
					"а также рамки и место обучения кадров влечет  за собой процесс внедрения и модернизации позиций," +
					"  занимаемых участниками в отношении поставленных задач. " +
					"Равным образом реализация намеченных  плановых заданий обеспечивает широкому кругу (" +
					"специалистов)  участие в формировании систем массового участия. " +
					"Товарищи! постоянный количественный  рост и сфера нашей активности представляет собой интересный эксперимент проверки новых предложений.",
				Roles: []uuid.UUID{uuid.New(), uuid.New()},
			},
			statusCode: http.StatusBadRequest,
			wantErr:    true,
			err:        apperr.ErrInvalidIDType,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			router := testpkg.GetRouter()
			w := httptest.NewRecorder()
			initResourceServer(router, test.service)

			jsonValue, _ := json.Marshal(test.data)
			req := httptest.NewRequest(method, fmt.Sprintf("%s/%s", target, test.id), bytes.NewBuffer(jsonValue))
			router.ServeHTTP(w, req)

			res := w.Result()

			assert.Equal(t, test.statusCode, res.StatusCode)

			if test.wantErr {
				testpkg.AssertHTTPError(t, test.statusCode, test.err, w)
			} else {
				var res models.Resource
				_ = json.Unmarshal(w.Body.Bytes(), &res)

				assert.NotEmpty(t, res)

				assert.Equal(t, test.id, res.ID.String())

				assert.Equal(t, test.data.Name, res.Name)
				assert.Equal(t, test.data.Description, res.Description)
			}
		})
	}
}

func TestResourceServer_delete(t *testing.T) {
	const target = "/admin/resource"
	const method = http.MethodDelete

	ctr := gomock.NewController(t)
	defer ctr.Finish()

	tests := []struct {
		name     string
		service  resourceService
		id       string
		wantCode int
		wantErr  bool
		err      error
	}{
		{
			name:     "exist resource",
			service:  msrv.SimulateResource(ctr, nil),
			id:       uuid.NewString(),
			wantCode: http.StatusNoContent,
			wantErr:  false,
		},
		{
			name:     "invalid id",
			service:  msrv.SimulateResource(ctr, nil),
			id:       gen.GenerateString(),
			wantCode: http.StatusBadRequest,
			wantErr:  true,
			err:      apperr.ErrInvalidIDType,
		},
		{
			name:     "not found",
			service:  msrv.SimulateResource(ctr, apperr.ErrResourceNotFound),
			id:       uuid.NewString(),
			wantCode: http.StatusNotFound,
			wantErr:  true,
			err:      apperr.ErrResourceNotFound,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			router := testpkg.GetRouter()
			w := httptest.NewRecorder()
			initResourceServer(router, test.service)

			req := httptest.NewRequest(method, fmt.Sprintf("%s/%s", target, test.id), nil)
			router.ServeHTTP(w, req)

			res := w.Result()

			assert.Equal(t, test.wantCode, res.StatusCode)

			if test.wantErr {
				testpkg.AssertHTTPError(t, test.wantCode, test.err, w)
			}
		})
	}
}
