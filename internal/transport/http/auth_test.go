package http

import (
	"bytes"
	"encoding/json"
	"github.com/gin-gonic/gin"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	apperr "sso/internal/consts/errors"
	msrv "sso/internal/mock/services"
	testpkg "sso/internal/pkg/test"
	"sso/internal/transport/http/schemas"
	"testing"
)

func initAuthServer(router *gin.Engine, service authService) {
	s := NewAuthServer(service)
	publicGroup := router.Group("/api")
	s.RegisterPublicRouts(publicGroup)
}

func TestNewAuthServer_login(t *testing.T) {
	const target = "/api/auth/"
	const method = http.MethodPost

	ctr := gomock.NewController(t)
	defer ctr.Finish()

	tests := []struct {
		name    string
		service authService
		data    schemas.LoginRequest
		code    int
		wantErr bool
		err     error
	}{
		{
			name:    "success",
			service: msrv.AuthSimulation(ctr, nil),
			data: schemas.LoginRequest{
				Username: "test",
				Password: "test",
			},
			code: http.StatusOK,
		},
		{
			name:    "user not found",
			service: msrv.AuthSimulation(ctr, apperr.ErrUserNotFound),
			data: schemas.LoginRequest{
				Username: "test",
				Password: "test",
			},
			code:    http.StatusUnauthorized,
			wantErr: true,
			err:     apperr.ErrInvalidUsernameOrPassword,
		},
		{
			name:    "wrong password",
			service: msrv.AuthSimulation(ctr, apperr.ErrInvalidPassword),
			data: schemas.LoginRequest{
				Username: "test",
				Password: "test",
			},
			code:    http.StatusUnauthorized,
			wantErr: true,
			err:     apperr.ErrInvalidUsernameOrPassword,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			router := testpkg.GetRouter()
			w := httptest.NewRecorder()
			initAuthServer(router, test.service)

			jsonData, _ := json.Marshal(test.data)
			req, _ := http.NewRequest(method, target, bytes.NewBuffer(jsonData))
			router.ServeHTTP(w, req)

			assert.Equal(t, test.code, w.Code)
			if test.wantErr {
				testpkg.AssertHTTPError(t, test.code, test.err, w)
			}
		})
	}
}

func TestNewAuthServer_refresh(t *testing.T) {
	const target = "/api/auth/refresh"
	const method = http.MethodPost

	ctr := gomock.NewController(t)
	defer ctr.Finish()
}
