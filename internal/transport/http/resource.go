//go:generate mockgen -source=resource.go -destination=../../mock/services/resource.go -package=services
//go:generate swag fmt && swag init -o ./api -g internal/transport/http/resource.go
package http

import (
	"errors"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"net/http"
	apperr "sso/internal/consts/errors"
	"sso/internal/models"
	"sso/internal/pkg/pg"
	"sso/internal/pkg/utils"
	"sso/internal/transport/http/schemas"
)

type resourceService interface {
	Create(name string, description string, rolesIDs []uuid.UUID) (resource models.Resource, err error)
	GetByID(id uuid.UUID) (resource models.Resource, err error)
	GetList(limit int, offset int, sort string) (resources []models.Resource, err error)
	GetByIDs(idList []uuid.UUID) (resources []models.Resource, err error)
	Count() (int64, error)
	UpdateByID(id uuid.UUID, name string, description string, rolesIDs []uuid.UUID) (resource models.Resource, err error)
	DeleteByID(id uuid.UUID) (err error)
}

type ResourceServer struct {
	service resourceService
}

func NewResourceServer(service resourceService) *ResourceServer {
	return &ResourceServer{
		service: service,
	}
}

func (srv *ResourceServer) RegisterAdminRouts(adminGroup *gin.RouterGroup) {
	resourceAdminRouts := adminGroup.Group("/resource")
	{
		resourceAdminRouts.POST("/", srv.create)
		resourceAdminRouts.GET("/:resource_id", srv.get)
		resourceAdminRouts.GET("/list", srv.list)
		resourceAdminRouts.PATCH("/:resource_id", srv.update)
		resourceAdminRouts.DELETE("/:resource_id", srv.delete)
	}
}

// create godoc
//
//	@Summary	Создать ресурс
//	@Tags		resource
//	@Accept		json
//	@Produce	json
//
//	@Param		resource	body		resourceRequest	true	"Resource"
//
//	@Success	200			{object}	models.Permission
//	@Failure	400			{object}	utils.HTTPError
//	@Failure	404			{object}	utils.HTTPError
//	@Failure	500			{object}	utils.HTTPError
//
//	@Router		/admin/resource/ [post]
func (srv *ResourceServer) create(c *gin.Context) {
	var req schemas.ResourceRequest
	if err := c.ShouldBindJSON(&req); err != nil {
		utils.NewError(c, http.StatusBadRequest, err)
		return
	}

	resource, err := srv.service.Create(req.Name, req.Description, req.Roles)
	if err != nil {
		utils.NewError(c, http.StatusBadRequest, err)
		return
	}

	c.JSON(http.StatusCreated, resource)
	return
}

// get godoc
//
//	@Summary	Получить ресурс
//	@Tags		resource
//	@Accept		json
//	@Produce	json
//
//	@Param		resource_id	path		string	true	"Resource ID"
//
//	@Success	200			{object}	models.Permission
//	@Failure	400			{object}	utils.HTTPError
//	@Failure	404			{object}	utils.HTTPError
//	@Failure	500			{object}	utils.HTTPError
//
//	@Router		/admin/resource/{resource_id} [get]
func (srv *ResourceServer) get(c *gin.Context) {
	resourceID, err := uuid.Parse(c.Param("resource_id"))
	if err != nil {
		utils.NewError(c, http.StatusBadRequest, apperr.ErrInvalidIDType)
		return
	}

	resource, err := srv.service.GetByID(resourceID)
	if err != nil {
		utils.NewError(c, http.StatusNotFound, err)
		return
	}

	c.JSON(http.StatusOK, resource)
	return
}

// list godoc
//
//	@Summary	Получить список ресурсов
//	@Tags		resource
//	@Accept		json
//	@Produce	json
//
//	@Param		page		query		string	true	"Page number"
//	@Param		page_size	query		string	true	"Page Size"
//
//	@Success	200			{object}	models.Permission
//	@Failure	400			{object}	utils.HTTPError
//	@Failure	404			{object}	utils.HTTPError
//	@Failure	500			{object}	utils.HTTPError
//
//	@Router		/admin/resource/list [get]
func (srv *ResourceServer) list(c *gin.Context) {
	var request pg.Request

	if err := c.ShouldBindQuery(&request); err != nil {
		utils.NewError(c, http.StatusBadRequest, err)
		return
	}

	permissions, err := srv.service.GetList(request.GetLimit(), request.GetOffset(), request.GetSort())
	if err != nil {
		utils.NewError(c, http.StatusInternalServerError, err)
		return
	}

	total, err := srv.service.Count()
	if err != nil {
		utils.NewError(c, http.StatusInternalServerError, err)
		return
	}

	response := pg.NewResponse(permissions, int64(len(permissions)), total, &request)

	c.JSON(http.StatusOK, response)
	return
}

// update godoc
//
//	@Summary	Обновить ресурс
//	@Tags		resource
//	@Accept		json
//	@Produce	json
//
//	@Param		resource_id	path		string			true	"Resource ID"
//	@Param		resource	body		resourceRequest	true	"Resource"
//
//	@Success	200			{object}	models.Resource
//	@Failure	400			{object}	utils.HTTPError
//	@Failure	404			{object}	utils.HTTPError
//	@Failure	500			{object}	utils.HTTPError
//
//	@Router		/admin/resource/{resource_id} [patch]
func (srv *ResourceServer) update(c *gin.Context) {
	var req schemas.ResourceRequest

	id, err := uuid.Parse(c.Param("resource_id"))
	if err != nil {
		utils.NewError(c, http.StatusBadRequest, apperr.ErrInvalidIDType)
		return
	}

	if err := c.ShouldBindJSON(&req); err != nil {
		utils.NewError(c, http.StatusBadRequest, err)
		return
	}

	resource, err := srv.service.UpdateByID(id, req.Name, req.Description, req.Roles)
	if err != nil {
		if errors.Is(err, apperr.ErrResourceNotFound) {
			utils.NewError(c, http.StatusNotFound, err)
			return
		}
		utils.NewError(c, http.StatusBadRequest, err)
		return
	}

	c.JSON(http.StatusOK, resource)
	return
}

// delete godoc
//
//	@Summary	Удалить ресурс
//	@Tags		resource
//	@Accept		json
//	@Produce	json
//
//	@Param		resource_id	path		string	true	"Resource ID"
//
//	@Success	200			{object}	models.Permission
//	@Failure	400			{object}	utils.HTTPError
//	@Failure	404			{object}	utils.HTTPError
//	@Failure	500			{object}	utils.HTTPError
//
//	@Router		/admin/resource/{resource_id} [delete]
func (srv *ResourceServer) delete(c *gin.Context) {
	resourceID, err := uuid.Parse(c.Param("resource_id"))
	if err != nil {
		utils.NewError(c, http.StatusBadRequest, apperr.ErrInvalidIDType)
		return
	}

	if err := srv.service.DeleteByID(resourceID); err != nil {
		utils.NewError(c, http.StatusNotFound, err)
		return
	}

	c.JSON(http.StatusNoContent, nil)
	return
}
