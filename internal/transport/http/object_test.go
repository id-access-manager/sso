package http

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/golang/mock/gomock"
	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"gorm.io/gorm"
	"net/http"
	"net/http/httptest"
	apperr "sso/internal/consts/errors"
	mocksrv "sso/internal/mock/services"
	"sso/internal/pkg/pg"
	"sso/internal/pkg/test"
	"sso/internal/pkg/utils"
	"sso/internal/transport/http/schemas"
	"strconv"
	"testing"
)

func initObjectServer(router *gin.Engine, service objectService) {
	s := NewObjectServer(service)
	adminGroup := router.Group("admin/")
	s.RegisterAdminRouts(adminGroup)
}

func TestObjectServer_create(t *testing.T) {
	const target = "/admin/object/"
	const method = http.MethodPost

	ctr := gomock.NewController(t)
	defer ctr.Finish()

	tests := []struct {
		name       string
		service    objectService
		object     schemas.ObjectRequest
		statusCode int
		wantErr    bool
		err        error
	}{
		{
			name:    "create object",
			service: mocksrv.SimulateObject(ctr, nil),
			object: schemas.ObjectRequest{
				Name: "test object",
			},
			statusCode: http.StatusCreated,
			wantErr:    false,
		},
		{
			name:    "db error",
			service: mocksrv.SimulateObject(ctr, gorm.ErrInvalidDB),
			object: schemas.ObjectRequest{
				Name: "test object",
			},
			statusCode: http.StatusBadRequest,
			wantErr:    true,
			err:        gorm.ErrInvalidDB,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			router := test.GetRouter()
			w := httptest.NewRecorder()
			initObjectServer(router, tt.service)

			jsonValue, _ := json.Marshal(tt.object)
			req := httptest.NewRequest(method, target, bytes.NewBuffer(jsonValue))
			router.ServeHTTP(w, req)

			if tt.wantErr {
				err := utils.HTTPError{Message: tt.err.Error()}
				jsonErr, _ := json.Marshal(err)

				assert.Equal(t, tt.statusCode, w.Code)
				assert.Equal(t, jsonErr, w.Body.Bytes())
			} else {
				assert.Equal(t, tt.statusCode, w.Code)
			}
		})
	}
}

func TestObjectServer_get(t *testing.T) {
	const target = "/admin/object"
	const method = http.MethodGet

	ctr := gomock.NewController(t)
	defer ctr.Finish()

	tests := []struct {
		name       string
		service    objectService
		objectID   string
		statusCode int
		wantErr    bool
		err        error
	}{
		{
			name:       "existing object",
			service:    mocksrv.SimulateObject(ctr, nil),
			objectID:   uuid.New().String(),
			statusCode: http.StatusOK,
			wantErr:    false,
		},
		{
			name:       "not found",
			service:    mocksrv.SimulateObject(ctr, apperr.ErrObjectNotFound),
			objectID:   uuid.New().String(),
			statusCode: http.StatusNotFound,
			wantErr:    true,
			err:        apperr.ErrObjectNotFound,
		},
		{
			name:       "invalid id",
			service:    mocksrv.SimulateObject(ctr, nil),
			objectID:   "qwerty",
			statusCode: http.StatusBadRequest,
			wantErr:    true,
			err:        apperr.ErrInvalidIDType,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			router := test.GetRouter()
			w := httptest.NewRecorder()
			initObjectServer(router, tt.service)

			req := httptest.NewRequest(method, fmt.Sprintf("%s/%s", target, tt.objectID), nil)
			router.ServeHTTP(w, req)
			if tt.wantErr {
				err := utils.HTTPError{Message: tt.err.Error()}
				jsonErr, _ := json.Marshal(err)

				assert.Equal(t, tt.statusCode, w.Code)
				assert.Equal(t, jsonErr, w.Body.Bytes())
			} else {
				assert.Equal(t, tt.statusCode, w.Code)
			}
		})
	}
}

func TestObjectServer_list(t *testing.T) {
	const target = "/admin/object/list"
	const method = http.MethodGet

	ctr := gomock.NewController(t)
	defer ctr.Finish()

	tests := []struct {
		name           string
		service        objectService
		statusCode     int
		withPagination bool
		pagination     pg.Request
		wantRes        pg.Response
	}{
		{
			name:           "without pagination",
			service:        mocksrv.SimulateObject(ctr, nil),
			statusCode:     http.StatusOK,
			withPagination: false,
		},
		{
			name:           "with pagination 1",
			service:        mocksrv.SimulateObject(ctr, nil),
			statusCode:     http.StatusOK,
			withPagination: true,
			pagination: pg.Request{
				Page:     1,
				PageSize: 1,
			},
		},
		{
			name:           "with pagination 2",
			service:        mocksrv.SimulateObject(ctr, nil),
			statusCode:     http.StatusOK,
			withPagination: true,
			pagination: pg.Request{
				Page:     0,
				PageSize: 0,
			},
		},
		{
			name:           "with pagination 3",
			service:        mocksrv.SimulateObject(ctr, nil),
			statusCode:     http.StatusOK,
			withPagination: true,
			pagination: pg.Request{
				Page:     12,
				PageSize: 0,
			},
		},
		{
			name:           "with pagination 4",
			service:        mocksrv.SimulateObject(ctr, nil),
			statusCode:     http.StatusOK,
			withPagination: true,
			pagination: pg.Request{
				Page:     2,
				PageSize: 10,
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			query := ""
			router := test.GetRouter()
			w := httptest.NewRecorder()
			initObjectServer(router, tt.service)

			if tt.withPagination {
				queryKeys := make(map[string]string, 2)
				if tt.pagination.Page != 0 {
					queryKeys["page"] = strconv.Itoa(tt.pagination.Page)
				}
				if tt.pagination.PageSize != 0 {
					queryKeys["page_size"] = strconv.Itoa(tt.pagination.PageSize)
				}
				query += "?"

				for key, value := range queryKeys {
					query += key + "=" + value + "&"
				}
			}

			req := httptest.NewRequest(method, fmt.Sprintf("%s%s", target, query), nil)
			router.ServeHTTP(w, req)

			assert.Equal(t, tt.statusCode, w.Code)

			res := pg.Response{}
			_ = json.Unmarshal(w.Body.Bytes(), &res)

			//if tt.pagination.PageSize != 0 {
			//	assert.Equal(t, int64(0), res.Meta.ObjectsCount)
			//}
		})
	}
}

func TestObjectServer_update(t *testing.T) {
	const target = "/admin/object"
	const method = http.MethodGet

	ctr := gomock.NewController(t)
	defer ctr.Finish()

	tests := []struct {
		name       string
		service    objectService
		objectID   string
		object     map[string]string
		statusCode int
		wantErr    bool
		err        error
	}{
		{
			name:     "existing object",
			service:  mocksrv.SimulateObject(ctr, nil),
			objectID: uuid.New().String(),
			object: map[string]string{
				"name": "test Action",
			},
			statusCode: http.StatusOK,
			wantErr:    false,
		},
		{
			name:       "not found",
			service:    mocksrv.SimulateObject(ctr, apperr.ErrObjectNotFound),
			objectID:   uuid.New().String(),
			statusCode: http.StatusNotFound,
			object: map[string]string{
				"name": "test Action",
			},
			wantErr: true,
			err:     apperr.ErrObjectNotFound,
		},
		{
			name:       "invalid id",
			service:    mocksrv.SimulateObject(ctr, nil),
			objectID:   "qwerty",
			statusCode: http.StatusBadRequest,
			object: map[string]string{
				"name": "test Action",
			},
			wantErr: true,
			err:     apperr.ErrInvalidIDType,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			router := test.GetRouter()
			w := httptest.NewRecorder()
			initObjectServer(router, tt.service)

			jsonValue, _ := json.Marshal(tt.object)
			req := httptest.NewRequest(method, fmt.Sprintf("%s/%s", target, tt.objectID), bytes.NewBuffer(jsonValue))
			router.ServeHTTP(w, req)

			if tt.wantErr {
				err := utils.HTTPError{Message: tt.err.Error()}
				jsonErr, _ := json.Marshal(err)

				assert.Equal(t, tt.statusCode, w.Code)
				assert.Equal(t, jsonErr, w.Body.Bytes())
			} else {
				assert.Equal(t, tt.statusCode, w.Code)
			}
		})
	}
}

func TestObjectServer_delete(t *testing.T) {
	const target = "/admin/object"
	const method = http.MethodDelete

	ctr := gomock.NewController(t)
	defer ctr.Finish()

	tests := []struct {
		name       string
		service    objectService
		objectID   string
		statusCode int
		wantErr    bool
		err        error
	}{
		{
			name:       "existing object",
			service:    mocksrv.SimulateObject(ctr, nil),
			objectID:   uuid.New().String(),
			statusCode: http.StatusNoContent,
			wantErr:    false,
		},
		{
			name:       "not found",
			service:    mocksrv.SimulateObject(ctr, apperr.ErrObjectNotFound),
			objectID:   uuid.New().String(),
			statusCode: http.StatusNotFound,
			wantErr:    true,
			err:        apperr.ErrObjectNotFound,
		},
		{
			name:       "invalid id",
			service:    mocksrv.SimulateObject(ctr, nil),
			objectID:   "qwerty",
			statusCode: http.StatusBadRequest,
			wantErr:    true,
			err:        apperr.ErrInvalidIDType,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			router := test.GetRouter()
			w := httptest.NewRecorder()
			initObjectServer(router, tt.service)

			req := httptest.NewRequest(method, fmt.Sprintf("%s/%s", target, tt.objectID), nil)
			router.ServeHTTP(w, req)
			if tt.wantErr {
				err := utils.HTTPError{Message: tt.err.Error()}
				jsonErr, _ := json.Marshal(err)

				assert.Equal(t, tt.statusCode, w.Code)
				assert.Equal(t, jsonErr, w.Body.Bytes())
			} else {
				assert.Equal(t, tt.statusCode, w.Code)
			}
		})
	}
}
