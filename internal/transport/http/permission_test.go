package http

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/golang/mock/gomock"
	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	apperr "sso/internal/consts/errors"
	mocksrv "sso/internal/mock/services"
	"sso/internal/models"
	"sso/internal/pkg/pg"
	testpkg "sso/internal/pkg/test"
	"sso/internal/transport/http/schemas"
	"strconv"
	"testing"
)

func initPermissionServer(router *gin.Engine, service permissionService) {
	s := NewPermissionServer(service)
	adminGroup := router.Group("/admin")
	s.RegisterAdminRouts(adminGroup)
}

func TestPermissionServer_create(t *testing.T) {
	const target = "/admin/permission/"
	const method = http.MethodPost

	ctr := gomock.NewController(t)
	defer ctr.Finish()

	tests := []struct {
		name       string
		service    permissionService
		data       schemas.PermissionRequest
		statusCode int
		wantErr    bool
		err        error
	}{
		{
			name:    "success test",
			service: mocksrv.SimulatePermission(ctr, nil),
			data: schemas.PermissionRequest{
				Name:     "test permission",
				ActionID: uuid.New(),
				ObjectID: uuid.New(),
			},
			statusCode: http.StatusCreated,
			wantErr:    false,
		},
		{
			name:    "not exists action",
			service: mocksrv.SimulatePermission(ctr, apperr.ErrActionNotFound),
			data: schemas.PermissionRequest{
				Name:     "test permission",
				ActionID: uuid.New(),
				ObjectID: uuid.New(),
			},
			statusCode: http.StatusBadRequest,
			wantErr:    true,
			err:        apperr.ErrActionNotFound,
		},
		{
			name:    "not exists object",
			service: mocksrv.SimulatePermission(ctr, apperr.ErrObjectNotFound),
			data: schemas.PermissionRequest{
				Name:     "test permission",
				ActionID: uuid.New(),
				ObjectID: uuid.New(),
			},
			statusCode: http.StatusBadRequest,
			wantErr:    true,
			err:        apperr.ErrObjectNotFound,
		},
		//{
		//	name:    "invalid action id",
		//	service: mocksrv.SimulatePermission(ctr, nil),
		//	data: permissionRequest{
		//		Name:     "test permission",
		//		ActionID: "qwerty",
		//		ObjectID: uuid.New(),
		//	},
		//	statusCode: http.StatusBadRequest,
		//	wantErr:    true,
		//	err:        apperr.ErrInvalidIDType,
		//},
		//{
		//	name:    "invalid object id",
		//	service: mocksrv.SimulatePermission(ctr, nil),
		//	data: permissionRequest{
		//		Name:     "test permission",
		//		ActionID: uuid.New(),
		//		ObjectID: "qwerty",
		//	},
		//	statusCode: http.StatusBadRequest,
		//	wantErr:    true,
		//	err:        apperr.ErrInvalidIDType,
		//},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			r := testpkg.GetRouter()
			w := httptest.NewRecorder()
			initPermissionServer(r, test.service)

			jsonValue, _ := json.Marshal(test.data)
			req := httptest.NewRequest(method, target, bytes.NewBuffer(jsonValue))
			r.ServeHTTP(w, req)

			assert.Equal(t, test.statusCode, w.Code)

			if test.wantErr {
				testpkg.AssertHTTPError(t, test.statusCode, test.err, w)
			} else {
				var permission models.Permission
				_ = json.Unmarshal(w.Body.Bytes(), &permission)

				assert.NotEmpty(t, permission)

				assert.Equal(t, test.data.Name, permission.Name)
			}
		})
	}
}

func TestPermissionServer_get(t *testing.T) {
	const target = "/admin/permission"
	const method = http.MethodGet

	ctr := gomock.NewController(t)
	defer ctr.Finish()

	tests := []struct {
		name         string
		service      permissionService
		permissionID string
		statusCode   int
		wantErr      bool
		err          error
	}{
		{
			name:         "exist permission",
			service:      mocksrv.SimulatePermission(ctr, nil),
			permissionID: uuid.New().String(),
			statusCode:   http.StatusOK,
			wantErr:      false,
		},
		{
			name:         "not exist permission",
			service:      mocksrv.SimulatePermission(ctr, apperr.ErrPermissionNotFound),
			permissionID: uuid.New().String(),
			statusCode:   http.StatusNotFound,
			wantErr:      true,
			err:          apperr.ErrPermissionNotFound,
		},
		{
			name:         "wrong permission id",
			service:      mocksrv.SimulatePermission(ctr, nil),
			permissionID: "qwerty",
			statusCode:   http.StatusBadRequest,
			wantErr:      true,
			err:          apperr.ErrInvalidIDType,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			r := testpkg.GetRouter()
			w := httptest.NewRecorder()
			initPermissionServer(r, test.service)

			req, _ := http.NewRequest(method, fmt.Sprintf("%s/%s", target, test.permissionID), nil)
			r.ServeHTTP(w, req)

			if test.wantErr {
				testpkg.AssertHTTPError(t, test.statusCode, test.err, w)
			} else {
				var data models.Permission

				_ = json.Unmarshal(w.Body.Bytes(), &data)
				res := w.Result()

				assert.Equal(t, test.statusCode, res.StatusCode)

				assert.Equal(t, test.permissionID, data.ID.String())
				assert.NotNil(t, data.Name)
			}
		})
	}
}

func TestPermissionServer_list(t *testing.T) {
	const target = "/admin/permission/list"
	const method = http.MethodGet

	ctr := gomock.NewController(t)
	defer ctr.Finish()

	tests := []struct {
		name           string
		service        permissionService
		statusCode     int
		withPagination bool
		pagination     pg.Request
		wantRes        pg.Response
	}{
		{
			name:           "without pagination",
			service:        mocksrv.SimulatePermission(ctr, nil),
			statusCode:     http.StatusOK,
			withPagination: false,
		},
		{
			name:           "with pagination 1",
			service:        mocksrv.SimulatePermission(ctr, nil),
			statusCode:     http.StatusOK,
			withPagination: true,
			pagination: pg.Request{
				Page:     1,
				PageSize: 1,
			},
		},
		{
			name:           "with pagination 2",
			service:        mocksrv.SimulatePermission(ctr, nil),
			statusCode:     http.StatusOK,
			withPagination: true,
			pagination: pg.Request{
				Page:     0,
				PageSize: 0,
			},
		},
		{
			name:           "with pagination 3",
			service:        mocksrv.SimulatePermission(ctr, nil),
			statusCode:     http.StatusOK,
			withPagination: true,
			pagination: pg.Request{
				Page:     12,
				PageSize: 0,
			},
		},
		{
			name:           "with pagination 4",
			service:        mocksrv.SimulatePermission(ctr, nil),
			statusCode:     http.StatusOK,
			withPagination: true,
			pagination: pg.Request{
				Page:     2,
				PageSize: 10,
			},
		},
	}

	for _, test := range tests {
		t.Run(test.name,
			func(t *testing.T) {
				query := ""
				router := testpkg.GetRouter()
				w := httptest.NewRecorder()

				initPermissionServer(router, test.service)

				if test.withPagination {
					queryKeys := make(map[string]string, 2)
					if test.pagination.Page != 0 {
						queryKeys["page"] = strconv.Itoa(test.pagination.Page)
					}
					if test.pagination.PageSize != 0 {
						queryKeys["page_size"] = strconv.Itoa(test.pagination.PageSize)
					}
					query += "?"

					for key, value := range queryKeys {
						query += key + "=" + value + "&"
					}
				}

				req := httptest.NewRequest(method, fmt.Sprintf("%s%s", target, query), nil)
				router.ServeHTTP(w, req)

				assert.Equal(t, test.statusCode, w.Code)

				res := pg.Response{}
				_ = json.Unmarshal(w.Body.Bytes(), &res)

				if test.pagination.PageSize > 0 {
					assert.Equal(t, int64(test.pagination.GetLimit()), res.Meta.ObjectsCount)
				}

				assert.Equal(t, test.pagination.GetPage(), res.Meta.Page)

				//if tt.pagination.PageSize != 0 {
				//	assert.Equal(t, int64(0), res.Meta.ObjectsCount)
				//}
			})
	}
}

func TestPermissionServer_update(t *testing.T) {
	const target = "/admin/permission"
	const method = http.MethodPatch

	ctr := gomock.NewController(t)
	defer ctr.Finish()

	tests := []struct {
		name           string
		service        permissionService
		permissionID   string
		data           schemas.PermissionRequest
		wantStatusCode int
		wantErr        bool
		err            error
	}{
		{
			name:         "existing",
			service:      mocksrv.SimulatePermission(ctr, nil),
			permissionID: uuid.NewString(),
			data: schemas.PermissionRequest{
				Name:     "test permission",
				ActionID: uuid.New(),
				ObjectID: uuid.New(),
			},
			wantStatusCode: http.StatusOK,
			wantErr:        false,
		},
		{
			name:         "not exists",
			service:      mocksrv.SimulatePermission(ctr, apperr.ErrPermissionNotFound),
			permissionID: uuid.NewString(),
			data: schemas.PermissionRequest{
				Name:     "test permission",
				ActionID: uuid.New(),
				ObjectID: uuid.New(),
			},
			wantStatusCode: http.StatusNotFound,
			wantErr:        true,
			err:            apperr.ErrPermissionNotFound,
		},
		{
			name:         "invalid id",
			service:      mocksrv.SimulatePermission(ctr, nil),
			permissionID: "qwerty",
			data: schemas.PermissionRequest{
				Name:     "test permission",
				ActionID: uuid.New(),
				ObjectID: uuid.New(),
			},
			wantStatusCode: http.StatusBadRequest,
			wantErr:        true,
			err:            apperr.ErrInvalidIDType,
		},
		{
			name:         "action not found",
			service:      mocksrv.SimulatePermission(ctr, apperr.ErrActionNotFound),
			permissionID: uuid.NewString(),
			data: schemas.PermissionRequest{
				Name:     "test permission",
				ActionID: uuid.New(),
				ObjectID: uuid.New(),
			},
			wantStatusCode: http.StatusBadRequest,
			wantErr:        true,
			err:            apperr.ErrActionNotFound,
		},
		{
			name:         "object not found",
			service:      mocksrv.SimulatePermission(ctr, apperr.ErrObjectNotFound),
			permissionID: uuid.NewString(),
			data: schemas.PermissionRequest{
				Name:     "test permission",
				ActionID: uuid.New(),
				ObjectID: uuid.New(),
			},
			wantStatusCode: http.StatusBadRequest,
			wantErr:        true,
			err:            apperr.ErrObjectNotFound,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			r := testpkg.GetRouter()
			w := httptest.NewRecorder()
			initPermissionServer(r, test.service)

			jsonValue, _ := json.Marshal(test.data)
			req, _ := http.NewRequest(method, fmt.Sprintf("%s/%s", target, test.permissionID), bytes.NewBuffer(jsonValue))
			r.ServeHTTP(w, req)

			assert.Equal(t, test.wantStatusCode, w.Code)
			if test.wantErr {
				testpkg.AssertHTTPError(t, test.wantStatusCode, test.err, w)
			} else {
				var res models.Permission
				_ = json.Unmarshal(w.Body.Bytes(), &res)

				assert.NotEmpty(t, res)

				assert.Equal(t, test.permissionID, res.ID.String())

				assert.Equal(t, test.data.Name, res.Name)
			}
		})
	}
}

func TestPermissionServer_delete(t *testing.T) {
	const target = "/admin/permission"
	const method = http.MethodDelete

	ctr := gomock.NewController(t)
	defer ctr.Finish()

	tests := []struct {
		name         string
		service      permissionService
		permissionID string
		statusCode   int
		wantErr      bool
		err          error
	}{
		{
			name:         "exist permission",
			service:      mocksrv.SimulatePermission(ctr, nil),
			permissionID: uuid.New().String(),
			statusCode:   http.StatusNoContent,
			wantErr:      false,
		},
		{
			name:         "not exist permission",
			service:      mocksrv.SimulatePermission(ctr, apperr.ErrPermissionNotFound),
			permissionID: uuid.New().String(),
			statusCode:   http.StatusNotFound,
			wantErr:      true,
			err:          apperr.ErrPermissionNotFound,
		},
		{
			name:         "wrong permission id",
			service:      mocksrv.SimulatePermission(ctr, nil),
			permissionID: "qwerty",
			statusCode:   http.StatusBadRequest,
			wantErr:      true,
			err:          apperr.ErrInvalidIDType,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			r := testpkg.GetRouter()
			w := httptest.NewRecorder()
			initPermissionServer(r, test.service)

			req, _ := http.NewRequest(method, fmt.Sprintf("%s/%s", target, test.permissionID), nil)
			r.ServeHTTP(w, req)

			if test.wantErr {
				testpkg.AssertHTTPError(t, test.statusCode, test.err, w)
			} else {
				res := w.Result()
				assert.Equal(t, test.statusCode, res.StatusCode)
			}
		})
	}
}
