package schemas

import "github.com/google/uuid"

type RoleRequest struct {
	Name        string      `json:"name"`
	Permissions []uuid.UUID `json:"permissions"`
}
