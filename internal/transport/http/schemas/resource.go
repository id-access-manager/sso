package schemas

import "github.com/google/uuid"

type ResourceRequest struct {
	Name        string      `json:"name"`
	Description string      `json:"description"`
	Roles       []uuid.UUID `json:"roles"`
}
