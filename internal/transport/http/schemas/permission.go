package schemas

import "github.com/google/uuid"

type PermissionRequest struct {
	Name     string    `json:"name"`
	ActionID uuid.UUID `json:"action_id"`
	ObjectID uuid.UUID `json:"object_id"`
}
