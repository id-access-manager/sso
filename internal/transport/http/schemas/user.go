package schemas

import "sso/internal/models"

type RegisterUserRequest struct {
	Email    models.Email `json:"email"`
	Username string       `json:"username"`
	Password string       `json:"password"`
}
