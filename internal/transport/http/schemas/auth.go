package schemas

type RefreshRequest struct {
	RefreshToken string `json:"refresh"`
}

type LoginRequest struct {
	Username string `json:"username"`
	Password string `json:"password"`
}
