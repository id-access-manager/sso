package schemas

import "sso/internal/models"

type ActionRequest struct {
	Name   string            `json:"name" binding:"required"`
	Action models.ActionType `json:"action" binding:"required"`
}
