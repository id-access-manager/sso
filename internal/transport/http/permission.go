//go:generate mockgen -source=permission.go -destination=../../mock/services/permission.go -package=services
//go:generate swag fmt && swag init -o ./api -g internal/transport/http/*.go
package http

import (
	"errors"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"net/http"
	apperr "sso/internal/consts/errors"
	"sso/internal/models"
	"sso/internal/pkg/pg"
	"sso/internal/pkg/utils"
	"sso/internal/transport/http/schemas"
)

type permissionService interface {
	Create(name string, actionID uuid.UUID, objectID uuid.UUID) (permission models.Permission, err error)
	GetByID(id uuid.UUID) (permission models.Permission, err error)
	GetList(limit int, offset int, sort string) (permissions []models.Permission, err error)
	GetCount() (int64, error)
	UpdateByID(id uuid.UUID, name string, actionID uuid.UUID, objectID uuid.UUID) (permission models.Permission, err error)
	DeleteByID(id uuid.UUID) (err error)
}

type PermissionServer struct {
	service permissionService
}

func NewPermissionServer(service permissionService) *PermissionServer {
	return &PermissionServer{
		service: service,
	}
}

func (ps *PermissionServer) RegisterAdminRouts(adminGroup *gin.RouterGroup) {
	permissionAdminGroup := adminGroup.Group("/permission")
	{
		permissionAdminGroup.POST("/", ps.create)
		permissionAdminGroup.GET("/:permission_id", ps.get)
		permissionAdminGroup.GET("/list", ps.list)
		permissionAdminGroup.PATCH("/:permission_id", ps.update)
		permissionAdminGroup.DELETE("/:permission_id", ps.delete)
	}
}

// create godoc
//
//	@Summary	Получить разрешение
//	@Tags		permission
//	@Accept		json
//	@Produce	json
//
//	@Param		permission	body		permissionRequest	true	"Permission"
//
//	@Success	200			{object}	models.Permission
//	@Failure	400			{object}	utils.HTTPError
//	@Failure	404			{object}	utils.HTTPError
//	@Failure	500			{object}	utils.HTTPError
//
//	@Router		/admin/permission/ [post]
func (ps *PermissionServer) create(c *gin.Context) {
	var req schemas.PermissionRequest

	if err := c.ShouldBindJSON(&req); err != nil {
		utils.NewError(c, http.StatusBadRequest, err)
		return
	}

	permission, err := ps.service.Create(req.Name, req.ActionID, req.ObjectID)
	if err != nil {
		utils.NewError(c, http.StatusBadRequest, err)
		return
	}

	c.JSON(http.StatusCreated, permission)
	return
}

// get godoc
//
//	@Summary	Получить разрешение
//	@Tags		permission
//	@Accept		json
//	@Produce	json
//
//	@Param		permission_id	path		string	true	"Object ID"
//
//	@Success	200				{object}	models.Permission
//	@Failure	400				{object}	utils.HTTPError
//	@Failure	404				{object}	utils.HTTPError
//	@Failure	500				{object}	utils.HTTPError
//
//	@Router		/admin/permission{permission_id} [get]
func (ps *PermissionServer) get(c *gin.Context) {
	permissionID, err := uuid.Parse(c.Param("permission_id"))
	if err != nil {
		utils.NewError(c, http.StatusBadRequest, apperr.ErrInvalidIDType)
		return
	}

	permission, err := ps.service.GetByID(permissionID)
	if err != nil {
		utils.NewError(c, http.StatusNotFound, err)
		return
	}

	c.JSON(http.StatusOK, permission)
	return
}

// list godoc
//
//	@Summary	Получить разрешение
//	@Tags		permission
//	@Accept		json
//	@Produce	json
//
//	@Param		page		query		string	true	"Page number"
//	@Param		page_size	query		string	true	"Page Size"
//
//	@Success	200			{object}	models.Permission
//	@Failure	400			{object}	utils.HTTPError
//	@Failure	404			{object}	utils.HTTPError
//	@Failure	500			{object}	utils.HTTPError
//
//	@Router		/admin/permission/list [get]
func (ps *PermissionServer) list(c *gin.Context) {
	var request pg.Request

	if err := c.ShouldBindQuery(&request); err != nil {
		utils.NewError(c, http.StatusBadRequest, err)
		return
	}

	permissions, err := ps.service.GetList(request.GetLimit(), request.GetOffset(), request.GetSort())
	if err != nil {
		utils.NewError(c, http.StatusInternalServerError, err)
		return
	}

	total, err := ps.service.GetCount()
	if err != nil {
		utils.NewError(c, http.StatusInternalServerError, err)
		return
	}

	response := pg.NewResponse(permissions, int64(len(permissions)), total, &request)

	c.JSON(http.StatusOK, response)
	return
}

// update godoc
//
//	@Summary	Получить разрешение
//	@Tags		permission
//	@Accept		json
//	@Produce	json
//
//	@Param		permission_id	path		string				true	"Permission ID"
//	@Param		permission		body		permissionRequest	true	"Permission"
//
//	@Success	200				{object}	models.Permission
//	@Failure	400				{object}	utils.HTTPError
//	@Failure	404				{object}	utils.HTTPError
//	@Failure	500				{object}	utils.HTTPError
//
//	@Router		/admin/permission/{permission_id} [patch]
func (ps *PermissionServer) update(c *gin.Context) {
	var req schemas.PermissionRequest

	permissionID, err := uuid.Parse(c.Param("permission_id"))
	if err != nil {
		utils.NewError(c, http.StatusBadRequest, apperr.ErrInvalidIDType)
		return
	}

	if err := c.ShouldBindJSON(&req); err != nil {
		utils.NewError(c, http.StatusBadRequest, err)
		return
	}

	permission, err := ps.service.UpdateByID(permissionID, req.Name, req.ActionID, req.ObjectID)
	if err != nil {
		if errors.Is(err, apperr.ErrPermissionNotFound) {
			utils.NewError(c, http.StatusNotFound, err)
			return
		}
		utils.NewError(c, http.StatusBadRequest, err)
		return
	}

	c.JSON(http.StatusOK, permission)
	return
}

// delete godoc
//
//	@Summary	Удалить разрешение
//	@Tags		permission
//	@Accept		json
//	@Produce	json
//
//	@Param		permission_id	path		string	true	"Object ID"
//
//	@Success	200				{object}	models.Permission
//	@Failure	400				{object}	utils.HTTPError
//	@Failure	404				{object}	utils.HTTPError
//	@Failure	500				{object}	utils.HTTPError
//
//	@Router		/admin/permission{permission_id} [delete]
func (ps *PermissionServer) delete(c *gin.Context) {
	permissionID, err := uuid.Parse(c.Param("permission_id"))
	if err != nil {
		utils.NewError(c, http.StatusBadRequest, apperr.ErrInvalidIDType)
		return
	}

	if err := ps.service.DeleteByID(permissionID); err != nil {
		utils.NewError(c, http.StatusNotFound, err)
		return
	}

	c.JSON(http.StatusNoContent, nil)
	return
}
