package http

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/golang/mock/gomock"
	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"gorm.io/gorm"
	"net/http"
	"net/http/httptest"
	apperr "sso/internal/consts/errors"
	mocksrv "sso/internal/mock/services"
	"sso/internal/pkg/pg"
	"sso/internal/pkg/test"
	"sso/internal/pkg/utils"
	"strconv"
	"testing"
)

func intiActionServer(router *gin.Engine, service actionService) {
	s := NewActionServer(service)
	adminGroup := router.Group("/admin")
	s.RegisterAdminRouts(adminGroup)
}

func TestAction_get(t *testing.T) {
	const target = "/admin/action"
	const method = http.MethodGet

	ctr := gomock.NewController(t)
	defer ctr.Finish()

	tests := []struct {
		name       string
		service    actionService
		actionId   string
		statusCode int
		wantErr    bool
		err        error
	}{
		{
			name:       "exist action",
			service:    mocksrv.SimulateAction(ctr, nil),
			actionId:   uuid.New().String(),
			wantErr:    false,
			statusCode: http.StatusOK,
		},
		{
			name:       "invalid id",
			actionId:   "qwerty",
			wantErr:    true,
			statusCode: http.StatusBadRequest,
			err:        apperr.ErrInvalidIDType,
		},
		{
			name:       "not exist action",
			service:    mocksrv.SimulateAction(ctr, apperr.ErrActionNotFound),
			actionId:   uuid.New().String(),
			wantErr:    true,
			statusCode: http.StatusNotFound,
			err:        apperr.ErrActionNotFound,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			router := test.GetRouter()
			w := httptest.NewRecorder()
			intiActionServer(router, tt.service)
			req := httptest.NewRequest(method, fmt.Sprintf("%s/%s", target, tt.actionId), nil)
			router.ServeHTTP(w, req)

			if tt.wantErr {
				err := utils.HTTPError{Message: tt.err.Error()}
				jsonErr, _ := json.Marshal(err)

				assert.Equal(t, tt.statusCode, w.Code)
				assert.Equal(t, jsonErr, w.Body.Bytes())
			} else {
				assert.Equal(t, tt.statusCode, w.Code)
			}
		})
	}
}

func TestAction_create(t *testing.T) {
	const target = "/admin/action/"
	const method = http.MethodPost

	ctr := gomock.NewController(t)
	defer ctr.Finish()

	tests := []struct {
		name       string
		service    actionService
		action     map[string]string
		statusCode int
		wantErr    bool
		err        error
	}{
		{
			name:    "create Action 1",
			service: mocksrv.SimulateAction(ctr, nil),
			action: map[string]string{
				"name":   "test Action",
				"action": "create",
			},
			statusCode: http.StatusCreated,
			wantErr:    false,
		},
		{
			name:    "create Action 2",
			service: mocksrv.SimulateAction(ctr, nil),
			action: map[string]string{
				"name":   "test Action",
				"action": "update",
			},
			statusCode: http.StatusCreated,
			wantErr:    false,
		},
		{
			name:    "create Action 3",
			service: mocksrv.SimulateAction(ctr, nil),
			action: map[string]string{
				"name":   "test Action",
				"action": "delete",
			},
			statusCode: http.StatusCreated,
			wantErr:    false,
		},
		{
			name:    "create Action 4",
			service: mocksrv.SimulateAction(ctr, nil),
			action: map[string]string{
				"name":   "test Action",
				"action": "read",
			},
			statusCode: http.StatusCreated,
			wantErr:    false,
		},
		{
			name:    "unknown action type",
			service: mocksrv.SimulateAction(ctr, apperr.ErrUnknownActionType),
			action: map[string]string{
				"name":   "test Action",
				"action": "qwerty",
			},
			statusCode: http.StatusBadRequest,
			wantErr:    true,
			err:        apperr.ErrUnknownActionType,
		},
		{
			name:    "storages create error",
			service: mocksrv.SimulateAction(ctr, gorm.ErrInvalidDB),
			action: map[string]string{
				"name":   "test Action",
				"action": "create",
			},
			statusCode: http.StatusBadRequest,
			wantErr:    true,
			err:        gorm.ErrInvalidDB,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			router := test.GetRouter()
			w := httptest.NewRecorder()

			intiActionServer(router, tt.service)

			jsonValue, _ := json.Marshal(tt.action)
			req := httptest.NewRequest(method, target, bytes.NewBuffer(jsonValue))
			router.ServeHTTP(w, req)

			if tt.wantErr {
				err := utils.HTTPError{Message: tt.err.Error()}
				jsonErr, _ := json.Marshal(err)

				assert.Equal(t, tt.statusCode, w.Code)
				assert.Equal(t, jsonErr, w.Body.Bytes())
			} else {
				assert.Equal(t, tt.statusCode, w.Code)
			}
		})
	}
}

func TestAction_update(t *testing.T) {
	const target = "/admin/action"
	const method = http.MethodPatch

	ctr := gomock.NewController(t)
	defer ctr.Finish()

	tests := []struct {
		name       string
		service    actionService
		actionID   string
		action     map[string]string
		statusCode int
		wantErr    bool
		err        error
	}{
		{
			name:     "update Action 1",
			service:  mocksrv.SimulateAction(ctr, nil),
			actionID: uuid.New().String(),
			action: map[string]string{
				"name":   "test Action",
				"action": "create",
			},
			statusCode: http.StatusOK,
			wantErr:    false,
		},
		{
			name:     "update Action 2",
			service:  mocksrv.SimulateAction(ctr, nil),
			actionID: uuid.New().String(),
			action: map[string]string{
				"name":   "test Action",
				"action": "update",
			},
			statusCode: http.StatusOK,
			wantErr:    false,
		},
		{
			name:     "update Action 3",
			service:  mocksrv.SimulateAction(ctr, nil),
			actionID: uuid.New().String(),
			action: map[string]string{
				"name":   "test Action",
				"action": "delete",
			},
			statusCode: http.StatusOK,
			wantErr:    false,
		},
		{
			name:     "update Action 4",
			service:  mocksrv.SimulateAction(ctr, nil),
			actionID: uuid.New().String(),
			action: map[string]string{
				"name":   "test Action",
				"action": "read",
			},
			statusCode: http.StatusOK,
			wantErr:    false,
		},
		{
			name:     "unknown Action",
			service:  mocksrv.SimulateAction(ctr, apperr.ErrUnknownActionType),
			actionID: uuid.New().String(),
			action: map[string]string{
				"name":   "test Action",
				"action": "qwerty",
			},
			statusCode: http.StatusBadRequest,
			wantErr:    true,
			err:        apperr.ErrUnknownActionType,
		},
		{
			name:     "storages update Action error",
			service:  mocksrv.SimulateAction(ctr, gorm.ErrInvalidDB),
			actionID: uuid.New().String(),
			action: map[string]string{
				"name":   "test Action",
				"action": "create",
			},
			statusCode: http.StatusBadRequest,
			wantErr:    true,
			err:        gorm.ErrInvalidDB,
		},
		{
			name:     "incorrect action id",
			service:  mocksrv.SimulateAction(ctr, nil),
			actionID: "qwerty",
			action: map[string]string{
				"name":   "test Action",
				"action": "create",
			},
			statusCode: http.StatusBadRequest,
			wantErr:    true,
			err:        apperr.ErrInvalidIDType,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			router := test.GetRouter()
			w := httptest.NewRecorder()

			intiActionServer(router, tt.service)

			jsonValue, _ := json.Marshal(tt.action)
			req := httptest.NewRequest(method, fmt.Sprintf("%s/%s", target, tt.actionID), bytes.NewBuffer(jsonValue))
			router.ServeHTTP(w, req)

			if tt.wantErr {
				err := utils.HTTPError{Message: tt.err.Error()}
				jsonErr, _ := json.Marshal(err)

				assert.Equal(t, tt.statusCode, w.Code)
				assert.Equal(t, jsonErr, w.Body.Bytes())
			} else {
				assert.Equal(t, tt.statusCode, w.Code)
			}
		})
	}
}

func TestAction_delete(t *testing.T) {
	const target = "/admin/action"
	const method = http.MethodDelete

	ctr := gomock.NewController(t)
	defer ctr.Finish()

	tests := []struct {
		name       string
		service    actionService
		actionID   string
		statusCode int
		wantErr    bool
		err        error
	}{
		{
			name:       "delete exist action",
			service:    mocksrv.SimulateAction(ctr, nil),
			actionID:   uuid.New().String(),
			wantErr:    false,
			statusCode: http.StatusNoContent,
		},
		{
			name:       "invalid action id",
			actionID:   "qwerty",
			wantErr:    true,
			statusCode: http.StatusBadRequest,
			err:        apperr.ErrInvalidIDType,
		},
		{
			name:       "not exist action",
			service:    mocksrv.SimulateAction(ctr, apperr.ErrActionNotFound),
			actionID:   uuid.New().String(),
			wantErr:    true,
			statusCode: http.StatusNotFound,
			err:        apperr.ErrActionNotFound,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			router := test.GetRouter()
			w := httptest.NewRecorder()
			intiActionServer(router, tt.service)

			req := httptest.NewRequest(method, fmt.Sprintf("%s/%s", target, tt.actionID), nil)
			router.ServeHTTP(w, req)

			if tt.wantErr {
				err := utils.HTTPError{Message: tt.err.Error()}
				jsonErr, _ := json.Marshal(err)

				assert.Equal(t, tt.statusCode, w.Code)
				assert.Equal(t, jsonErr, w.Body.Bytes())
			} else {
				assert.Equal(t, tt.statusCode, w.Code)
			}
		})
	}
}

func TestAction_list(t *testing.T) {
	const target = "/admin/action/list"
	const method = http.MethodGet

	ctr := gomock.NewController(t)
	defer ctr.Finish()

	tests := []struct {
		name           string
		service        actionService
		statusCode     int
		withPagination bool
		pagination     pg.Request
		wantRes        pg.Response
	}{
		{
			name:           "without pagination",
			service:        mocksrv.SimulateAction(ctr, nil),
			statusCode:     http.StatusOK,
			withPagination: false,
		},
		{
			name:           "with pagination 1",
			service:        mocksrv.SimulateAction(ctr, nil),
			statusCode:     http.StatusOK,
			withPagination: true,
			pagination: pg.Request{
				Page:     1,
				PageSize: 1,
			},
		},
		{
			name:           "with pagination 2",
			service:        mocksrv.SimulateAction(ctr, nil),
			statusCode:     http.StatusOK,
			withPagination: true,
			pagination: pg.Request{
				Page:     0,
				PageSize: 0,
			},
		},
		{
			name:           "with pagination 3",
			service:        mocksrv.SimulateAction(ctr, nil),
			statusCode:     http.StatusOK,
			withPagination: true,
			pagination: pg.Request{
				Page:     12,
				PageSize: 0,
			},
		},
		{
			name:           "with pagination 4",
			service:        mocksrv.SimulateAction(ctr, nil),
			statusCode:     http.StatusOK,
			withPagination: true,
			pagination: pg.Request{
				Page:     2,
				PageSize: 10,
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			query := ""
			router := test.GetRouter()
			w := httptest.NewRecorder()
			intiActionServer(router, tt.service)

			if tt.withPagination {
				queryKeys := make(map[string]string, 2)
				if tt.pagination.Page != 0 {
					queryKeys["page"] = strconv.Itoa(tt.pagination.Page)
				}
				if tt.pagination.PageSize != 0 {
					queryKeys["page_size"] = strconv.Itoa(tt.pagination.PageSize)
				}
				query += "?"

				for key, value := range queryKeys {
					query += key + "=" + value + "&"
				}
			}

			req := httptest.NewRequest(method, fmt.Sprintf("%s%s", target, query), nil)
			router.ServeHTTP(w, req)

			assert.Equal(t, tt.statusCode, w.Code)

			res := pg.Response{}
			_ = json.Unmarshal(w.Body.Bytes(), &res)

			//if tt.pagination.PageSize != 0 {
			//	assert.Equal(t, int64(0), res.Meta.ObjectsCount)
			//}
		})
	}
}
