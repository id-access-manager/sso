//go:generate mockgen -source=user.go -destination=../../mock/services/user.go -package=services
//go:generate swag fmt && swag init -o ./api -g internal/transport/http/user.go
package http

import (
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"net/http"
	apperr "sso/internal/consts/errors"
	"sso/internal/models"
	"sso/internal/pkg/pg"
	"sso/internal/pkg/utils"
	"sso/internal/transport/http/schemas"
)

type userService interface {
	GetById(id uuid.UUID) (user models.User, err error)
	GetAll(limit int, offset int, sort string) ([]models.User, error)
	Count() (int64, error)
	Register(email models.Email, username string, password string) (user models.User, err error)
}

type UserServer struct {
	service userService
}

func NewUserServer(service userService) *UserServer {
	return &UserServer{
		service: service,
	}
}

func (srv *UserServer) RegisterAdminRouts(adminGroup *gin.RouterGroup) {
	userAdminGroup := adminGroup.Group("user")
	{
		userAdminGroup.POST("/", srv.create)
		userAdminGroup.GET("/:role_id", srv.get)
		userAdminGroup.GET("/list", srv.list)
		userAdminGroup.PATCH("/:role_id", srv.update)
		userAdminGroup.DELETE("/:role_id", srv.delete)
	}
}

func (srv *UserServer) RegisterPublicRouts(publicGroup *gin.RouterGroup) {
	userPublicGroup := publicGroup.Group("user")
	{
		userPublicGroup.POST("/register", srv.register)
	}
}

// register godoc
//
//	@Summary	Регистрация пользователя
//
//	@Tags		user
//	@Accept		json
//	@Produce	json
//
//	@Param		user	body		registerRequest	true	"User"
//
//	@Success	200		{object}	models.User
//	@Failure	400		{object}	utils.HTTPError
//	@Failure	404		{object}	utils.HTTPError
//	@Failure	500		{object}	utils.HTTPError
//
//	@Router		/api/user/register [post]
func (srv *UserServer) register(c *gin.Context) {
	var req schemas.RegisterUserRequest
	if err := c.ShouldBind(&req); err != nil {
		utils.NewError(c, http.StatusBadRequest, err)
		return
	}
	user, err := srv.service.Register(req.Email, req.Username, req.Password)
	if err != nil {
		utils.NewError(c, http.StatusBadRequest, err)
		return
	}
	c.JSON(http.StatusCreated, user)
	return
}

// create godoc
//
//	@Summary		Создать пользователя
//
//	@Description	Не реализовано!!!
//
//	@Tags			user
//	@Accept			json
//	@Produce		json
//
//	@Param			user	body		roleRequest	true	"User"
//
//	@Success		200		{object}	models.Role
//	@Failure		400		{object}	utils.HTTPError
//	@Failure		404		{object}	utils.HTTPError
//	@Failure		500		{object}	utils.HTTPError
//
//	@Deprecated		true
//
//	@Router			/admin/user/ [post]
func (srv *UserServer) create(c *gin.Context) {
	// TODO implement
	panic("implement me")
}

// get godoc
//
//	@Summary	Получить пользователя
//	@Tags		user
//	@Accept		json
//	@Produce	json
//
//	@Param		user_id	path		string	true	"User ID"
//
//	@Success	200		{object}	models.Role
//	@Failure	400		{object}	utils.HTTPError
//	@Failure	404		{object}	utils.HTTPError
//	@Failure	500		{object}	utils.HTTPError
//
//	@Router		/admin/user/{user_id} [get]
func (srv *UserServer) get(c *gin.Context) {
	id, err := uuid.Parse(c.Param("role_id"))
	if err != nil {
		utils.NewError(c, http.StatusBadRequest, apperr.ErrInvalidIDType)
		return
	}

	role, err := srv.service.GetById(id)
	if err != nil {
		utils.NewError(c, http.StatusNotFound, err)
		return
	}

	c.JSON(http.StatusOK, role)
	return
}

// list godoc
//
//	@Summary	Получить список пользователей
//	@Tags		user
//	@Accept		json
//	@Produce	json
//
//	@Param		page		query		string	true	"Page number"
//	@Param		page_size	query		string	true	"Page Size"
//
//	@Success	200			{object}	models.Role
//	@Failure	400			{object}	utils.HTTPError
//	@Failure	404			{object}	utils.HTTPError
//	@Failure	500			{object}	utils.HTTPError
//
//	@Router		/admin/user/list [get]
func (srv *UserServer) list(c *gin.Context) {
	var request pg.Request

	if err := c.ShouldBindQuery(&request); err != nil {
		utils.NewError(c, http.StatusBadRequest, err)
		return
	}

	permissions, err := srv.service.GetAll(request.GetLimit(), request.GetOffset(), request.GetSort())
	if err != nil {
		utils.NewError(c, http.StatusInternalServerError, err)
		return
	}

	total, err := srv.service.Count()
	if err != nil {
		utils.NewError(c, http.StatusInternalServerError, err)
		return
	}

	response := pg.NewResponse(permissions, int64(len(permissions)), total, &request)

	c.JSON(http.StatusOK, response)
	return
}

// update godoc
//
//	@Summary		Обновить пользователя
//
//	@Description	Не реализовано!!!
//
//	@Tags			user
//	@Accept			json
//	@Produce		json
//
//	@Param			user	body		roleRequest	true	"Role"
//	@Param			user_id	path		string		true	"Role ID"
//
//	@Success		200		{object}	models.Role
//	@Failure		400		{object}	utils.HTTPError
//	@Failure		404		{object}	utils.HTTPError
//	@Failure		500		{object}	utils.HTTPError
//
//	@Deprecated		true
//
//	@Router			/admin/user/{role_id} [patch]
func (srv *UserServer) update(c *gin.Context) {
	// TODO implement
	panic("implement me")
}

// delete godoc
//
//	@Summary		Удалить пользователя
//
//	@Description	Не реализовано!!!
//
//	@Tags			user
//	@Accept			json
//	@Produce		json
//
//	@Param			user_id	path		string	true	"Role ID"
//
//	@Success		200		{object}	models.Role
//	@Failure		400		{object}	utils.HTTPError
//	@Failure		404		{object}	utils.HTTPError
//	@Failure		500		{object}	utils.HTTPError
//
//	@Deprecated		true
//
//	@Router			/admin/user/{user_id} [delete]
func (srv *UserServer) delete(c *gin.Context) {
	// TODO implement
	panic("implement me")
}
