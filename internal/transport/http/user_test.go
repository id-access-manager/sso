package http

import (
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/golang/mock/gomock"
	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	apperr "sso/internal/consts/errors"
	"sso/internal/mock/gen"
	msrv "sso/internal/mock/services"
	"sso/internal/models"
	"sso/internal/pkg/pg"
	testpkg "sso/internal/pkg/test"
	"strconv"
	"testing"
)

func initUserServer(router *gin.Engine, service userService) {
	s := NewUserServer(service)
	adminGroup := router.Group("/admin")
	s.RegisterAdminRouts(adminGroup)
}

func TestUserServer_create(t *testing.T) {
	const target = "/admin/user/"
	const method = http.MethodPost

	ctr := gomock.NewController(t)
	defer ctr.Finish()

	// TODO add test
}

func TestUserServer_get(t *testing.T) {
	const target = "/admin/user"
	const method = http.MethodGet

	ctr := gomock.NewController(t)
	defer ctr.Finish()

	tests := []struct {
		name     string
		service  userService
		id       string
		wantCode int
		wantErr  bool
		err      error
	}{
		{
			name:     "exist user",
			service:  msrv.SimulateUser(ctr, nil),
			id:       uuid.NewString(),
			wantCode: http.StatusOK,
			wantErr:  false,
		},
		{
			name:     "invalid id",
			service:  msrv.SimulateUser(ctr, nil),
			id:       gen.GenerateString(),
			wantCode: http.StatusBadRequest,
			wantErr:  true,
			err:      apperr.ErrInvalidIDType,
		},
		{
			name:     "not found",
			service:  msrv.SimulateUser(ctr, apperr.ErrUserNotFound),
			id:       uuid.NewString(),
			wantCode: http.StatusNotFound,
			wantErr:  true,
			err:      apperr.ErrUserNotFound,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			router := testpkg.GetRouter()
			w := httptest.NewRecorder()

			initUserServer(router, test.service)

			req, _ := http.NewRequest(method, fmt.Sprintf("%s/%s", target, test.id), nil)
			router.ServeHTTP(w, req)

			res := w.Result()

			assert.Equal(t, test.wantCode, res.StatusCode)

			if test.wantErr {
				testpkg.AssertHTTPError(t, test.wantCode, test.err, w)
			} else {
				var user models.Resource
				_ = json.Unmarshal(w.Body.Bytes(), &user)

				assert.NotEmpty(t, user)
			}
		})
	}
}

func TestUserServer_list(t *testing.T) {
	const target = "/admin/user/list"
	const method = http.MethodGet

	ctr := gomock.NewController(t)
	defer ctr.Finish()

	tests := []struct {
		name           string
		service        userService
		statusCode     int
		withPagination bool
		pagination     pg.Request
		wantRes        pg.Response
	}{
		{
			name:           "without pagination",
			service:        msrv.SimulateUser(ctr, nil),
			statusCode:     http.StatusOK,
			withPagination: false,
		},
		{
			name:           "with pagination 1",
			service:        msrv.SimulateUser(ctr, nil),
			statusCode:     http.StatusOK,
			withPagination: true,
			pagination: pg.Request{
				Page:     1,
				PageSize: 1,
			},
		},
		{
			name:           "with pagination 2",
			service:        msrv.SimulateUser(ctr, nil),
			statusCode:     http.StatusOK,
			withPagination: true,
			pagination: pg.Request{
				Page:     0,
				PageSize: 0,
			},
		},
		{
			name:           "with pagination 3",
			service:        msrv.SimulateUser(ctr, nil),
			statusCode:     http.StatusOK,
			withPagination: true,
			pagination: pg.Request{
				Page:     12,
				PageSize: 0,
			},
		},
		{
			name:           "with pagination 4",
			service:        msrv.SimulateUser(ctr, nil),
			statusCode:     http.StatusOK,
			withPagination: true,
			pagination: pg.Request{
				Page:     2,
				PageSize: 10,
			},
		},
	}

	for _, test := range tests {
		t.Run(test.name,
			func(t *testing.T) {
				query := ""
				router := testpkg.GetRouter()
				w := httptest.NewRecorder()

				initUserServer(router, test.service)

				if test.withPagination {
					queryKeys := make(map[string]string, 2)
					if test.pagination.Page != 0 {
						queryKeys["page"] = strconv.Itoa(test.pagination.Page)
					}
					if test.pagination.PageSize != 0 {
						queryKeys["page_size"] = strconv.Itoa(test.pagination.PageSize)
					}
					query += "?"

					for key, value := range queryKeys {
						query += key + "=" + value + "&"
					}
				}

				req := httptest.NewRequest(method, fmt.Sprintf("%s%s", target, query), nil)
				router.ServeHTTP(w, req)

				assert.Equal(t, test.statusCode, w.Code)

				res := pg.Response{}
				_ = json.Unmarshal(w.Body.Bytes(), &res)

				assert.Equal(t, test.pagination.GetPage(), res.Meta.Page)
			},
		)
	}
}

func TestUserServer_update(t *testing.T) {
	const target = "/admin/user"
	const method = http.MethodPatch

	ctr := gomock.NewController(t)
	defer ctr.Finish()

	// TODO add test
}

func TestUserServer_delete(t *testing.T) {
	const target = "/admin/user"
	const method = http.MethodDelete

	ctr := gomock.NewController(t)
	defer ctr.Finish()

	// TODO add test
}
