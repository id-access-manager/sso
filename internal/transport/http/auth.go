//go:generate mockgen -source=auth.go -destination=../../mock/services/auth.go -package=services
//go:generate swag fmt && swag init -o ./api -g internal/transport/http/auth.go
package http

import (
	"errors"
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
	apperr "sso/internal/consts/errors"
	"sso/internal/dto"
	"sso/internal/pkg/utils"
	"sso/internal/transport/http/schemas"
)

type authService interface {
	Login(username string, password string) (dto.Tokens, error)
	Refresh(refreshToken string) (dto.Tokens, error)
}

type AuthServer struct {
	service authService
}

func NewAuthServer(service authService) *AuthServer {
	return &AuthServer{service}
}

func (srv *AuthServer) RegisterPublicRouts(group *gin.RouterGroup) {
	authUserGroup := group.Group("/auth")
	{
		authUserGroup.POST("/", srv.login)
		authUserGroup.POST("/refresh", srv.refresh)
	}
}

func (srv *AuthServer) RegisterStaticRouts(router *gin.Engine) {
	router.LoadHTMLGlob("./web/template/*")
	router.Static("static", "web/static")
	{
		router.GET("/auth", srv.loginForm)
		router.POST("/auth", srv.loginForm)
	}
}

func (srv *AuthServer) loginForm(c *gin.Context) {
	if c.Request.Method == http.MethodGet {
		c.HTML(http.StatusOK, "login.gohtml", nil)
		return
	} else if c.Request.Method == http.MethodPost {
		username := c.PostForm("username")
		password := c.PostForm("password")
		tokens, err := srv.service.Login(username, password)
		if err != nil {
			fmt.Println(err)
			return
		}
		fmt.Println(tokens)
		return
	}
}

// login godoc
//
//	@Summary	Вход пользователя
//
//	@Tags		auth
//	@Accept		json
//	@Produce	json
//
//	@Param		user	body		loginRequest	true	"Login Data"
//
//	@Success	200		{object}	token.Tokens
//	@Failure	400		{object}	utils.HTTPError
//	@Failure	404		{object}	utils.HTTPError
//	@Failure	500		{object}	utils.HTTPError
//
//	@Router		/api/auth/ [post]
func (srv *AuthServer) login(c *gin.Context) {
	var req schemas.LoginRequest
	if err := c.ShouldBind(&req); err != nil {
		utils.NewError(c, http.StatusBadRequest, err)
		return
	}
	tokens, err := srv.service.Login(req.Username, req.Password)
	if err != nil {
		if errors.Is(err, apperr.ErrUserNotFound) || errors.Is(err, apperr.ErrInvalidPassword) {
			utils.NewError(c, http.StatusUnauthorized, apperr.ErrInvalidUsernameOrPassword)
			return
		}
		utils.NewError(c, http.StatusBadRequest, err)
		return
	}
	c.JSON(http.StatusOK, tokens)
	return
}

// refresh godoc
//
//	@Summary	Обновление токенов
//
//	@Tags		auth
//	@Accept		json
//	@Produce	json
//
//	@Param		user	body		refreshRequest	true	"Refresh Data"
//
//	@Success	200		{object}	token.Tokens
//	@Failure	400		{object}	utils.HTTPError
//	@Failure	404		{object}	utils.HTTPError
//	@Failure	500		{object}	utils.HTTPError
//
//	@Router		/api/auth/register [post]
func (srv *AuthServer) refresh(c *gin.Context) {
	var req schemas.RefreshRequest
	if err := c.ShouldBindJSON(&req); err != nil {
		utils.NewError(c, http.StatusUnprocessableEntity, err)
		return
	}
	tokens, err := srv.service.Refresh(req.RefreshToken)
	if err != nil {
		utils.NewError(c, http.StatusUnprocessableEntity, err)
		return
	}
	c.JSON(http.StatusOK, tokens)
	return
}
