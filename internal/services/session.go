//go:generate mockgen -source=session.go -destination=../mock/storages/session.go -package=storages

package services

import (
	"errors"
	"github.com/google/uuid"
	"gorm.io/gorm"
	"log/slog"
	apperr "sso/internal/consts/errors"
	"sso/internal/models"
	"sso/internal/pkg/logger/sl"
	"time"
)

type (
	sessionStorage interface {
		sessionCreator
		sessionGetter
		sessionUpdater
		sessionDeleter
	}

	sessionCreator interface {
		Create(session *models.Session) error
	}

	sessionGetter interface {
		GetByID(session *models.Session, id uuid.UUID) error
		GetValidByID(session *models.Session, id uuid.UUID) error
	}

	sessionUpdater interface {
		Update(session *models.Session, data map[string]any) error
	}

	sessionDeleter interface {
		Delete(session *models.Session) error
	}

	// Session implement business mock_storages for models.Session
	Session struct {
		log        *slog.Logger
		storage    sessionStorage
		timeToLife time.Duration
	}
)

// NewSession construct instance of Session
func NewSession(log *slog.Logger, storage sessionStorage, ttl time.Duration) *Session {
	return &Session{
		log:        log,
		storage:    storage,
		timeToLife: ttl,
	}
}

// Create new models.Session
func (s *Session) Create(user models.User) (session models.Session, err error) {
	const op = "mock_services.Session.Create"

	log := s.log.With(
		slog.String("op", op),
		slog.String("account id", user.ID.String()),
	)
	log.Info("creating account session")

	now := time.Now()

	session = models.Session{
		User:      user,
		CreatedAt: now,
		ExpiredAt: now.Add(s.timeToLife),
	}

	if err := s.storage.Create(&session); err != nil {
		log.Error("failed to create session", sl.Err(err))
		return models.Session{}, err
	}

	log.Debug("successfully created session")

	return session, err
}

// GetByID models.Session instance by id
func (s *Session) GetByID(id uuid.UUID) (session models.Session, err error) {
	const op = "mock_services.Session.GetByID"

	log := s.log.With(
		slog.String("op", op),
		slog.String("session id", id.String()),
	)
	log.Info("getting account session")

	if err := s.storage.GetByID(&session, id); err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			log.Info("session not found")
			return models.Session{}, apperr.ErrSessionNotFound
		}
		log.Error("failed to find session", sl.Err(err))
		return models.Session{}, err
	}

	log.Debug("successfully found session")

	return session, err
}

// GetValidSessionByID models.Session instance where `ExpiredAt > now AND id = uuid.UUID`
func (s *Session) GetValidSessionByID(id uuid.UUID) (session models.Session, err error) {
	const op = "mock_services.Session.GetValidSessionByID"

	log := s.log.With(
		slog.String("op", op),
		slog.String("session id", id.String()),
	)
	log.Info("getting valid session by ID")

	if err = s.storage.GetValidByID(&session, id); err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			log.Info("session not found")
			return models.Session{}, apperr.ErrSessionNotFound
		}

		log.Error("failed to find valid session by ID", sl.Err(err))
		return models.Session{}, err
	}

	log.Debug("successfully found valid session by ID")

	return session, nil
}

// Refresh models.Session for models.User
func (s *Session) Refresh(session *models.Session, expiredAt time.Time) error {
	const op = "mock_services.Session.Refresh"

	log := s.log.With(slog.String("op", op), slog.String("session id", session.ID.String()))
	log.Info("refreshing account session")

	if err := s.storage.Update(session, map[string]any{"expired_at": expiredAt}); err != nil {
		log.Error("failed to refresh session", sl.Err(err))
		return err
	}

	log.Debug("successfully refreshed session")

	return nil
}

// delete instance of models.User
//func (s *Session) delete(session *models.Session) error {
//	const op = "mock_services.Session.delete"
//	log := s.log.With(
//		slog.String("op", op),
//		slog.String("session id", session.ID.String()),
//	)
//	log.Info("deleting session")
//
//	if err := s.database.Delete(session).Error; err != nil {
//		log.Error("failed to delete session", sl.Err(err))
//		return err
//	}
//
//	log.Debug("successfully deleted session")
//
//	return nil
//}

// DeleteByID instance of models.Session got by GetByID
func (s *Session) DeleteByID(id uuid.UUID) error {
	const op = "mock_services.Session.DeleteByUserID"
	log := s.log.With(
		slog.String("op", op),
		slog.String("session id", id.String()),
	)
	log.Info("deleting session by ID")

	session, err := s.GetByID(id)
	if err != nil {
		log.Error("failed to delete session", sl.Err(err))
		return err
	}

	if err := s.storage.Delete(&session); err != nil {
		log.Error("failed to delete session", sl.Err(err))
		return err
	}

	log.Debug("successfully deleted session")

	return nil
}
