//go:generate mockgen -source=permission.go -destination=../mock/storages/permission.go -package=storages

package services

import (
	"errors"
	"github.com/google/uuid"
	"gorm.io/gorm"
	"log/slog"
	apperr "sso/internal/consts/errors"
	"sso/internal/models"
	"sso/internal/pkg/logger/sl"
)

type (
	permissionStorage interface {
		permissionCreator
		permissionGetter
		permissionUpdater
		permissionDeleter
	}

	permissionCreator interface {
		Create(permission *models.Permission) error
	}

	permissionGetter interface {
		GetByID(permission *models.Permission, id uuid.UUID) error
		GetList(permissions *[]models.Permission, limit int, offset int, sort string) error
		Count(total *int64) error
		GetByIDs(permissions *[]models.Permission, ids []uuid.UUID) error
	}

	permissionUpdater interface {
		Update(permission *models.Permission, data map[string]any) error
	}

	permissionDeleter interface {
		Delete(permission *models.Permission) error
	}
)

// Permission implement services business mock_storages
type Permission struct {
	log     *slog.Logger
	storage permissionStorage
	action  *Action
	object  *Object
}

// NewPermission construct instance of Permission
func NewPermission(log *slog.Logger, storage permissionStorage, actionService *Action,
	objectService *Object) *Permission {
	return &Permission{
		log:     log,
		storage: storage,
		action:  actionService,
		object:  objectService,
	}
}

// Create instance of models.Permission
func (p *Permission) Create(name string, actionID uuid.UUID, objectID uuid.UUID) (permission models.Permission, err error) {
	const op = "services.Permission.Create"
	log := p.log.With(
		slog.String("op", op),
	)

	log.Info("creating permission")

	action, err := p.action.GetByID(actionID)
	if err != nil {
		return models.Permission{}, err
	}

	object, err := p.object.GetByID(objectID)
	if err != nil {
		return models.Permission{}, err
	}

	permission = models.Permission{
		Name:   name,
		Object: object,
		Action: action,
	}

	if err := p.storage.Create(&permission); err != nil {
		log.Error("failed to create permission", sl.Err(err))
		return models.Permission{}, err
	}

	return permission, nil
}

// GetByID instance of models.Permission by id
func (p *Permission) GetByID(id uuid.UUID) (permission models.Permission, err error) {
	const op = "services.Permission.GetByID"
	log := p.log.With(
		slog.String("op", op),
		slog.String("permission id", id.String()),
	)

	log.Info("getting permission by id")

	err = p.storage.GetByID(&permission, id)
	if err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			log.Error("profile not found")
			return models.Permission{}, apperr.ErrPermissionNotFound
		}
		log.Error("failed to get permission", sl.Err(err))
		return models.Permission{}, err
	}

	return permission, nil
}

// GetList of instance of models.Permission with pg
func (p *Permission) GetList(limit int, offset int, sort string) (permissions []models.Permission, err error) {
	const op = "services.Permission.GetAll"
	log := p.log.With(
		slog.String("op", op),
		slog.Int("limit", limit),
		slog.Int("offset", offset),
		slog.String("sort", sort),
	)

	log.Info("getting list of permissions")

	if err := p.storage.GetList(&permissions, limit, offset, sort); err != nil {
		log.Error("failed to get list of permissions")
		return nil, err
	}

	log.Debug("successfully got list of permissions")

	return permissions, nil
}

func (p *Permission) GetCount() (int64, error) {
	const op = "services.Permission.GetPermissionTotal"
	var total int64

	log := p.log.With(slog.String("op", op))
	log.Info("getting total of permissions")

	if err := p.storage.Count(&total); err != nil {
		log.Error("failed to get total of permissions", sl.Err(err))
		return 0, err
	}

	log.Debug("successfully got total of permissions")

	return total, nil
}

func (p *Permission) GetByIDs(IDs []uuid.UUID) (permissions []models.Permission, err error) {
	const op = "services.Permission.GetByIDs"

	log := p.log.With(slog.String("op", op))

	log.Info("getting list of permissions")

	if err := p.storage.GetByIDs(&permissions, IDs); err != nil {
		log.Error("failed to get list of permissions", sl.Err(err))
		return nil, err
	}

	return permissions, nil
}

// UpdateByID instance of models.Permission got by GetByID
func (p *Permission) UpdateByID(id uuid.UUID, name string, actionID uuid.UUID,
	objectID uuid.UUID) (permission models.Permission, err error) {
	const op = "services.Permission.UpdateByID"
	log := p.log.With(
		slog.String("op", op),
		slog.String("permission id", id.String()),
	)

	log.Info("updating permission by id")

	_, err = p.action.GetByID(actionID)
	if err != nil {
		return models.Permission{}, err
	}

	_, err = p.object.GetByID(objectID)
	if err != nil {
		return models.Permission{}, err
	}

	permission, err = p.GetByID(id)
	if err != nil {
		return models.Permission{}, err
	}

	if err = p.storage.Update(&permission, map[string]any{"Name": name, "object_id": objectID,
		"action_id": actionID}); err != nil {
		log.Error("failed to update permission by id")
		return models.Permission{}, err
	}

	log.Info("permission successfully updated")

	return permission, nil
}

//// delete instance of models.Permission
//func (p *Permission) delete(permission *models.Permission) error {
//	const op = "services.Permission."
//	log := p.log.With(
//		slog.String("op", op),
//		slog.String("permission id", permission.ID.String()),
//	)
//
//	log.Info("deleting permission")
//
//	if res := p.database.Delete(&permission); res.Error != nil {
//		log.Error("failed to delete permission", sl.Err(res.Error))
//		return res.Error
//	}
//
//	return nil
//}

// DeleteByID instance of models.Permission got by GetByID
func (p *Permission) DeleteByID(id uuid.UUID) (err error) {
	const op = "services.Permission."
	var permission models.Permission
	log := p.log.With(
		slog.String("op", op),
		slog.String("permission id", id.String()),
	)

	log.Info("deleting permission by id")

	permission, err = p.GetByID(id)
	if err != nil {
		return err
	}

	if err := p.storage.Delete(&permission); err != nil {
		log.Error("failed to delete permission by id")
		return err
	}

	log.Info("permission successfully delete")

	return nil
}
