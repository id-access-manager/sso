//go:generate mockgen -source=role.go -destination=../mock/storages/role.go -package=storages

package services

import (
	"errors"
	"github.com/google/uuid"
	"gorm.io/gorm"
	"log/slog"
	apperr "sso/internal/consts/errors"
	"sso/internal/models"
	"sso/internal/pkg/logger/sl"
	"strconv"
)

type roleStorage interface {
	roleCreator
	roleGetter
	roleUpdater
	roleDeleter
}

type (
	roleCreator interface {
		Create(role *models.Role) error
		AddPermissions(role *models.Role, permissions []models.Permission) error
	}
	roleGetter interface {
		GetByID(role *models.Role, id uuid.UUID) error
		GetAll(roles *[]models.Role, limit int, offset int, sort string) error
		GetByIDs(resources *[]models.Role, ids []uuid.UUID) error
		Count(count *int64) error
	}
	roleUpdater interface {
		Update(role *models.Role, data map[string]any) error
		ReplacePermissions(role *models.Role, permissions []models.Permission) error
	}
	roleDeleter interface {
		RemovePermissions(role *models.Role, permissions []models.Permission) error
		Delete(role *models.Role) error
	}
)

// Role implement business mock_storages for models.Role
type Role struct {
	log        *slog.Logger
	storage    roleStorage
	permission *Permission
}

// NewRole construct instance of Role
func NewRole(log *slog.Logger, storage roleStorage, permission *Permission) *Role {
	return &Role{
		log:        log,
		storage:    storage,
		permission: permission,
	}
}

// Create models.Role instance
func (r *Role) Create(name string, permissionIDs []uuid.UUID) (models.Role, error) {
	const op = "services.Role.Create"

	log := r.log.With(
		slog.String("op", op),
	)
	log.Info("creating role")

	permissions, err := r.permission.GetByIDs(permissionIDs)
	if err != nil {
		log.Error("failed to get permissions", sl.Err(err))
		return models.Role{}, err
	}

	role := models.Role{
		Name:        name,
		Permissions: permissions,
	}

	if err := r.storage.Create(&role); err != nil {
		log.Error("failed to create role", sl.Err(err))
		return models.Role{}, err
	}

	if err := r.storage.AddPermissions(&role, permissions); err != nil {
		log.Error("failed to add permissions", sl.Err(err))
		return models.Role{}, err
	}

	log.Info("successfully created role")

	return role, nil
}

// GetByID models.Role instance by ID
func (r *Role) GetByID(id uuid.UUID) (role models.Role, err error) {
	const op = "services.Role."

	log := r.log.With(
		slog.String("op", op),
		slog.String("role id", id.String()),
	)
	log.Info("getting role by id")

	if err = r.storage.GetByID(&role, id); err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			log.Info("role not found")
			return models.Role{}, apperr.ErrRoleNotFound
		}
		log.Error("failed to get role", sl.Err(err))
		return models.Role{}, err
	}

	log.Info("successfully got role")

	return role, nil
}

func (r *Role) GetAll(limit int, offset int, sort string) (roles []models.Role, err error) {
	const op = "services.Role.GetAll"
	log := r.log.With(
		slog.String("op", op),
		slog.String("limit", strconv.Itoa(limit)),
		slog.String("offset", strconv.Itoa(offset)),
		slog.String("sort", sort))
	log.Info("getting roles")

	if err := r.storage.GetAll(&roles, limit, offset, sort); err != nil {
		log.Error("failed to get roles", sl.Err(err))
		return []models.Role{}, err
	}

	log.Debug("successfully got roles")

	return roles, nil
}

func (r *Role) GetByIDs(ids []uuid.UUID) (roles []models.Role, err error) {
	const op = "services.Role.GetByIDs"

	log := r.log.With(
		slog.String("op", op))

	log.Info("getting roles")

	if err := r.storage.GetByIDs(&roles, ids); err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			log.Info("role not found")
			return []models.Role{}, apperr.ErrRoleNotFound
		}
		log.Error("failed to get roles", sl.Err(err))
		return []models.Role{}, err
	}

	log.Debug("successfully got roles")
	return roles, nil
}

func (r *Role) Count() (int64, error) {
	const op = "services.Role.Count"
	var count int64
	log := r.log.With(
		slog.String("op", op))
	log.Info("counting roles")
	if err := r.storage.Count(&count); err != nil {
		log.Error("failed to count roles", sl.Err(err))
		return 0, err
	}
	log.Debug("successfully got roles")
	return count, nil
}

// UpdateByID models.Role instance got by GetByID
func (r *Role) UpdateByID(id uuid.UUID, name string, permissionIDs []uuid.UUID) (models.Role, error) {
	const op = "services.Role."

	log := r.log.With(
		slog.String("op", op),
		slog.String("role id", id.String()),
	)
	log.Info("updating role by id")

	role, err := r.GetByID(id)
	if err != nil {
		return models.Role{}, err
	}

	permissions, err := r.permission.GetByIDs(permissionIDs)
	if err != nil {
		log.Error("failed to get permissions", sl.Err(err))
		return models.Role{}, err
	}

	if err := r.storage.Update(&role, map[string]any{
		"name": name,
	}); err != nil {
		log.Error("failed to update role", sl.Err(err))
		return models.Role{}, err
	}

	if err := r.storage.ReplacePermissions(&role, permissions); err != nil {
		log.Error("failed to update role", sl.Err(err))
		return models.Role{}, err
	}

	log.Info("successfully updated role by id")

	return role, nil
}

// DeleteByID models.Role instance got by GetByID
func (r *Role) DeleteByID(id uuid.UUID) error {
	const op = "services.Role."

	log := r.log.With(
		slog.String("op", op),
		slog.String("role id", id.String()),
	)
	log.Info("deleting role by id")

	role, err := r.GetByID(id)
	if err != nil {
		log.Error("failed to get role", sl.Err(err))
		return err
	}

	if err := r.storage.Delete(&role); err != nil {
		log.Error("failed to delete role", sl.Err(err))
		return err
	}

	log.Info("successfully deleted role by id")

	return nil
}
