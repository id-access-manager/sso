//go:generate mockgen -source=access.go -destination=../../mock/storages/token/access.go -package=token

package token

import (
	"errors"
	"github.com/google/uuid"
	"gorm.io/gorm"
	"log/slog"
	apperr "sso/internal/consts/errors"
	"sso/internal/models"
	"sso/internal/models/token"
	"sso/internal/pkg/logger/sl"
	"time"
)

type (
	accessStorage interface {
		accessCreator
		accessGetter
		accessDeleter
	}

	accessCreator interface {
		Save(token *token.Access) error
		Create(token *token.Access) error
	}

	accessGetter interface {
		GetByID(token *token.Access, id uuid.UUID) error
		GetByUserID(token *token.Access, userID uuid.UUID) error
	}

	accessDeleter interface {
		Delete(token *token.Access) error
	}
)

// Access implement business mock_storages for models.AccessToken
type Access struct {
	log        *slog.Logger
	storage    accessStorage
	timeToLife time.Duration // Time ti live access token
}

// NewAccess construct instance of Access
func NewAccess(log *slog.Logger, storage accessStorage, ttl time.Duration) *Access {
	return &Access{
		log:        log,
		storage:    storage,
		timeToLife: ttl,
	}
}

// Create new access JWT token
func (at *Access) Create(user models.User, session models.Session) (accessToken token.Access, err error) {
	const op = "services.token.Access.Create"

	log := at.log.With(
		slog.String("op", op),
		slog.String("user id", user.ID.String()),
	)
	log.Info("creating new access token")

	accessToken = token.Access{
		User:    user,
		Session: session,
	}

	if err := accessToken.Generate(at.timeToLife); err != nil {
		log.Error("failed to generate access token", sl.Err(err))
		return token.Access{}, err
	}

	if err = at.storage.Create(&accessToken); err != nil {
		log.Error("failed to create access token", sl.Err(err))
		return token.Access{}, err
	}

	log.Debug("successfully created access token")

	return accessToken, nil
}

// Refresh access token
func (at *Access) Refresh(tokenRow *token.Access) error {
	const op = "services.token.Access.Refresh"

	log := at.log.With(
		slog.String("op", op),
		slog.String("token id", tokenRow.ID.String()),
	)
	log.Info("refreshing access token")
	if err := tokenRow.Refresh(at.timeToLife); err != nil {
		log.Error("failed to refresh access token", sl.Err(err))
		return err
	}

	if err := at.storage.Save(tokenRow); err != nil {
		log.Error("failed to save access token", sl.Err(err))
		return err
	}

	log.Debug("successfully refreshed access token")

	return nil
}

// GetByID models.AccessToken instance by id
func (at *Access) GetByID(id uuid.UUID) (accessToken token.Access, err error) {
	const op = "mock_services.token.Access.GetByID"

	log := at.log.With(
		slog.String("op", op),
		slog.String("id", id.String()),
	)
	log.Info("getting access token by ID")

	if err := at.storage.GetByID(&accessToken, id); err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			log.Info("access token not found")
			return token.Access{}, apperr.ErrTokenNotFound
		}
		log.Error("failed to get access token by ID", sl.Err(err))
		return token.Access{}, err
	}

	log.Info("successfully got access token by ID")

	return accessToken, nil
}

// GetByUserID token.Access token by models.User
func (at *Access) GetByUserID(id uuid.UUID) (tokenRow token.Access, err error) {
	const op = "mock_services.token.Access.GetByUserID"

	log := at.log.With(
		slog.String("op", op),
		slog.String("user id", id.String()),
	)
	log.Info("getting access token by account id")

	if err = at.storage.GetByUserID(&tokenRow, id); err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			log.Info("access token not found")
			return token.Access{}, apperr.ErrTokenNotFound
		}
		log.Error("failed to get access token by user", sl.Err(err))
		return token.Access{}, err
	}

	log.Info("successfully got access token by user")

	return tokenRow, nil
}

// DeleteByUserID instance of token.Access got by GetByUserID
func (at *Access) DeleteByUserID(id uuid.UUID) error {
	const op = "mock_services.token.Access.deleteByID"
	log := at.log.With(
		slog.String("op", op),
		slog.String("id", id.String()),
	)
	log.Info("deleting access token by ID")

	tokenRow, err := at.GetByUserID(id)
	if err != nil {
		log.Error("failed to get access token by ID", sl.Err(err))
		return err
	}

	if err := at.storage.Delete(&tokenRow); err != nil {
		log.Error("failed to delete access token by ID", sl.Err(err))
		return err
	}

	log.Debug("successfully deleted access token by ID")

	return nil
}
