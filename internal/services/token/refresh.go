//go:generate mockgen -source=refresh.go -destination=../../mock/storages/token/refresh.go -package=token

package token

import (
	"errors"
	"github.com/google/uuid"
	"gorm.io/gorm"
	"log/slog"
	apperr "sso/internal/consts/errors"
	"sso/internal/models"
	"sso/internal/models/token"
	"sso/internal/pkg/jwt"
	"sso/internal/pkg/logger/sl"
	"time"
)

type (
	refreshStorage interface {
		refreshCreator
		refreshGetter
		refreshDeleter
	}

	refreshCreator interface {
		Save(token *token.Refresh) error
		Create(token *token.Refresh) error
	}

	refreshGetter interface {
		GetByID(token *token.Refresh, id uuid.UUID) error
		GetByUserID(token *token.Refresh, userID uuid.UUID) error
	}

	refreshDeleter interface {
		Delete(token *token.Refresh) error
	}
)

// Refresh implement business mock_storages for models.Refresh
type Refresh struct {
	log        *slog.Logger
	storage    refreshStorage
	timeToLife time.Duration // Time to live of Refresh
}

// NewRefresh construct instance of Refresh
func NewRefresh(log *slog.Logger, storage refreshStorage, ttl time.Duration) *Refresh {
	return &Refresh{
		log:        log,
		storage:    storage,
		timeToLife: ttl,
	}
}

// Create new access JWT token
func (rt *Refresh) Create(user models.User, session models.Session) (refreshToken token.Refresh, err error) {
	const op = "services.token.Refresh.Create"

	log := rt.log.With(
		slog.String("op", op),
		slog.String("user id", user.ID.String()),
	)
	log.Info("creating new refresh token")

	authAt := time.Now()
	expiredAt := authAt.Add(rt.timeToLife)

	refreshToken = token.Refresh{
		BaseToken: token.BaseToken{
			ID:        uuid.New(),
			ExpiredAt: expiredAt,
		},
		User:    user,
		Session: session,
	}

	log.Info("generating new refresh token")
	refreshToken.Token, err = jwt.NewRefreshToken(user, authAt, expiredAt, refreshToken.ID, session.ID)
	if err != nil {
		log.Error("failed to generate refresh token", sl.Err(err))
		return token.Refresh{}, err
	}

	if err := rt.storage.Create(&refreshToken); err != nil {
		log.Error("failed to create refresh token", sl.Err(err))
		return token.Refresh{}, err
	}

	log.Debug("successfully created refresh token")

	return refreshToken, nil
}

// GetByUserID token.Identity token by models.User
func (rt *Refresh) GetByUserID(id uuid.UUID) (tokenRow token.Refresh, err error) {
	const op = "mock_services.token.Refresh.GetByUserID"

	log := rt.log.With(
		slog.String("op", op),
		slog.String("user id", id.String()),
	)
	log.Info("getting refresh token by session")

	if err = rt.storage.GetByUserID(&tokenRow, id); err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			log.Info("refresh token not found")
			return token.Refresh{}, apperr.ErrTokenNotFound
		}
		log.Error("failed to get refresh token by user id", sl.Err(err))
		return token.Refresh{}, err
	}

	log.Info("successfully got refresh token by session")

	return tokenRow, nil
}

// GetByID models.AccessToken instance by id
func (rt *Refresh) GetByID(id uuid.UUID) (accessToken token.Refresh, err error) {
	const op = "mock_services.token.Refresh.GetByID"

	log := rt.log.With(
		slog.String("op", op),
		slog.String("id", id.String()),
	)
	log.Info("getting refresh token by ID")

	if err = rt.storage.GetByID(&accessToken, id); err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			log.Info("refresh token not found")
			return token.Refresh{}, apperr.ErrTokenNotFound
		}
		log.Error("failed to get refresh token by ID", sl.Err(err))
		return token.Refresh{}, err
	}

	log.Info("successfully got refresh token by ID")

	return accessToken, nil
}

// Refresh token.Refresh
func (rt *Refresh) Refresh(tokenRow *token.Refresh) error {
	const op = "mock_services.token.Refresh.Refresh.Refresh"

	log := rt.log.With(
		slog.String("op", op),
		slog.String("refresh token", tokenRow.ID.String()),
	)

	log.Info("refreshing access token")

	if err := tokenRow.Refresh(rt.timeToLife); err != nil {
		log.Error("failed to refresh refresh token", sl.Err(err))
		return err
	}

	if err := rt.storage.Save(tokenRow); err != nil {
		log.Error("failed to save refresh token", sl.Err(err))
		return err
	}

	log.Info("successfully refreshed access token")

	return nil
}

// DeleteByUserID instance of token.Refresh got by GetByUserID
func (rt *Refresh) DeleteByUserID(id uuid.UUID) error {
	const op = "mock_services.token.Refresh.deleteByID"
	log := rt.log.With(
		slog.String("op", op),
		slog.String("id", id.String()),
	)
	log.Info("deleting access token by ID")

	tokenRow, err := rt.GetByUserID(id)
	if err != nil {
		log.Error("failed to get access token by ID", sl.Err(err))
		return err
	}

	if err := rt.storage.Delete(&tokenRow); err != nil {
		log.Error("failed to delete access token by ID", sl.Err(err))
		return err
	}

	log.Debug("successfully deleted access token by ID")

	return nil
}
