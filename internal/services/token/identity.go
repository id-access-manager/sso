//go:generate mockgen -source=identity.go -destination=../../mock/storages/token/id.go -package=token

package token

import (
	"errors"
	"github.com/google/uuid"
	"gorm.io/gorm"
	"log/slog"
	apperr "sso/internal/consts/errors"
	"sso/internal/models"
	"sso/internal/models/token"
	"sso/internal/pkg/logger/sl"
	"time"
)

type (
	idStorage interface {
		idCreator
		idGetter
		idDeleter
	}

	idCreator interface {
		Save(token *token.Identity) error
		Create(token *token.Identity) error
	}

	idGetter interface {
		GetByID(token *token.Identity, id uuid.UUID) error
		GetByUserID(token *token.Identity, userID uuid.UUID) error
		GetBySessionID(token *token.Identity, id uuid.UUID) error
	}

	idDeleter interface {
		Delete(token *token.Identity) error
	}
)

// Identity implement business mock_storages for models.IdentityToken
type Identity struct {
	log        *slog.Logger
	storage    idStorage
	timeToLife time.Duration // Time ti live token.Identity
}

// NewIdentity construct instance of Identity
func NewIdentity(log *slog.Logger, storage idStorage, ttl time.Duration) *Identity {
	return &Identity{
		log:        log,
		storage:    storage,
		timeToLife: ttl,
	}
}

// Create instance of token.Identity
func (it *Identity) Create(user models.User, session models.Session) (identityToken token.Identity, err error) {
	const op = "services.token.Identity.Create"

	log := it.log.With(
		slog.String("op", op),
		slog.String("user id", user.ID.String()),
	)
	log.Info("creating identity token")

	idToken := token.Identity{
		User:    user,
		Session: session,
	}

	if err := idToken.Generate(it.timeToLife); err != nil {
		log.Error("failed to generate identity token", "error", err)
		return token.Identity{}, err
	}

	if err := it.storage.Create(&idToken); err != nil {
		log.Error("failed to create identity token", sl.Err(err))
		return token.Identity{}, err
	}

	log.Debug("identity token created")

	return idToken, nil
}

// GetBySession token.Identity token by models.Session
func (it *Identity) GetBySession(session models.Session) (tokenRow token.Identity, err error) {
	const op = "mock_services.token.Identity.GetByUserID"

	log := it.log.With(
		slog.String("op", op),
		slog.String("session", session.ID.String()),
	)
	log.Info("getting access token by session")

	if err = it.storage.GetBySessionID(&tokenRow, session.ID); err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			log.Info("access token not found")
			return token.Identity{}, apperr.ErrTokenNotFound
		}
		log.Error("failed to get access token by session", sl.Err(err))
		return token.Identity{}, err
	}

	log.Info("successfully got access token by session")

	return tokenRow, nil
}

// GetByUserID get token.Identity by models.User id
func (it *Identity) GetByUserID(id uuid.UUID) (tokenRow token.Identity, err error) {
	const op = "mock_services.token.Identity.GetByUserID"

	log := it.log.With(
		slog.String("op", op),
		slog.String("user id", id.String()),
	)

	log.Info("getting identity token by user id")

	if err = it.storage.GetByUserID(&tokenRow, id); err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			log.Info("identity token not found")
			return token.Identity{}, apperr.ErrTokenNotFound
		}
		log.Error("failed to get identity token by user id", sl.Err(err))
		return token.Identity{}, err
	}

	log.Info("successfully got identity token by user id")

	return tokenRow, nil
}

// GetByID models.AccessToken instance by id
func (it *Identity) GetByID(id uuid.UUID) (accessToken token.Identity, err error) {
	const op = "mock_services.token.Identity.GetByID"

	log := it.log.With(
		slog.String("op", op),
		slog.String("id", id.String()),
	)
	log.Info("getting identity token by ID")

	if err = it.storage.GetByID(&accessToken, id); err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			log.Info("identity token not found")
			return token.Identity{}, apperr.ErrTokenNotFound
		}
		log.Error("failed to get identity token by ID", sl.Err(err))
		return token.Identity{}, err
	}

	log.Info("successfully got identity token by ID")

	return accessToken, nil
}

// Refresh token.Identity
func (it *Identity) Refresh(tokenRow *token.Identity) error {
	const op = "mock_services.token.Identity.Refresh"

	log := it.log.With(
		slog.String("op", op),
		slog.String("token id", tokenRow.ID.String()),
	)

	log.Info("refreshing identity token")

	if err := tokenRow.Refresh(it.timeToLife); err != nil {
		log.Error("failed to refresh identity token", sl.Err(err))
		return err
	}

	if err := it.storage.Save(tokenRow); err != nil {
		log.Error("failed to save identity token", sl.Err(err))
		return err
	}

	log.Info("successfully refreshed identity token")

	return nil
}

// DeleteByUserID instance of token.Identity got by GetByUserID
func (it *Identity) DeleteByUserID(id uuid.UUID) error {
	const op = "mock_services.token.Identity.deleteByID"
	log := it.log.With(
		slog.String("op", op),
		slog.String("id", id.String()),
	)
	log.Info("deleting access token by ID")

	tokenRow, err := it.GetByUserID(id)
	if err != nil {
		log.Error("failed to get access token by ID", sl.Err(err))
		return err
	}

	if err := it.storage.Delete(&tokenRow); err != nil {
		log.Error("failed to delete access token by ID", sl.Err(err))
		return err
	}

	log.Debug("successfully deleted access token by ID")

	return nil
}
