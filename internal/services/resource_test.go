package services

import (
	"github.com/golang/mock/gomock"
	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"gorm.io/gorm"
	"log/slog"
	"os"
	apperr "sso/internal/consts/errors"
	"sso/internal/mock"
	"sso/internal/mock/gen"
	mstg "sso/internal/mock/storages"
	"sso/internal/pkg/logger/handlers/slogpretty"
	"testing"
)

type resourceObj struct {
	name        string
	description string
	roles       []uuid.UUID
}

// setupPrettySlog is creating custom logger for application
func setupPrettySlog() *slog.Logger {
	opts := slogpretty.PrettyHandlerOptions{
		SlogOpts: &slog.HandlerOptions{
			Level: slog.LevelDebug,
		},
	}

	handler := opts.NewPrettyHandler(os.Stdout)

	return slog.New(handler)
}

func TestResource_Create(t *testing.T) {
	ctr := gomock.NewController(t)
	defer ctr.Finish()

	tests := []struct {
		name    string
		storage resourceStorage
		role    *Role
		obj     resourceObj
		wantErr bool
		err     error
	}{
		{
			name:    "success",
			storage: mstg.SimulateResource(ctr, nil),
			role: NewRole(
				mock.Logger,
				mstg.SimulateRole(ctr, nil),
				NewPermission(
					mock.Logger,
					mstg.SimulatePermission(ctr, nil),
					NewAction(mock.Logger, mstg.SimulateAction(ctr, nil)),
					NewObject(mock.Logger, mstg.SimulateObj(ctr, nil)),
				),
			),
			obj: resourceObj{
				name:        gen.GenerateString(),
				description: gen.GenerateString(),
			},
			wantErr: false,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			s := NewResource(setupPrettySlog(), test.storage, test.role)

			resource, err := s.Create(test.obj.name, test.obj.description, test.obj.roles)

			if test.wantErr {
				assert.Error(t, err)
				assert.Empty(t, resource)
				assert.ErrorIs(t, err, test.err)
			} else {
				assert.NoError(t, err)
				assert.NotEmpty(t, resource)
				assert.Equal(t, test.obj.name, resource.Name)
				assert.Equal(t, test.obj.description, resource.Description)
			}
		})
	}
}

func TestResource_GetByID(t *testing.T) {
	ctr := gomock.NewController(t)
	defer ctr.Finish()

	tests := []struct {
		name    string
		storage resourceStorage
		id      uuid.UUID
		wantErr bool
		err     error
	}{
		{
			name:    "exists",
			storage: mstg.SimulateResource(ctr, nil),
			id:      uuid.New(),
			wantErr: false,
		},
		{
			name:    "not found",
			storage: mstg.SimulateResource(ctr, gorm.ErrRecordNotFound),
			id:      uuid.New(),
			wantErr: true,
			err:     apperr.ErrResourceNotFound,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			s := NewResource(mock.Logger, test.storage, nil)

			resource, err := s.GetByID(test.id)

			if test.wantErr {
				assert.Error(t, err)
				assert.Empty(t, resource)
				assert.ErrorIs(t, err, test.err)
			} else {
				assert.NoError(t, err)
				assert.NotEmpty(t, resource)
				assert.Equal(t, test.id, resource.ID)
			}
		})
	}
}

func TestResource_GetByIDs(t *testing.T) {
	ctr := gomock.NewController(t)
	defer ctr.Finish()

	tests := []struct {
		name    string
		storage resourceStorage
		ids     []uuid.UUID
		wantErr bool
		err     error
	}{
		{
			name:    "success",
			storage: mstg.SimulateResource(ctr, nil),
			ids: []uuid.UUID{
				uuid.New(),
				uuid.New(),
				uuid.New(),
				uuid.New(),
			},
			wantErr: false,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			s := NewResource(mock.Logger, test.storage, nil)
			resources, err := s.GetByIDs(test.ids)

			if test.wantErr {
				assert.Error(t, err)
				assert.Nil(t, resources)
				assert.ErrorIs(t, err, test.err)
			} else {
				assert.NoError(t, err)
				assert.NotNil(t, resources)
				assert.Equal(t, true, len(test.ids) == len(resources))
			}
		})
	}
}

func TestResource_GetList(t *testing.T) {
	ctr := gomock.NewController(t)
	defer ctr.Finish()

	tests := []struct {
		name    string
		storage resourceStorage
		limit   int
		offset  int
		sort    string
		wantErr bool
		err     error
	}{
		{
			name:    "success",
			storage: mstg.SimulateResource(ctr, nil),
			limit:   10,
			offset:  0,
			wantErr: false,
		},
		{
			name:    "limit empty",
			storage: mstg.SimulateResource(ctr, nil),
			limit:   0,
			offset:  0,
			wantErr: false,
		},
		{
			name:    "offset empty",
			storage: mstg.SimulateResource(ctr, nil),
			limit:   10,
			offset:  0,
			wantErr: false,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			s := NewResource(mock.Logger, test.storage, nil)
			resources, err := s.GetList(test.limit, test.offset, test.sort)
			if test.wantErr {
				assert.Error(t, err)
				assert.Nil(t, resources)
				assert.ErrorIs(t, err, test.err)
			} else {
				assert.NoError(t, err)
				assert.NotNil(t, resources)
			}
		})
	}
}

func TestResource_UpdateByID(t *testing.T) {
	ctr := gomock.NewController(t)
	defer ctr.Finish()

	tests := []struct {
		name    string
		storage resourceStorage
		roleSrv *Role
		id      uuid.UUID
		obj     resourceObj
		wantErr bool
		err     error
	}{
		{
			name:    "success",
			storage: mstg.SimulateResource(ctr, nil),
			roleSrv: NewRole(
				mock.Logger,
				mstg.SimulateRole(ctr, nil),
				NewPermission(
					mock.Logger,
					mstg.SimulatePermission(ctr, nil),
					NewAction(mock.Logger, mstg.SimulateAction(ctr, nil)),
					NewObject(mock.Logger, mstg.SimulateObj(ctr, nil)),
				),
			),
			id: uuid.New(),
			obj: resourceObj{
				name:        gen.GenerateString(),
				description: gen.GenerateString(),
			},
			wantErr: false,
		},
		{
			name:    "not found",
			storage: mstg.SimulateResource(ctr, gorm.ErrRecordNotFound),
			roleSrv: NewRole(
				mock.Logger,
				mstg.SimulateRole(ctr, nil),
				NewPermission(
					mock.Logger,
					mstg.SimulatePermission(ctr, nil),
					NewAction(mock.Logger, mstg.SimulateAction(ctr, nil)),
					NewObject(mock.Logger, mstg.SimulateObj(ctr, nil)),
				),
			),
			id: uuid.New(),
			obj: resourceObj{
				name:        gen.GenerateString(),
				description: gen.GenerateString(),
			},
			wantErr: true,
			err:     apperr.ErrResourceNotFound,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			s := NewResource(mock.Logger, test.storage, test.roleSrv)

			resource, err := s.UpdateByID(test.id, test.obj.name, test.obj.description, test.obj.roles)

			if test.wantErr {
				assert.Error(t, err)
				assert.Empty(t, resource)
				assert.Error(t, err, test.err)
			} else {
				assert.NoError(t, err)
				assert.NotEmpty(t, resource)
				assert.Equal(t, test.obj.name, resource.Name)
				assert.Equal(t, test.obj.description, resource.Description)
			}
		})
	}
}

func TestResource_DeleteByID(t *testing.T) {
	ctr := gomock.NewController(t)
	defer ctr.Finish()

	tests := []struct {
		name    string
		storage resourceStorage
		id      uuid.UUID
		wantErr bool
		err     error
	}{
		{
			name:    "exists",
			storage: mstg.SimulateResource(ctr, nil),
			id:      uuid.New(),
			wantErr: false,
		},
		{
			name:    "not found",
			storage: mstg.SimulateResource(ctr, gorm.ErrRecordNotFound),
			id:      uuid.New(),
			wantErr: true,
			err:     apperr.ErrResourceNotFound,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			s := NewResource(mock.Logger, test.storage, nil)
			err := s.DeleteByID(test.id)

			if test.wantErr {
				assert.Error(t, err)
				assert.ErrorIs(t, err, test.err)
			}
		})
	}
}
