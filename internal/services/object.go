//go:generate mockgen -source=object.go -destination=../mock/storages/obj.go -package=storages

package services

import (
	"errors"
	_ "github.com/golang/mock/gomock"
	"github.com/google/uuid"
	"gorm.io/gorm"
	"log/slog"
	apperr "sso/internal/consts/errors"
	"sso/internal/models"
	"sso/internal/pkg/logger/sl"
)

type (
	objStorage interface {
		objectCreator
		objectGetter
		objectUpdater
		objectDeleter
	}
	objectCreator interface {
		Insert(obj *models.Object) error
		Create(obj *models.Object) error
	}
	objectUpdater interface {
		Update(obj *models.Object, data map[string]any) error
	}
	objectGetter interface {
		Get(obj *models.Object, id uuid.UUID) error
		GetAll(objs *[]models.Object, limit int, offset int, sort string) error
		Count(total *int64) error
	}
	objectDeleter interface {
		Delete(obj *models.Object) error
	}
)

// Object implement business mock_storages for models.Object
type Object struct {
	log     *slog.Logger
	storage objStorage
}

// NewObject construct instance of Object
func NewObject(log *slog.Logger, storage objStorage) *Object {
	return &Object{
		log:     log,
		storage: storage,
	}
}

// Create instance of models.Object
func (o *Object) Create(name string) (object models.Object, err error) {
	const op = "services.Object.Create"
	log := o.log.With(
		slog.String("op", op),
	)

	log.Info("creating Object")

	object = models.Object{
		Name: name,
	}

	if err := o.storage.Create(&object); err != nil {
		log.Error("failed to create object", sl.Err(err))
		return models.Object{}, err
	}

	log.Info("object successfully created")

	return object, nil
}

// GetByID instance of models.Object
func (o *Object) GetByID(id uuid.UUID) (object models.Object, err error) {
	const op = "services.Object.GetByID"
	log := o.log.With(
		slog.String("op", op),
		slog.String("object id", id.String()),
	)

	log.Info("getting object by id")

	if err := o.storage.Get(&object, id); err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			log.Info("object not found")
			return models.Object{}, apperr.ErrObjectNotFound
		}
		log.Error("failed to find object", sl.Err(err))
		return models.Object{}, err
	}

	log.Info("object successfully found")

	return object, nil
}

// GetList of instance of models.Action with pg
func (o *Object) GetList(limit int, offset int, sort string) (objects []models.Object, err error) {
	const op = "services.Object.GetAll"
	log := o.log.With(
		slog.String("op", op),
		slog.Int("limit", limit),
		slog.Int("offset", offset),
		slog.String("sort", sort),
	)

	log.Info("getting list of objects")

	if err := o.storage.GetAll(&objects, limit, offset, sort); err != nil {
		log.Error("failed to get list objects", sl.Err(err))
		return nil, err
	}

	return objects, nil
}

// UpdateByID instance of models.Object got by GetByID
func (o *Object) UpdateByID(id uuid.UUID, name string) (object models.Object, err error) {
	const op = "services.Object.UpdateByID"
	log := o.log.With(
		slog.String("op", op),
		slog.String("object id", id.String()),
	)

	log.Info("updating object by id")

	object, err = o.GetByID(id)
	if err != nil {
		return models.Object{}, err
	}

	if err := o.storage.Update(&object, map[string]any{"Name": name}); err != nil {
		return models.Object{}, err
	}

	log.Info("object successfully updated")

	return object, nil
}

// DeleteByID instance of models.Object got by GetByID
func (o *Object) DeleteByID(id uuid.UUID) error {
	const op = "services.Object.DeleteByUserID"
	log := o.log.With(
		slog.String("op", op),
		slog.String("object id", id.String()),
	)

	log.Info("deleting object by id")

	object, err := o.GetByID(id)
	if err != nil {
		return err
	}

	if err := o.storage.Delete(&object); err != nil {
		log.Error("failed to deleted object", sl.Err(err))
		return err
	}

	log.Info("object successfully deleted")

	return nil
}

func (o *Object) GetObjectTotal() (int64, error) {
	const op = "services.Object.GetObjectTotal"
	var total int64

	log := o.log.With(slog.String("op", op))
	log.Info("getting object total")

	if err := o.storage.Count(&total); err != nil {
		log.Error("failed to get object total", sl.Err(err))
		return 0, err
	}

	log.Info("object total successfully")
	return total, nil
}
