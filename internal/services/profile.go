//go:generate mockgen -source=profile.go -destination=../mock/storages/profile.go -package=storages

package services

import (
	"errors"
	"github.com/google/uuid"
	"gorm.io/gorm"
	"log/slog"
	apperr "sso/internal/consts/errors"
	"sso/internal/models"
	"sso/internal/pkg/logger/sl"
)

type (
	profileStorage interface {
		profileCreator
		profileGetter
		profileUpdater
		profileDeleter
	}

	profileGetter interface {
		GetByID(profile *models.Profile, id uuid.UUID) error
		GetByUserId(profile *models.Profile, id uuid.UUID) error
		GetList(profile *[]models.Profile, offset int, limit int, sort string) error
	}

	profileCreator interface {
		Create(profile *models.Profile) error
	}

	profileUpdater interface {
		Update(profile *models.Profile, data map[string]any) error
	}

	profileDeleter interface {
		Delete(profile *models.Profile) error
	}

	// Profile implement mock_services business mock_storages
	Profile struct {
		log     *slog.Logger
		storage profileStorage
	}
)

// NewProfile creating mock_services structure
func NewProfile(log *slog.Logger, storage profileStorage) *Profile {
	return &Profile{
		log:     log,
		storage: storage,
	}
}

// Create instance of models.Profile in database
func (p *Profile) Create(firstName string, lastName string, middleName string, gender models.Gender, phone string,
	userID uuid.UUID) (profile models.Profile, err error) {
	const op = "mock_services.Profile.Create"

	log := p.log.With(
		slog.String("op", op),
		slog.String("account id", userID.String()),
	)

	log.Info("creating profile")

	profile = models.Profile{
		FirstName:  firstName,
		LastName:   lastName,
		MiddleName: middleName,
		Gender:     gender,
		Phone:      phone,
		UserID:     userID,
	}

	if err := p.storage.Create(&profile); err != nil {
		log.Error("failed to create profile", sl.Err(err))
		return models.Profile{}, err
	}

	return profile, nil
}

// GetByID instance of models.Profile by id
func (p *Profile) GetByID(id uuid.UUID) (profile models.Profile, err error) {
	const op = "mock_services.Profile.GetByID"

	log := p.log.With(
		slog.String("op", op),
		slog.String("id", id.String()),
	)

	log.Info("getting profile by id")

	if err = p.storage.GetByID(&profile, id); err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			log.Error("profile not found")
			return models.Profile{}, apperr.ErrProfileNotFound
		}
		log.Error("failed to find profile", sl.Err(err))
		return models.Profile{}, err
	}

	return models.Profile{}, err
}

// GetUserProfile instance of models.Profile by models.User id
func (p *Profile) GetUserProfile(user models.User) (profile models.Profile, err error) {
	const op = "mock_services.Profile.GetUserProfile"

	log := p.log.With(
		slog.String("op", op),
		slog.String("account id", user.ID.String()),
	)

	log.Info("getting account profile")

	if err = p.storage.GetByUserId(&profile, user.ID); err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			log.Error("profile not found")
			return models.Profile{}, apperr.ErrProfileNotFound
		}
		log.Error("failed to find profile", sl.Err(err))
		return models.Profile{}, err
	}

	return models.Profile{}, err
}

// GetUser models.User for models.Profile got by GetByID
func (p *Profile) GetUser(id uuid.UUID) (user models.User, err error) {
	// TODO add logger
	// TODO add mock_storages
	return models.User{}, nil
}

// GetList get list of instance with pg
func (p *Profile) GetList(offset int, limit int, sort string) (profiles []models.Profile, err error) {
	const op = "mock_services.Profile.GetAll"

	log := p.log.With(
		slog.String("op", op),
	)

	log.Info("getting profile list")

	if err = p.storage.GetList(&profiles, offset, limit, sort); err != nil {
		log.Error("failed to get profiles", sl.Err(err))
		return nil, err
	}

	return profiles, nil
}

//// Update instance of models.Profile
//func (p *Profile) Update(profile *models.Profile, profileToUpdate models.Profile) error {
//	const op = "mock_services.Profile.Update"
//
//	log := p.log.With(
//		slog.String("op", op),
//		slog.String("profile id", profile.ID.String()),
//	)
//
//	log.Info("updating profile instance")
//
//	result := p.database.Model(&profile).Updates(profileToUpdate)
//
//	if result.Error != nil {
//		log.Error("failed to Update profile", sl.Err(result.Error))
//		return result.Error
//	}
//
//	return nil
//}

// UpdateByID get and Update instance of models.Profile by instance id
func (p *Profile) UpdateByID(id uuid.UUID, firstName string, lastName string, middleName string, gender models.Gender,
	phone string) (profile models.Profile, err error) {
	const op = "mock_services.Profile.UpdateByID"

	log := p.log.With(
		slog.String("op", op),
		slog.String("id", id.String()),
	)

	log.Info("updating profile by id")

	profile, err = p.GetByID(id)

	if err != nil {
		return models.Profile{}, err
	}

	if err = p.storage.Update(&profile, map[string]any{
		"first_name":  firstName,
		"last_name":   lastName,
		"middle_name": middleName,
		"gender":      gender,
		"phone":       phone,
	}); err != nil {
		log.Error("failed to update profile", sl.Err(err))
		return models.Profile{}, err
	}

	return models.Profile{}, err
}

// UpdateUserProfile get and Update instance of models.Profile by models.User id
func (p *Profile) UpdateUserProfile(user models.User, firstName string, lastName string, middleName string,
	gender models.Gender, phone string) (profile models.Profile, err error) {
	const op = "mock_services.Profile.UpdateUserProfile"

	log := p.log.With(
		slog.String("op", op),
		slog.String("account id", user.ID.String()),
	)

	log.Info("updating account profile")

	profile, err = p.GetUserProfile(user)
	if err != nil {
		return models.Profile{}, err
	}

	if err = p.storage.Update(&profile, map[string]any{
		"first_name":  firstName,
		"last_name":   lastName,
		"middle_name": middleName,
		"gender":      gender,
		"phone":       phone,
	}); err != nil {
		log.Error("failed to update profile", sl.Err(err))
		return models.Profile{}, err
	}

	return models.Profile{}, err
}

//// delete instance of models.Profile
//func (p *Profile) delete(profile *models.Profile) error {
//	const op = "mock_services.Profile.delete"
//
//	log := p.log.With(
//		slog.String("op", op),
//		slog.String("profile id", profile.ID.String()),
//	)
//
//	log.Info("deleting profile instance")
//
//	if result := p.database.Delete(&profile); result.Error != nil {
//		log.Error("failed to delete profile", sl.Err(result.Error))
//		return result.Error
//	}
//
//	return nil
//}

// DeleteByID instance of models.Profile by ID
func (p *Profile) DeleteByID(id uuid.UUID) error {
	const op = "mock_services.Profile.DeleteByUserID"
	var profile models.Profile

	log := p.log.With(
		slog.String("op", op),
		slog.String("id", id.String()),
	)

	log.Info("deleting profile by id")

	profile, err := p.GetByID(id)
	if err != nil {
		return err
	}

	if err := p.storage.Delete(&profile); err != nil {
		log.Error("failed to delete profile", sl.Err(err))
		return err
	}

	return nil
}

// DeleteUserProfile instance of models.Profile by models.User id
func (p *Profile) DeleteUserProfile(user models.User) error {
	const op = "mock_services.Profile.DeleteUserProfile"

	log := p.log.With(
		slog.String("op", op),
		slog.String("account id", user.ID.String()),
	)

	log.Info("deleting profile instance")

	profile, err := p.GetUserProfile(user)
	if err != nil {
		return err
	}

	if err := p.storage.Delete(&profile); err != nil {
		log.Error("failed to delete profile", sl.Err(err))
		return err
	}

	return nil
}
