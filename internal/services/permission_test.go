package services

import (
	"github.com/golang/mock/gomock"
	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"gorm.io/gorm"
	apperr "sso/internal/consts/errors"
	"sso/internal/mock"
	"sso/internal/mock/gen"
	mockstorage "sso/internal/mock/storages"
	"testing"
)

type permObj struct {
	name     string
	actionID uuid.UUID
	objectID uuid.UUID
}

func TestPermission_Create(t *testing.T) {
	ctr := gomock.NewController(t)
	defer ctr.Finish()

	tests := []struct {
		name      string
		storage   permissionStorage
		actionSrv *Action
		objectSrv *Object
		data      permObj
		wantErr   bool
		err       error
	}{
		{
			name:    "success",
			storage: mockstorage.SimulatePermission(ctr, nil),
			actionSrv: NewAction(
				mock.Logger,
				mockstorage.SimulateAction(ctr, nil),
			),
			objectSrv: NewObject(
				mock.Logger,
				mockstorage.SimulateObj(ctr, nil),
			),
			data: permObj{
				name:     gen.GenerateString(),
				actionID: uuid.New(),
				objectID: uuid.New(),
			},
			wantErr: false,
		},
		{
			name:    "action not found",
			storage: mockstorage.SimulatePermission(ctr, nil),
			actionSrv: NewAction(
				mock.Logger,
				mockstorage.SimulateAction(ctr, apperr.ErrActionNotFound),
			),
			objectSrv: NewObject(
				mock.Logger,
				mockstorage.SimulateObj(ctr, nil),
			),
			data: permObj{
				name:     gen.GenerateString(),
				actionID: uuid.New(),
				objectID: uuid.New(),
			},
			wantErr: true,
			err:     apperr.ErrActionNotFound,
		},
		{
			name:    "obj not found",
			storage: mockstorage.SimulatePermission(ctr, nil),
			actionSrv: NewAction(
				mock.Logger,
				mockstorage.SimulateAction(ctr, nil),
			),
			objectSrv: NewObject(
				mock.Logger,
				mockstorage.SimulateObj(ctr, apperr.ErrObjectNotFound),
			),
			data: permObj{
				name:     gen.GenerateString(),
				actionID: uuid.New(),
				objectID: uuid.New(),
			},
			wantErr: true,
			err:     apperr.ErrObjectNotFound,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			p := NewPermission(mock.Logger, test.storage, test.actionSrv, test.objectSrv)

			permission, err := p.Create(test.data.name, test.data.actionID, test.data.objectID)

			if test.wantErr {
				assert.NotNil(t, err)
				assert.Equal(t, test.err, err)
				assert.Empty(t, permission)
			} else {
				assert.Nil(t, err)
				assert.NotEmpty(t, permission)
				assert.Equal(t, test.data.name, permission.Name)
			}
		})
	}
}

func TestPermission_GetByID(t *testing.T) {
	ctr := gomock.NewController(t)
	defer ctr.Finish()

	tests := []struct {
		name      string
		storage   permissionStorage
		actionSrv *Action
		objectSrv *Object
		id        uuid.UUID
		wandErr   bool
		err       error
	}{
		{
			name:    "exist permission",
			storage: mockstorage.SimulatePermission(ctr, nil),
			actionSrv: NewAction(
				mock.Logger,
				mockstorage.SimulateAction(ctr, nil),
			),
			objectSrv: NewObject(
				mock.Logger,
				mockstorage.SimulateObj(ctr, nil),
			),
			id:      uuid.New(),
			wandErr: false,
		},
		{
			name:    "not exist permission",
			storage: mockstorage.SimulatePermission(ctr, gorm.ErrRecordNotFound),
			actionSrv: NewAction(
				mock.Logger,
				mockstorage.SimulateAction(ctr, nil),
			),
			objectSrv: NewObject(
				mock.Logger,
				mockstorage.SimulateObj(ctr, nil),
			),
			id:      uuid.New(),
			wandErr: true,
			err:     apperr.ErrPermissionNotFound,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			p := NewPermission(mock.Logger, test.storage, test.actionSrv, test.objectSrv)

			permission, err := p.GetByID(test.id)

			if test.wandErr {
				assert.Error(t, err)
				assert.Empty(t, permission)
				assert.Equal(t, test.err, err)
			} else {
				assert.NoError(t, err)
				assert.NotEmpty(t, permission)
				assert.Equal(t, test.id, permission.ID)
			}
		})
	}
}

func TestPermission_UpdateByID(t *testing.T) {
	ctr := gomock.NewController(t)
	defer ctr.Finish()

	tests := []struct {
		name      string
		storage   permissionStorage
		actionSrv *Action
		objectSrv *Object
		id        uuid.UUID
		data      permObj
		wandErr   bool
		err       error
	}{
		{
			name:    "exist",
			storage: mockstorage.SimulatePermission(ctr, nil),
			actionSrv: NewAction(
				mock.Logger,
				mockstorage.SimulateAction(ctr, nil),
			),
			objectSrv: NewObject(
				mock.Logger,
				mockstorage.SimulateObj(ctr, nil),
			),
			id: uuid.New(),
			data: permObj{
				name:     gen.GenerateString(),
				actionID: uuid.New(),
				objectID: uuid.New(),
			},
			wandErr: false,
		},
		{
			name:    "not exist",
			storage: mockstorage.SimulatePermission(ctr, apperr.ErrPermissionNotFound),
			actionSrv: NewAction(
				mock.Logger,
				mockstorage.SimulateAction(ctr, nil),
			),
			objectSrv: NewObject(
				mock.Logger,
				mockstorage.SimulateObj(ctr, nil),
			),
			id: uuid.New(),
			data: permObj{
				name:     gen.GenerateString(),
				actionID: uuid.New(),
				objectID: uuid.New(),
			},
			wandErr: true,
			err:     apperr.ErrPermissionNotFound,
		},
		{
			name:    "action not exist",
			storage: mockstorage.SimulatePermission(ctr, nil),
			actionSrv: NewAction(
				mock.Logger,
				mockstorage.SimulateAction(ctr, apperr.ErrActionNotFound),
			),
			objectSrv: NewObject(
				mock.Logger,
				mockstorage.SimulateObj(ctr, nil),
			),
			id: uuid.New(),
			data: permObj{
				name:     gen.GenerateString(),
				actionID: uuid.New(),
				objectID: uuid.New(),
			},
			wandErr: true,
			err:     apperr.ErrActionNotFound,
		},
		{
			name:    "object not found",
			storage: mockstorage.SimulatePermission(ctr, nil),
			actionSrv: NewAction(
				mock.Logger,
				mockstorage.SimulateAction(ctr, nil),
			),
			objectSrv: NewObject(
				mock.Logger,
				mockstorage.SimulateObj(ctr, apperr.ErrObjectNotFound),
			),
			id: uuid.New(),
			data: permObj{
				name:     gen.GenerateString(),
				actionID: uuid.New(),
				objectID: uuid.New(),
			},
			wandErr: true,
			err:     apperr.ErrObjectNotFound,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			p := NewPermission(mock.Logger, test.storage, test.actionSrv, test.objectSrv)
			permission, err := p.UpdateByID(test.id, test.data.name, test.data.actionID, test.data.objectID)
			if test.wandErr {
				assert.Error(t, err)
				assert.Empty(t, permission)
				assert.Equal(t, test.err, err)
			} else {
				assert.NoError(t, err)
				assert.NotEmpty(t, permission)
				assert.Equal(t, test.id, permission.ID)
			}
		})
	}
}

func TestPermission_DeleteByID(t *testing.T) {
	ctr := gomock.NewController(t)
	defer ctr.Finish()

	tests := []struct {
		name      string
		storage   permissionStorage
		actionSrv *Action
		objectSrv *Object
		id        uuid.UUID
		wandErr   bool
		err       error
	}{
		{
			name:    "exist",
			storage: mockstorage.SimulatePermission(ctr, nil),
			actionSrv: NewAction(
				mock.Logger,
				mockstorage.SimulateAction(ctr, nil),
			),
			objectSrv: NewObject(
				mock.Logger,
				mockstorage.SimulateObj(ctr, nil),
			),
			id:      uuid.New(),
			wandErr: false,
		},
		{
			name:    "not exist",
			storage: mockstorage.SimulatePermission(ctr, gorm.ErrRecordNotFound),
			actionSrv: NewAction(
				mock.Logger,
				mockstorage.SimulateAction(ctr, nil),
			),
			objectSrv: NewObject(
				mock.Logger,
				mockstorage.SimulateObj(ctr, nil),
			),
			id:      uuid.New(),
			wandErr: true,
			err:     apperr.ErrPermissionNotFound,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			p := NewPermission(mock.Logger, test.storage, test.actionSrv, test.objectSrv)
			err := p.DeleteByID(test.id)
			if test.wandErr {
				assert.Error(t, err)
				assert.Equal(t, test.err, err)
			} else {
				assert.NoError(t, err)
			}
		})
	}
}

func TestPermission_GetList(t *testing.T) {
	ctr := gomock.NewController(t)
	defer ctr.Finish()

	tests := []struct {
		name      string
		storage   permissionStorage
		actionSrv *Action
		objectSrv *Object
		limit     int
		wandErr   bool
		err       error
	}{
		{
			name:    "get one permission",
			storage: mockstorage.SimulatePermission(ctr, nil),
			actionSrv: NewAction(
				mock.Logger,
				mockstorage.SimulateAction(ctr, nil),
			),
			objectSrv: NewObject(
				mock.Logger,
				mockstorage.SimulateObj(ctr, nil),
			),
			limit:   1,
			wandErr: false,
		},
		{
			name:    "get none permission",
			storage: mockstorage.SimulatePermission(ctr, nil),
			actionSrv: NewAction(
				mock.Logger,
				mockstorage.SimulateAction(ctr, nil),
			),
			objectSrv: NewObject(
				mock.Logger,
				mockstorage.SimulateObj(ctr, nil),
			),
			limit:   0,
			wandErr: false,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			p := NewPermission(mock.Logger, test.storage, test.actionSrv, test.objectSrv)
			permissions, err := p.GetList(test.limit, 0, "")

			assert.NoError(t, err)
			assert.Equal(t, test.limit, len(permissions))
		})
	}
}

func TestPermission_Count(t *testing.T) {
	ctr := gomock.NewController(t)
	defer ctr.Finish()

	// TODO create test
}
