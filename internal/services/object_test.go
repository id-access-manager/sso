package services

import (
	"github.com/golang/mock/gomock"
	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"gorm.io/gorm"
	apperr "sso/internal/consts/errors"
	"sso/internal/mock"
	mocklogic "sso/internal/mock/storages"
	"testing"
)

func TestObject_Create(t *testing.T) {
	ctr := gomock.NewController(t)
	defer ctr.Finish()

	tests := []struct {
		name    string
		storage objStorage
		objName string
	}{
		{
			name:    "test with",
			storage: mocklogic.SimulateObj(ctr, nil),
			objName: "test name",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := NewObject(mock.Logger, tt.storage)
			obj, err := s.Create(tt.objName)
			assert.NoError(t, err)
			assert.Equal(t, tt.objName, obj.Name)
			assert.NotEmpty(t, obj.ID)
		})
	}
}

func TestObject_DeleteByID(t *testing.T) {
	ctr := gomock.NewController(t)
	defer ctr.Finish()

	tests := []struct {
		name    string
		storage objStorage
		objID   uuid.UUID
		wantErr bool
		err     error
	}{
		{
			name:    "test with existing object",
			storage: mocklogic.SimulateObj(ctr, nil),
			objID:   uuid.New(),
			wantErr: false,
		},
		{
			name:    "test without existing object",
			storage: mocklogic.SimulateObj(ctr, gorm.ErrRecordNotFound),
			objID:   uuid.Nil,
			wantErr: true,
			err:     apperr.ErrObjectNotFound,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := NewObject(mock.Logger, tt.storage)
			err := s.DeleteByID(tt.objID)

			if tt.wantErr {
				assert.Equal(t, err, tt.err)
			} else {
				assert.NoError(t, err)
			}
		})
	}
}

func TestObject_GetByID(t *testing.T) {
	ctr := gomock.NewController(t)
	defer ctr.Finish()

	tests := []struct {
		name    string
		storage objStorage
		obiID   uuid.UUID
		wantErr bool
		err     error
	}{
		{
			name:    "test with existing object",
			storage: mocklogic.SimulateObj(ctr, nil),
			obiID:   uuid.New(),
			wantErr: false,
		},
		{
			name:    "test without existing object",
			storage: mocklogic.SimulateObj(ctr, gorm.ErrRecordNotFound),
			obiID:   uuid.Nil,
			wantErr: true,
			err:     gorm.ErrRecordNotFound,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := NewObject(mock.Logger, tt.storage)
			obj, err := s.GetByID(tt.obiID)
			if tt.wantErr == true {
				assert.Equal(t, err, apperr.ErrObjectNotFound)
			} else {
				assert.Equal(t, tt.obiID, obj.ID)
			}
		})
	}
}

func TestObject_GetList(t *testing.T) {
	ctr := gomock.NewController(t)
	defer ctr.Finish()

	tests := []struct {
		name    string
		storage objStorage
		limit   int
		wantErr bool
		err     error
	}{
		{
			name:    "exist limit",
			storage: mocklogic.SimulateObj(ctr, nil),
			limit:   10,
			wantErr: false,
		},
		{
			name:    "not exist limit",
			storage: mocklogic.SimulateObj(ctr, nil),
			limit:   0,
			wantErr: false,
		},
		{
			name:    "database err",
			storage: mocklogic.SimulateObj(ctr, gorm.ErrInvalidDB),
			limit:   10,
			wantErr: true,
			err:     gorm.ErrInvalidDB,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := NewObject(mock.Logger, tt.storage)
			objects, err := s.GetList(tt.limit, 0, "")
			if tt.wantErr {
				assert.Equal(t, tt.err, err)
			} else {
				assert.Equal(t, tt.limit, len(objects))
			}
		})
	}
}

func TestObject_UpdateByID(t *testing.T) {
	ctr := gomock.NewController(t)
	defer ctr.Finish()

	tests := []struct {
		name    string
		storage objStorage
		obiID   uuid.UUID
		objName string
		wantErr bool
		err     error
	}{
		{
			name:    "test with existing object",
			storage: mocklogic.SimulateObj(ctr, nil),
			obiID:   uuid.New(),
			objName: "test name",
			wantErr: false,
		},
		{
			name:    "test without existing object",
			storage: mocklogic.SimulateObj(ctr, gorm.ErrRecordNotFound),
			obiID:   uuid.Nil,
			objName: "test name",
			wantErr: true,
			err:     apperr.ErrObjectNotFound,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := NewObject(mock.Logger, tt.storage)
			obj, err := s.UpdateByID(tt.obiID, tt.objName)

			if tt.wantErr && err != nil {
				assert.Equal(t, err, apperr.ErrObjectNotFound)
			} else {
				assert.Equal(t, tt.obiID, obj.ID)
			}
		})
	}
}
