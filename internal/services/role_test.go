package services

import (
	"github.com/golang/mock/gomock"
	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"gorm.io/gorm"
	apperr "sso/internal/consts/errors"
	"sso/internal/mock"
	mstg "sso/internal/mock/storages"
	"testing"
)

type roleObj struct {
	name          string
	permissionIDs []uuid.UUID
}

func TestRole_Create(t *testing.T) {
	ctr := gomock.NewController(t)
	defer ctr.Finish()

	tests := []struct {
		name    string
		storage roleStorage
		permSrv *Permission
		roleObj roleObj
		wantErr bool
		err     error
	}{
		{
			name:    "success",
			storage: mstg.SimulateRole(ctr, nil),
			permSrv: NewPermission(
				mock.Logger,
				mstg.SimulatePermission(ctr, nil),
				NewAction(mock.Logger, mstg.SimulateAction(ctr, nil)),
				NewObject(mock.Logger, mstg.SimulateObj(ctr, nil)),
			),
			roleObj: roleObj{
				name:          "test",
				permissionIDs: []uuid.UUID{uuid.New(), uuid.New(), uuid.New()},
			},
			wantErr: false,
			err:     nil,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			s := NewRole(mock.Logger, test.storage, test.permSrv)

			role, err := s.Create(test.roleObj.name, test.roleObj.permissionIDs)

			if test.wantErr {
				assert.Error(t, err)
				assert.ErrorIs(t, err, test.err)
				assert.Empty(t, role)
			} else {
				assert.NoError(t, err)
				assert.NotEmpty(t, role)
			}
		})
	}
}
func TestRole_GetByID(t *testing.T) {
	ctr := gomock.NewController(t)
	defer ctr.Finish()

	tests := []struct {
		name    string
		storage roleStorage
		id      uuid.UUID
		wantErr bool
		err     error
	}{
		{
			name:    "success",
			storage: mstg.SimulateRole(ctr, nil),
			id:      uuid.New(),
			wantErr: false,
		},
		{
			name:    "not found",
			storage: mstg.SimulateRole(ctr, gorm.ErrRecordNotFound),
			id:      uuid.Nil,
			wantErr: true,
			err:     apperr.ErrRoleNotFound,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			s := NewRole(mock.Logger, test.storage, nil)

			role, err := s.GetByID(test.id)

			if test.wantErr {
				assert.Error(t, err)
				assert.ErrorIs(t, err, test.err)
				assert.Empty(t, role)
			} else {
				assert.NoError(t, err)
				assert.NotEmpty(t, role)
			}
		})
	}
}

func TestRole_GetAll(t *testing.T) {
	ctr := gomock.NewController(t)
	defer ctr.Finish()

	tests := []struct {
		name    string
		storage roleStorage
		limit   int
		offset  int
		sort    string
		wantErr bool
		err     error
	}{
		{
			name:    "success",
			storage: mstg.SimulateRole(ctr, nil),
			limit:   10,
			offset:  0,
			wantErr: false,
		},
		{
			name:    "limit empty",
			storage: mstg.SimulateRole(ctr, nil),
			limit:   0,
			offset:  0,
			wantErr: false,
		},
		{
			name:    "offset empty",
			storage: mstg.SimulateRole(ctr, nil),
			limit:   10,
			offset:  0,
			wantErr: false,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			s := NewRole(mock.Logger, test.storage, nil)

			roles, err := s.GetAll(test.limit, test.offset, test.sort)

			if test.wantErr {
				assert.Error(t, err)
				assert.ErrorIs(t, err, test.err)
				assert.Nil(t, roles)
			} else {
				assert.NoError(t, err)
				assert.Equal(t, test.limit, len(roles))
			}
		})
	}
}

func TestRole_GetByIDs(t *testing.T) {
	ctr := gomock.NewController(t)
	defer ctr.Finish()

	tests := []struct {
		name    string
		storage roleStorage
		ids     []uuid.UUID
		wantErr bool
		err     error
	}{
		{
			name:    "success",
			storage: mstg.SimulateRole(ctr, nil),
			ids:     []uuid.UUID{uuid.New(), uuid.New()},
			wantErr: false,
		},
		{
			name:    "not found",
			storage: mstg.SimulateRole(ctr, gorm.ErrRecordNotFound),
			ids:     []uuid.UUID{},
			wantErr: true,
			err:     apperr.ErrRoleNotFound,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			s := NewRole(mock.Logger, test.storage, nil)

			_, err := s.GetByIDs(test.ids)

			if test.wantErr {
				assert.Error(t, err)
				assert.ErrorIs(t, err, test.err)
			}
		})
	}
}

func TestRole_UpdateByID(t *testing.T) {
	ctr := gomock.NewController(t)
	defer ctr.Finish()

	tests := []struct {
		name    string
		storage roleStorage
		permSrv *Permission
		id      uuid.UUID
		roleObj roleObj
		wantErr bool
		err     error
	}{
		{
			name:    "success",
			storage: mstg.SimulateRole(ctr, nil),
			permSrv: NewPermission(
				mock.Logger,
				mstg.SimulatePermission(ctr, nil),
				NewAction(mock.Logger, mstg.SimulateAction(ctr, nil)),
				NewObject(mock.Logger, mstg.SimulateObj(ctr, nil)),
			),
			id: uuid.New(),
			roleObj: roleObj{
				name:          "test",
				permissionIDs: []uuid.UUID{uuid.New(), uuid.New()},
			},
			wantErr: false,
		},
		{
			name:    "not found",
			storage: mstg.SimulateRole(ctr, gorm.ErrRecordNotFound),
			permSrv: NewPermission(
				mock.Logger,
				mstg.SimulatePermission(ctr, nil),
				NewAction(mock.Logger, mstg.SimulateAction(ctr, nil)),
				NewObject(mock.Logger, mstg.SimulateObj(ctr, nil)),
			),
			id: uuid.New(),
			roleObj: roleObj{
				name:          "test",
				permissionIDs: []uuid.UUID{uuid.New(), uuid.New()},
			},
			wantErr: true,
			err:     apperr.ErrRoleNotFound,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			s := NewRole(mock.Logger, test.storage, test.permSrv)

			role, err := s.UpdateByID(test.id, test.roleObj.name, test.roleObj.permissionIDs)

			if test.wantErr {
				assert.Error(t, err)
				assert.ErrorIs(t, err, test.err)
				assert.Empty(t, role)
			} else {
				assert.NoError(t, err)
				assert.NotEmpty(t, role)
			}
		})
	}
}
func TestRole_DeleteByID(t *testing.T) {
	ctr := gomock.NewController(t)
	defer ctr.Finish()

	tests := []struct {
		name    string
		storage roleStorage
		id      uuid.UUID
		wantErr bool
		err     error
	}{
		{
			name:    "success",
			storage: mstg.SimulateRole(ctr, nil),
			id:      uuid.New(),
			wantErr: false,
		},
		{
			name:    "not found",
			storage: mstg.SimulateRole(ctr, gorm.ErrRecordNotFound),
			id:      uuid.Nil,
			wantErr: true,
			err:     apperr.ErrRoleNotFound,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			s := NewRole(mock.Logger, test.storage, nil)
			err := s.DeleteByID(test.id)
			if test.wantErr {
				assert.Error(t, err)
				assert.ErrorIs(t, err, test.err)
			} else {
				assert.NoError(t, err)
			}
		})
	}
}
