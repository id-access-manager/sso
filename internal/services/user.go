//go:generate mockgen -source=user.go -destination=../mock/storages/user.go -package=storages

package services

import (
	"errors"
	"github.com/google/uuid"
	"gorm.io/gorm"
	"log/slog"
	apperr "sso/internal/consts/errors"
	"sso/internal/models"
	"sso/internal/pkg/logger/sl"
	"strconv"
)

type (
	userStorage interface {
		userCreator
		userGetter
		userUpdater
		userDeleter
	}

	userCreator interface {
		Save(user *models.User) error
		Create(user *models.User) error
	}

	userGetter interface {
		GetByID(user *models.User, id uuid.UUID) error
		GetByEmail(user *models.User, email string) error
		IsExist(id uuid.UUID) (isExist bool, err error)
		GetByUsername(user *models.User, username string) error
		GetAll(users *[]models.User, limit int, offset int, sort string) error
		Count(count *int64) error
	}

	userUpdater interface {
		Update(user *models.User, data map[string]any) error
	}

	userDeleter interface {
		Delete(user *models.User) error
	}
)

type resourceService interface{}

// User implement business mock_storages for models.User
type User struct {
	log      *slog.Logger
	storage  userStorage
	resource resourceService
	role     *Role
}

// NewUser construct instance of User
func NewUser(log *slog.Logger, storage userStorage) *User {
	return &User{
		log:     log,
		storage: storage,
	}
}

// Create instance of models.User
func (u *User) Create(email models.Email, username string, password string) (user models.User, err error) {
	const op = "mock_services.User.Create"

	log := u.log.With(slog.String("operation", op))
	log.Info("creating account")

	user = models.User{
		Email:         email,
		Username:      username,
		IsTmpPassword: true,
	}

	if err := user.SetPassword(password); err != nil {
		log.Error("failed to set password", sl.Err(err))
		return models.User{}, err
	}

	if err := u.storage.Create(&user); err != nil {
		log.Error("failed to create account", sl.Err(err))
		return models.User{}, err
	}

	log.Info("account successfully created")

	return user, nil
}

// Register creating models.User instance by Create
func (u *User) Register(email models.Email, username string, password string) (user models.User, err error) {
	const op = "mock_services.User.init"

	log := u.log.With(
		slog.String("operation", op),
		slog.String("email", email.String()),
	)
	log.Info("registering account")

	user, err = u.Create(email, username, password)
	if err != nil {
		log.Error("failed to register account", sl.Err(err))
		return models.User{}, err
	}

	user.IsTmpPassword = false
	if err := u.storage.Save(&user); err != nil {
		log.Error("failed to save account", sl.Err(err))
		return models.User{}, err
	}

	log.Info("account successfully registered")

	return user, nil
}

// ChangePassword check old models.User password and set new password
func (u *User) ChangePassword(user *models.User, oldPassword string, newPassword string) error {
	const op = "mock_services.User.ChangePassword"

	log := u.log.With(
		slog.String("operation", op),
		slog.String("user id", user.ID.String()),
	)
	log.Info("changing password")

	if err := user.CheckPassword(oldPassword); err != nil {
		log.Error("failed to change password", sl.Err(err))
		return err
	}

	if err := user.SetPassword(newPassword); err != nil {
		log.Error("failed to change password", sl.Err(err))
		return err
	}

	if err := u.storage.Save(user); err != nil {
		log.Error("failed to save user account", sl.Err(err))
		return err
	}

	log.Debug("account successfully changed")

	return nil
}

// IsExist checking exiting models.User in database
func (u *User) IsExist(id uuid.UUID) bool {
	const op = "mock_services.User.IsExist"
	var (
		err     error
		isExist bool
	)

	log := u.log.With(slog.String("op", op))
	log.Info("checking if user exists")

	isExist, err = u.storage.IsExist(id)
	if err != nil {
		log.Error("failed to check if user exists", sl.Err(err))
		return false
	}

	log.Debug("successfully checking if user exists")

	return isExist
}

// GetByEmail models.User instance by models.Email
func (u *User) GetByEmail(email models.Email) (user models.User, err error) {
	const op = "mock_services.User.GetByEmail"

	log := u.log.With(
		slog.String("operation", op),
		slog.String("email", email.String()),
	)
	log.Info("getting account by email")

	if err = u.storage.GetByEmail(&user, email.String()); err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			log.Info("account not found")
			return models.User{}, apperr.ErrUserNotFound
		}
		log.Error("failed to get account by email", sl.Err(err))
		return models.User{}, err
	}

	log.Debug("account successfully found")

	return user, nil
}

// GetAll list of models.User
func (u *User) GetAll(limit int, offset int, sort string) ([]models.User, error) {
	const op = "mock_services.User.GetAll"
	var users []models.User
	log := u.log.With(
		slog.String("op", op),
		slog.String("limit", strconv.Itoa(limit)),
		slog.String("offset", strconv.Itoa(offset)),
	)
	log.Info("getting all users")

	if err := u.storage.GetAll(&users, limit, offset, sort); err != nil {
		log.Error("failed to get all users", sl.Err(err))
		return nil, err
	}

	log.Info("got all users")

	return users, nil
}

// GetById models.User instance by id
func (u *User) GetById(id uuid.UUID) (user models.User, err error) {
	const op = "mock_services.User.GetByUD"

	log := u.log.With(
		slog.String("operation", op),
		slog.String("account id", id.String()),
	)
	log.Info("getting account by id")

	if err = u.storage.GetByID(&user, id); err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			log.Info("account not found")
			return models.User{}, apperr.ErrUserNotFound
		}
		log.Error("failed to get account by id", sl.Err(err))
		return models.User{}, err
	}

	log.Info("account successfully found")

	return user, nil
}

func (u *User) GetByUsername(username string) (user models.User, err error) {
	const op = "mock_services.User.GetByUsername"
	log := u.log.With(
		slog.String("operation", op),
		slog.String("username", username),
	)

	log.Info("getting account by username")

	if err = u.storage.GetByUsername(&user, username); err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			log.Info("account not found")
			return models.User{}, apperr.ErrUserNotFound
		}
		log.Error("failed to get account by username", sl.Err(err))
		return models.User{}, err
	}

	log.Info("account successfully found")

	return user, nil
}

func (u *User) Count() (int64, error) {
	const op = "mock_services.User.Count"
	var count int64
	log := u.log.With(slog.String("operation", op))
	log.Info("getting account count")

	if err := u.storage.Count(&count); err != nil {
		log.Error("failed to get account count", sl.Err(err))
		return 0, err
	}
	log.Info("got account count")
	return count, nil
}

//// GetProfile models.Profile for models.User
//func (u *User) GetProfile(user *models.User) (models.Profile, error) {
//	const op = "mock_services.User.GetProfile"
//	var profile models.Profile
//
//	log := u.log.With(
//		slog.String("operation", op),
//		slog.String("user id", user.ID.String()),
//	)
//	log.Info("getting user profile")
//
//	result := u.database.First(&profile, "user_id = ?", user.ID.String())
//	if result.Error != nil {
//
//		if errors.Is(result.Error, gorm.ErrRecordNotFound) {
//			log.Error("profile not found")
//			return models.Profile{}, apperr.ErrProfileNotFound
//		}
//
//		log.Error("failed to get user profile", sl.Err(result.Error))
//		return models.Profile{}, result.Error
//	}
//
//	log.Debug("successfully got profile")
//
//	return profile, nil
//}
//
//// GetResources list of models.Resource for models.User
//func (u *User) GetResources(user *models.User) ([]models.Resource, error) {
//	const op = "mock_services.User.GetResources"
//	var resources []models.Resource
//
//	log := u.log.With(
//		slog.String("operation", op),
//		slog.String("user id", user.ID.String()),
//	)
//	log.Info("getting user resources")
//
//	result := u.database.Model(&models.Resource{}).Where("user_id = ?", user.ID.String()).Find(&resources)
//	if result.Error != nil {
//		log.Error("failed to get user resources", sl.Err(result.Error))
//		return nil, result.Error
//	}
//
//	log.Debug("successfully got user resources")
//
//	return resources, nil
//}
//
//// GetRoles list of models.Role for models.User
//func (u *User) GetRoles(user *models.User) ([]models.Role, error) {
//	const op = "mock_services.User.GetRoles"
//	var roles []models.Role
//
//	log := u.log.With(
//		slog.String("operation", op),
//		slog.String("user id", user.ID.String()),
//	)
//	log.Info("getting user roles")
//
//	result := u.database.Model(&models.Role{}).Where("user_id = ?", user.ID.String()).Find(&roles)
//	if result.Error != nil {
//		log.Error("failed to get user roles", sl.Err(result.Error))
//		return nil, result.Error
//	}
//
//	return roles, nil
//}

//// update models.User instance
//func (u *User) update(user *models.User, username string) error {
//	const op = "mock_services.User.update"
//
//	log := u.log.With(
//		slog.String("operation", op),
//		slog.String("account id", user.ID.String()),
//	)
//	log.Info("updating account")
//
//	userToUpdate := models.User{Username: username}
//
//	if err := u.database.Model(&user).Updates(userToUpdate).Error; err != nil {
//		log.Error("failed to update account", sl.Err(err))
//		return err
//	}
//
//	return nil
//}

// UpdateByID TODO add description
func (u *User) UpdateByID() {
	const op = "mock_services.User."

	log := u.log.With(slog.String("operation", op))
	log.Info("")

	// TODO add mock_storages
}

//// delete models.User instance
//func (u *User) delete(user *models.User) error {
//	const op = "mock_services.User.delete"
//
//	log := u.log.With(
//		slog.String("operation", op),
//		slog.String("user id", user.ID.String()),
//	)
//	log.Info("deleting user")
//
//	if err := u.database.Delete(&user).Error; err != nil {
//		log.Error("failed to delete user", sl.Err(err))
//		return err
//	}
//
//	return nil
//}

// DeleteByID models.User instance got by GetById
func (u *User) DeleteByID(id uuid.UUID) error {
	const op = "mock_services.User.DeleteByUserID"

	log := u.log.With(
		slog.String("op", op),
		slog.String("user id", id.String()),
	)
	log.Info("deleting user by id")

	user, err := u.GetById(id)
	if err != nil {
		log.Error("failed to delete user", sl.Err(err))
		return err
	}

	if err := u.storage.Delete(&user); err != nil {
		log.Error("failed to delete user", sl.Err(err))
		return err
	}

	return nil
}
