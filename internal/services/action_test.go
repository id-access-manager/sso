package services

import (
	"github.com/golang/mock/gomock"
	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"gorm.io/gorm"
	apperr "sso/internal/consts/errors"
	"sso/internal/mock"
	mocklogic "sso/internal/mock/storages"
	"sso/internal/models"
	"testing"
)

func TestAction_Create(t *testing.T) {
	ctr := gomock.NewController(t)
	defer ctr.Finish()

	tests := []struct {
		name       string
		storage    actionStorage
		actionName string
		typeName   models.ActionType
	}{
		{
			name:       "create action",
			storage:    mocklogic.SimulateAction(ctr, nil),
			actionName: "test create",
			typeName:   models.CreateAction,
		},
		{
			name:       "create action 2",
			storage:    mocklogic.SimulateAction(ctr, nil),
			actionName: "test read",
			typeName:   models.ReadAction,
		},
		{
			name:       "create action 3",
			storage:    mocklogic.SimulateAction(ctr, nil),
			actionName: "test update",
			typeName:   models.UpdateAction,
		},
		{
			name:       "create action 4",
			storage:    mocklogic.SimulateAction(ctr, nil),
			actionName: "test delete",
			typeName:   models.DeleteAction,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := NewAction(mock.Logger, tt.storage)
			obj, err := s.Create(tt.name, tt.typeName)
			assert.NoError(t, err)
			assert.Equal(t, tt.name, obj.Name)
			assert.Equal(t, tt.typeName, obj.ActionType)
			assert.NotEmpty(t, obj.ID)
		})
	}
}

func TestAction_GetByID(t *testing.T) {
	ctr := gomock.NewController(t)
	defer ctr.Finish()

	tests := []struct {
		name    string
		storage actionStorage
		id      uuid.UUID
		wantErr bool
		err     error
	}{
		{
			name:    "get action by id",
			storage: mocklogic.SimulateAction(ctr, nil),
			id:      uuid.New(),
			wantErr: false,
		},
		{
			name:    "get action by id",
			storage: mocklogic.SimulateAction(ctr, gorm.ErrRecordNotFound),
			id:      uuid.Nil,
			wantErr: true,
			err:     apperr.ErrActionNotFound,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := NewAction(mock.Logger, tt.storage)
			got, err := s.GetByID(tt.id)
			if tt.wantErr {
				assert.Equal(t, err, tt.err)
			} else {
				assert.NoError(t, err)
				assert.Equal(t, tt.id, got.ID)
			}
		})
	}
}

func TestAction_UpdateByID(t *testing.T) {
	ctr := gomock.NewController(t)
	defer ctr.Finish()

	tests := []struct {
		name       string
		storage    actionStorage
		id         uuid.UUID
		actionName string
		actionType models.ActionType
		wantErr    bool
		err        error
	}{
		{
			name:       "test with exists action",
			storage:    mocklogic.SimulateAction(ctr, nil),
			id:         uuid.New(),
			actionName: "test update",
			actionType: models.UpdateAction,
			wantErr:    false,
		},
		{
			name:       "test with exists action 2",
			storage:    mocklogic.SimulateAction(ctr, nil),
			id:         uuid.New(),
			actionName: "test read",
			actionType: models.ReadAction,
			wantErr:    false,
		},
		{
			name:       "test with exists action 3",
			storage:    mocklogic.SimulateAction(ctr, nil),
			id:         uuid.New(),
			actionName: "test delete",
			actionType: models.DeleteAction,
			wantErr:    false,
		},
		{
			name:       "test without exists action",
			storage:    mocklogic.SimulateAction(ctr, gorm.ErrRecordNotFound),
			id:         uuid.Nil,
			actionName: "test not found",
			wantErr:    true,
			err:        apperr.ErrActionNotFound,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := NewAction(mock.Logger, tt.storage)
			obj, err := s.UpdateByID(tt.id, tt.actionName, tt.actionType)
			if tt.wantErr {
				assert.Equal(t, err, tt.err)
			} else {
				assert.NoError(t, err)
				assert.Equal(t, tt.actionName, obj.Name)
				assert.Equal(t, tt.actionType, obj.ActionType)
				assert.NotEmpty(t, obj.ID)
			}
		})
	}
}

func TestAction_DeleteByID(t *testing.T) {
	ctr := gomock.NewController(t)
	defer ctr.Finish()

	tests := []struct {
		name    string
		storage actionStorage
		id      uuid.UUID
		wantErr bool
		err     error
	}{
		{
			name:    "delete exists action",
			storage: mocklogic.SimulateAction(ctr, nil),
			id:      uuid.New(),
			wantErr: false,
		},
		{
			name:    "delete action by id",
			storage: mocklogic.SimulateAction(ctr, gorm.ErrRecordNotFound),
			id:      uuid.Nil,
			wantErr: true,
			err:     apperr.ErrActionNotFound,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := NewAction(mock.Logger, tt.storage)
			err := s.DeleteByID(tt.id)
			if tt.wantErr {
				assert.Equal(t, err, tt.err)
			} else {
				assert.NoError(t, err)
			}
		})
	}
}

func TestAction_GetList(t *testing.T) {
	ctr := gomock.NewController(t)
	defer ctr.Finish()

	tests := []struct {
		name    string
		storage actionStorage
		limit   int
		wantErr bool
		err     error
	}{
		{
			name:    "exist limit",
			storage: mocklogic.SimulateAction(ctr, nil),
			limit:   10,
			wantErr: false,
		},
		{
			name:    "not exist limit",
			storage: mocklogic.SimulateAction(ctr, nil),
			limit:   0,
			wantErr: false,
		},
		{
			name:    "database err",
			storage: mocklogic.SimulateAction(ctr, gorm.ErrInvalidDB),
			limit:   10,
			wantErr: true,
			err:     gorm.ErrInvalidDB,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := NewAction(mock.Logger, tt.storage)
			actions, err := s.GetList(tt.limit, 0, "")
			if tt.wantErr {
				assert.Equal(t, tt.err, err)
			} else {
				assert.Equal(t, tt.limit, len(actions))
			}
		})
	}
}
