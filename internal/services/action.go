//go:generate mockgen -source=action.go -destination=../mock/storages/action.go -package=storages

package services

import (
	"errors"
	"github.com/google/uuid"
	"gorm.io/gorm"
	"log/slog"
	apperr "sso/internal/consts/errors"
	"sso/internal/models"
	"sso/internal/pkg/logger/sl"
)

type actionStorage interface {
	actionCreator
	actionGetter
	actionUpdater
	actionDeleter
}
type (
	actionCreator interface {
		Create(action *models.Action) error
	}
	actionGetter interface {
		GetByID(action *models.Action, id uuid.UUID) error
		GetAll(actions *[]models.Action, limit int, offset int, sort string) error
		Count(total *int64) error
	}
	actionUpdater interface {
		Update(action *models.Action, data map[string]any) error
	}
	actionDeleter interface {
		Delete(action *models.Action) error
	}
)

// Action implement business mock_storages for models.Action
type Action struct {
	log     *slog.Logger
	storage actionStorage
}

// NewAction construct instance of Action
func NewAction(log *slog.Logger, storage actionStorage) *Action {
	return &Action{
		log:     log,
		storage: storage,
	}
}

// Create instance of models.Action
func (a *Action) Create(name string, actionName models.ActionType) (action models.Action, err error) {
	const op = "services.Action.Create"
	log := a.log.With(
		slog.String("op", op),
	)

	log.Info("creating action")

	action = models.Action{
		Name:       name,
		ActionType: actionName,
	}

	if err = a.storage.Create(&action); err != nil {
		log.Error("failed to create action", sl.Err(err))
		return models.Action{}, err
	}

	log.Info("action created")

	return action, nil
}

// GetByID instance of models.Action
func (a *Action) GetByID(id uuid.UUID) (action models.Action, err error) {
	const op = "services.Action.GetByID"
	log := a.log.With(
		slog.String("op", op),
		slog.String("action id", id.String()),
	)

	log.Info("getting action by id")

	if err = a.storage.GetByID(&action, id); err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			log.Info("action not found")
			return models.Action{}, apperr.ErrActionNotFound
		}
		log.Error("failed to find action", sl.Err(err))
		return models.Action{}, err
	}

	return action, nil
}

// GetList of instance of models.Action with pg
func (a *Action) GetList(limit int, offset int, sort string) (actions []models.Action, err error) {
	const op = "services.Action.GetAll"
	log := a.log.With(
		slog.String("op", op),
		slog.Int("limit", limit),
		slog.Int("offset", offset),
		slog.String("sort", sort),
	)

	log.Info("getting list of actionsStorage")

	if err = a.storage.GetAll(&actions, limit, offset, sort); err != nil {
		log.Error("failed to get list of actionsStorage", sl.Err(err))
		return nil, err
	}

	return actions, nil
}

func (a *Action) GetActionsTotal() (int64, error) {
	const op = "services.Action.GetTotal"
	var total int64
	log := a.log.With(
		slog.String("op", op))
	log.Info("getting total of actions")

	if err := a.storage.Count(&total); err != nil {
		log.Error("failed to get total of actions", sl.Err(err))
		return 0, err
	}

	log.Info("total of actions found")
	return total, nil
}

// UpdateByID instance of models.Action got by GetByID
func (a *Action) UpdateByID(id uuid.UUID, name string, actionName models.ActionType) (action models.Action, err error) {
	const op = "services.Action.UpdateByID"
	log := a.log.With(
		slog.String("op", op),
		slog.String("action id", id.String()),
	)

	log.Info("updating action by id")

	action, err = a.GetByID(id)
	if err != nil {
		return models.Action{}, err
	}

	if err = a.storage.Update(&action, map[string]any{"Name": name, "ActionType": actionName}); err != nil {
		log.Error("failed to update action", sl.Err(err))
		return models.Action{}, err
	}

	log.Info("action successfully updated")

	return action, nil
}

// DeleteByID instance of models.Action got by GetByID
func (a *Action) DeleteByID(id uuid.UUID) error {
	const op = "services.Action.DeleteByUserID"
	log := a.log.With(
		slog.String("op", op),
		slog.String("action id", id.String()),
	)

	log.Info("deleting action by id")

	action, err := a.GetByID(id)
	if err != nil {
		return err
	}

	if err := a.storage.Delete(&action); err != nil {
		log.Error("failed to deleted action", sl.Err(err))
		return err
	}

	log.Info("action successfully deleted")

	return nil
}
