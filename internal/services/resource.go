//go:generate mockgen -source=resource.go -destination=../mock/storages/resource.go -package=storages

package services

import (
	"errors"
	"github.com/google/uuid"
	"gorm.io/gorm"
	"log/slog"
	apperr "sso/internal/consts/errors"
	"sso/internal/models"
	"sso/internal/pkg/logger/sl"
)

type (
	resourceStorage interface {
		resourceCreator
		resourceUpdater
		resourceDeleter
		resourceGetter
	}

	resourceCreator interface {
		Create(resource *models.Resource) error
		AddRoles(resource *models.Resource, roles *[]models.Role) error
	}

	resourceGetter interface {
		GetByID(resource *models.Resource, id uuid.UUID) error
		GetAll(resources *[]models.Resource, limit int, offset int, sort string) error
		GetByIDs(resources *[]models.Resource, ids []uuid.UUID) error
		GetCount(count *int64) error
	}

	resourceUpdater interface {
		Update(resource *models.Resource, data map[string]any) error
		ReplaceRoles(resource *models.Resource, roles *[]models.Role) error
	}

	resourceDeleter interface {
		Delete(resource *models.Resource) error
	}
)

// Resource implement services business mock_storages
type Resource struct {
	log     *slog.Logger
	storage resourceStorage
	role    *Role
}

// NewResource creating services structure
func NewResource(log *slog.Logger, storage resourceStorage, role *Role) *Resource {
	return &Resource{
		log:     log,
		storage: storage,
		role:    role,
	}
}

// Create instance of models.Resource
func (r *Resource) Create(name string, description string, rolesIDs []uuid.UUID) (resource models.Resource, err error) {
	const op = "services.Resource.Create"
	log := r.log.With(
		slog.String("op", op),
	)

	log.Info("creating resource")

	resource = models.Resource{
		Name:        name,
		Description: description,
	}

	if err = r.storage.Create(&resource); err != nil {
		log.Error("failed to create resource", sl.Err(err))
		return models.Resource{}, err
	}

	roles, err := r.role.GetByIDs(rolesIDs)
	if err != nil {
		log.Error("failed to get roles", sl.Err(err))
		return models.Resource{}, err
	}

	if err := r.storage.AddRoles(&resource, &roles); err != nil {
		log.Error("failed to add roles", sl.Err(err))
		return models.Resource{}, err
	}

	return resource, nil
}

// GetByID get models.Resource instance by id
func (r *Resource) GetByID(id uuid.UUID) (resource models.Resource, err error) {
	const op = "services.Resource.GetByID"
	log := r.log.With(
		slog.String("op", op),
		slog.String("resource id", id.String()),
	)

	log.Info("getting resource by id")

	if err = r.storage.GetByID(&resource, id); err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			log.Error("not found resource by id", sl.Err(err))
			return models.Resource{}, apperr.ErrResourceNotFound
		}
		log.Error("failed to find resource by id", sl.Err(err))
		return models.Resource{}, err
	}

	return resource, nil
}

// GetList get list of models.Resource instances with pg
func (r *Resource) GetList(limit int, offset int, sort string) (resources []models.Resource, err error) {
	const op = "services.Resource.GetAll"
	log := r.log.With(
		slog.String("op", op),
		slog.Int("limit", limit),
		slog.Int("offset", offset),
	)

	log.Info("getting list of resources")

	if err = r.storage.GetAll(&resources, limit, offset, sort); err != nil {
		log.Error("failed to get list of resources", sl.Err(err))
		return nil, err
	}

	return resources, nil
}

func (r *Resource) Count() (int64, error) {
	const op = "services.Resource.Count"
	var count int64

	log := r.log.With(slog.String("op", op))
	log.Info("getting count of resources")

	if err := r.storage.GetCount(&count); err != nil {
		log.Error("failed to get count of resources", sl.Err(err))
		return 0, err
	}

	log.Debug("successfully got count of resources")

	return count, nil
}

// GetByIDs get list of models.Resource by list of uuid.UUID
func (r *Resource) GetByIDs(idList []uuid.UUID) (resources []models.Resource, err error) {
	const op = "services.Resource.GetIDList"
	log := r.log.With(
		slog.String("op", op),
	)

	log.Info("getting list of resources by list of id")

	if err = r.storage.GetByIDs(&resources, idList); err != nil {
		log.Error("failed to get list of resource by ids", sl.Err(err))
		return nil, err
	}

	return resources, err
}

// UpdateByID models.Resource instance got by GetByID
func (r *Resource) UpdateByID(id uuid.UUID, name string, description string, rolesIDs []uuid.UUID) (
	resource models.Resource, err error) {
	const op = "services.Resource.GetUserResource"
	log := r.log.With(
		slog.String("op", op),
		slog.String("id", id.String()),
	)

	log.Info("updating resource by id")

	resource, err = r.GetByID(id)
	if err != nil {
		return models.Resource{}, err
	}

	if err = r.storage.Update(&resource, map[string]any{
		"name":        name,
		"description": description,
	}); err != nil {
		log.Error("failed to update resource", sl.Err(err))
		return models.Resource{}, err
	}

	roles, err := r.role.GetByIDs(rolesIDs)
	if err != nil {
		log.Error("failed to get roles", sl.Err(err))
		return models.Resource{}, err
	}

	if err := r.storage.ReplaceRoles(&resource, &roles); err != nil {
		log.Error("failed to update roles", sl.Err(err))
		return models.Resource{}, err
	}

	return resource, nil
}

// DeleteByID models.Resource instance got by GetByID
func (r *Resource) DeleteByID(id uuid.UUID) (err error) {
	const op = "services.Resource.GetUserResource"
	log := r.log.With(
		slog.String("op", op),
		slog.String("id", id.String()),
	)

	log.Info("deleting resource by id")

	resource, err := r.GetByID(id)
	if err != nil {
		return err
	}

	if err = r.storage.Delete(&resource); err != nil {
		log.Error("failed to delete resource", sl.Err(err))
		return err
	}

	return nil
}
