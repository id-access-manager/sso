package auth

import (
	"github.com/google/uuid"
	"log/slog"
	"sso/internal/dto"
	"sso/internal/models"
	"sso/internal/models/token"
)

// accessTokenManager describes the mock_storages of working with access token
type accessTokenManager interface {
	Create(user models.User, session models.Session) (accessToken token.Access, err error)
	GetByUserID(id uuid.UUID) (tokenRow token.Access, err error)
	Refresh(tokenRow *token.Access) error
	DeleteByUserID(id uuid.UUID) error
}

// refreshTokenManager describes the mock_storages of working with refresh token
type refreshTokenManager interface {
	Create(user models.User, session models.Session) (refreshToken token.Refresh, err error)
	GetByUserID(id uuid.UUID) (tokenRow token.Refresh, err error)
	Refresh(tokenRow *token.Refresh) error
	DeleteByUserID(id uuid.UUID) error
}

// identityTokenManager describes the mock_storages of working with identity token
type identityTokenManager interface {
	Create(user models.User, session models.Session) (identityToken token.Identity, err error)
	GetByUserID(id uuid.UUID) (tokenRow token.Identity, err error)
	Refresh(tokenRow *token.Identity) error
	DeleteByUserID(id uuid.UUID) error
}

// Tokens describes scheme of auth token response
type Tokens struct {
	log   *slog.Logger
	token struct {
		access  accessTokenManager
		refresh refreshTokenManager
		id      identityTokenManager
	}
}

// NewTokens contract an instance of Tokens
func NewTokens(
	log *slog.Logger, access accessTokenManager, refresh refreshTokenManager, identity identityTokenManager,
) *Tokens {
	return &Tokens{
		log: log,
		token: struct {
			access  accessTokenManager
			refresh refreshTokenManager
			id      identityTokenManager
		}{access: access, refresh: refresh, id: identity},
	}
}

// refreshTokens all Tokens for models.User
func (t *Tokens) refreshTokens(userId uuid.UUID) (dto.Tokens, error) {
	const op = "mock_services.UserID.RefreshTokens"
	log := t.log.With(
		slog.String("op", op),
		slog.String("userId", userId.String()),
	)
	log.Info("refreshing tokens")

	accessTokenRow, err := t.token.access.GetByUserID(userId)
	if err != nil {
		return dto.Tokens{}, err
	}

	if err := t.token.access.Refresh(&accessTokenRow); err != nil {
		return dto.Tokens{}, err
	}

	refreshTokenRow, err := t.token.refresh.GetByUserID(userId)
	if err != nil {
		return dto.Tokens{}, err
	}

	if err := t.token.refresh.Refresh(&refreshTokenRow); err != nil {
		return dto.Tokens{}, err
	}

	identityTokenRow, err := t.token.id.GetByUserID(userId)
	if err != nil {
		return dto.Tokens{}, err
	}

	if err := t.token.id.Refresh(&identityTokenRow); err != nil {
		return dto.Tokens{}, err
	}

	log.Debug("successfully refreshed tokens")

	return dto.Tokens{
		Access:   accessTokenRow.Token,
		Refresh:  refreshTokenRow.Token,
		Identity: identityTokenRow.Token,
	}, nil
}

// createTokens creating tokens for models.User
func (t *Tokens) createTokens(user models.User, session models.Session) (dto.Tokens, error) {
	const op = "mock_services.Auth.createTokens"
	var (
		accessTokenRow   token.Access
		refreshTokenRow  token.Refresh
		identityTokenRow token.Identity
		err              error
	)

	log := t.log.With(
		slog.String("operation", op),
		slog.String("user_id", user.ID.String()),
		slog.String("session_id", session.ID.String()),
	)
	log.Info("creating tokens")

	accessTokenRow, err = t.token.access.Create(user, session)
	if err != nil {
		return dto.Tokens{}, err
	}

	refreshTokenRow, err = t.token.refresh.Create(user, session)
	if err != nil {
		return dto.Tokens{}, err
	}

	identityTokenRow, err = t.token.id.Create(user, session)
	if err != nil {
		return dto.Tokens{}, err
	}

	return dto.Tokens{
		Access:   accessTokenRow.Token,
		Refresh:  refreshTokenRow.Token,
		Identity: identityTokenRow.Token,
	}, nil
}
