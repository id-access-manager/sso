package auth

import (
	"github.com/google/uuid"
	"log/slog"
	apperr "sso/internal/consts/errors"
	"sso/internal/dto"
	"sso/internal/models"
	"sso/internal/pkg/jwt"
	"sso/internal/pkg/logger/sl"
	"time"
)

type (
	// userManager describes the mock_storages of working with User
	userManager interface {
		userGetter
	}

	userGetter interface {
		GetByEmail(email models.Email) (user models.User, err error)
		IsExist(id uuid.UUID) (exists bool)
		GetByUsername(username string) (user models.User, err error)
	}
)

type (
	// SessionManager describes the mock_storages of working with Session
	SessionManager interface {
		sessionCreator
		sessionGetter
		sessionUpdater
		sessionDeleter
	}

	sessionCreator interface {
		Create(user models.User) (session models.Session, err error)
	}

	sessionGetter interface {
		GetByID(id uuid.UUID) (session models.Session, err error)
	}

	sessionUpdater interface {
		Refresh(session *models.Session, expiredAt time.Time) error
	}

	sessionDeleter interface {
		DeleteByID(id uuid.UUID) error
	}
)

type (
	// tokenManager describes the mock_storages of working with tokens
	tokenManager interface {
		tokenCreator
		tokenRefresher
	}

	tokenCreator interface {
		refreshTokens(userId uuid.UUID) (dto.Tokens, error)
	}

	tokenRefresher interface {
		createTokens(user models.User, session models.Session) (dto.Tokens, error)
	}
)

type (
	// Auth implement auth mock_storages in application
	Auth struct {
		log     *slog.Logger
		session SessionManager
		user    userManager
		tokens  tokenManager
	}
)

func New(log *slog.Logger, session SessionManager, user userManager, tokens tokenManager) *Auth {
	return &Auth{
		log:     log,
		session: session,
		user:    user,
		tokens:  tokens,
	}
}

// Login models.User and generate access, refresh and identity token
func (a *Auth) Login(username string, password string) (dto.Tokens, error) {
	const op = "services.auth.Login"
	var user models.User

	log := a.log.With(
		slog.String("operation", op),
		slog.String("username", username),
	)
	log.Info("logging in account")

	user, err := a.user.GetByUsername(username)
	if err != nil {
		log.Error("logging in account failed", sl.Err(err))
		return dto.Tokens{}, apperr.ErrInvalidUsername
	}

	if err := user.CheckPassword(password); err != nil {
		log.Error("logging in account failed", sl.Err(err))
		return dto.Tokens{}, apperr.ErrInvalidPassword
	}

	session, err := a.session.Create(user)
	if err != nil {
		log.Error("failed to create session", sl.Err(err))
		return dto.Tokens{}, err
	}

	tokens, err := a.tokens.createTokens(user, session)
	if err != nil {
		log.Error("creating tokens failed", sl.Err(err))
		return dto.Tokens{}, err
	}

	log.Info("successfully logged in account")

	return tokens, nil
}

// Refresh all tokens for models.User.
// If token is invalid returns err of validate token
func (a *Auth) Refresh(refreshToken string) (dto.Tokens, error) {
	const op = "mock_services.UserID.Refresh"

	log := a.log.With(
		slog.String("op", op),
	)
	log.Info("refreshing tokens")

	refreshTokenClaims, err := jwt.ValidateRefreshToken(refreshToken)
	if err != nil {
		log.Error("failed to parse refresh token", sl.Err(err))
		return dto.Tokens{}, err
	}

	userId, err := uuid.Parse(refreshTokenClaims.UserID)
	if err != nil {
		log.Error("failed to parse refresh token ID", sl.Err(err))
		return dto.Tokens{}, apperr.ErrTokenInvalid
	}

	if !a.user.IsExist(userId) {
		log.Error("failed to find user", sl.Err(err))
		return dto.Tokens{}, apperr.ErrTokenInvalid
	}

	tokens, err := a.tokens.refreshTokens(userId)
	if err != nil {
		log.Error("failed to refresh tokens", sl.Err(err))
		return dto.Tokens{}, err
	}

	return tokens, err
}

// Logout TODO added doc string
func (a *Auth) Logout() error {
	const op = "mock_services.UserID.Logout"

	log := a.log.With(slog.String("op", op))
	log.Info("logging out account")

	// TODO implement delete mock_storages

	return nil
}
