// Package database
package database

import (
	"gorm.io/gorm"
	"log/slog"
	"sso/internal/config"
	"sso/internal/database/postgres"
	"sso/internal/database/sqlite"
	apperr "sso/internal/pkg/database/errors"
)

// Driver interfaces of database driver
type Driver interface {
	Connect() (*gorm.DB, error)
	MustConnect() *gorm.DB
	Stop(db *gorm.DB)
}

// New database driver
//
// Panic if provided unknown database name
func New(log *slog.Logger, name config.DatabaseName, dsn string, env config.EnvType) Driver {
	switch name {
	case config.SQLiteDBName:
		return sqlite.New(log, env)
	case config.PostgresDBName:
		return postgres.New(log, dsn, env)
	default:
		panic(apperr.ErrUnknownDriverName.Error())
	}
}
