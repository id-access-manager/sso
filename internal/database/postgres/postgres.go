package postgres

import (
	"fmt"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"log/slog"
	"sso/internal/config"
	"sso/internal/pkg/logger/sl"
)

// Postgres structure for work with postgres database
type Postgres struct {
	dsn    string
	log    *slog.Logger
	driver *gorm.DB
	env    config.EnvType
}

// New Postgres structure
func New(log *slog.Logger, dsn string, env config.EnvType) *Postgres {
	return &Postgres{log: log, dsn: dsn, env: env}
}

// Connect to database
func (p *Postgres) Connect() (*gorm.DB, error) {
	const op = "postgres.Connect"
	var err error

	log := p.log.With(
		slog.String("op", op),
	)

	log.Info("connection to database <Postgres>")

	switch p.env {
	case config.EnvLocal, config.EnvDev:
		p.driver, err = gorm.Open(postgres.Open(p.dsn), &gorm.Config{Logger: logger.Default.LogMode(logger.Info)})
	case config.EnvProd:
		p.driver, err = gorm.Open(postgres.Open(p.dsn), &gorm.Config{Logger: logger.Default.LogMode(logger.Silent)})
	}

	if err != nil {
		log.Error("can not connect to postgres database", sl.Err(err))
		return nil, fmt.Errorf("%s: %w", op, err)
	}
	return p.driver, nil
}

// MustConnect required connection to database
//
// Panic if connection was failed
func (p *Postgres) MustConnect() *gorm.DB {
	_, err := p.Connect()
	if err != nil {
		panic(err)
	}
	return p.driver
}

// Stop database connecting
//
// Graceful stopping database connection
func (p *Postgres) Stop(db *gorm.DB) {
	const op = "postgres.Stop"
	log := p.log.With(
		slog.String("op", op),
	)

	log.Info("stopping connection to database")

	_db, err := db.DB()
	if err != nil {
		log.Error("can not get db from driver", sl.Err(err))
	}

	if err := _db.Close(); err != nil {
		log.Error("can not close driver connection", sl.Err(err))
	}
}
