package sqlite

import (
	"fmt"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"log/slog"
	"sso/internal/config"
	"sso/internal/pkg/logger/sl"
)

// SQLite structure for work with SQLite database
type SQLite struct {
	path string       // path to database
	log  *slog.Logger // object for logging
	env  config.EnvType
}

// New SQLite structure
func New(log *slog.Logger, env config.EnvType) *SQLite {
	return &SQLite{log: log, path: "storage/sso.db", env: env}
}

// Connect to database
func (p *SQLite) Connect() (*gorm.DB, error) {
	const op = "sqlite.Connect"
	var err error
	var db *gorm.DB

	log := p.log.With(
		slog.String("op", op),
	)

	log.Info("connection to database <sqlite>")

	switch p.env {
	case config.EnvLocal, config.EnvDev:
		db, err = gorm.Open(sqlite.Open(p.path), &gorm.Config{Logger: logger.Default.LogMode(logger.Info)})
	case config.EnvProd:
		db, err = gorm.Open(sqlite.Open(p.path), &gorm.Config{Logger: logger.Default.LogMode(logger.Silent)})
	}

	if err != nil {
		log.Error("can not connect to sqlite database", sl.Err(err))
		return nil, fmt.Errorf("%s: %w", op, err)
	}
	return db, nil
}

// MustConnect required connection to database
//
// Panic if connection failed
func (p *SQLite) MustConnect() *gorm.DB {
	db, err := p.Connect()
	if err != nil {
		panic(err)
	}
	return db
}

// Stop connection to database
func (p *SQLite) Stop(db *gorm.DB) {
	const op = "sqlite.Stop"
	log := p.log.With(
		slog.String("op", op),
	)

	log.Info("stopping connection to database")

	_db, err := db.DB()
	if err != nil {
		log.Error("can not get db from driver", sl.Err(err))
	}

	if err := _db.Close(); err != nil {
		log.Error("can not close driver connection", sl.Err(err))
	}
}
