package token

import (
	"github.com/google/uuid"
	"sso/internal/models"
	"sso/internal/pkg/jwt"
	"time"
)

// Access model of access token in app
type Access struct {
	BaseToken
	UserID    uuid.UUID      `gorm:"type:uuid; index:idx_name,unique" json:"user_id"`    // The User for whom the token was issued
	User      models.User    `gorm:"foreignKey: UserID"`                                 // The User object
	SessionID uuid.UUID      `gorm:"type:uuid; index:idx_name,unique" json:"session_id"` // The Session within which the token is valid
	Session   models.Session `gorm:"foreignKey: SessionID"`                              // The Session object
}

// TableName set a name of table in database
func (Access) TableName() string {
	return "access_tokens"
}

// Generate JWT access token
func (a *Access) Generate(ttl time.Duration) error {
	var err error

	a.UpdatedAt = a.CreatedAt
	a.ExpiredAt = a.CreatedAt.Add(ttl)

	a.Token, err = jwt.NewAccessToken(a.User, a.CreatedAt, a.ExpiredAt, a.ID, a.Session.ID)
	if err != nil {
		return err
	}

	return nil
}

// Refresh JWT access token
func (a *Access) Refresh(ttl time.Duration) error {
	var err error

	a.UpdatedAt = time.Now()
	a.ExpiredAt = a.UpdatedAt.Add(ttl)

	a.Token, err = jwt.NewAccessToken(a.User, a.UpdatedAt, a.ExpiredAt, a.ID, a.Session.ID)
	if err != nil {
		return err
	}

	return nil
}
