package token

import (
	"github.com/google/uuid"
	"sso/internal/models"
	"sso/internal/pkg/jwt"
	"time"
)

// Refresh models
type Refresh struct {
	BaseToken
	UserID    uuid.UUID      `gorm:"type:uuid; unique_index:idx_user_id_session_id" json:"user_id"`    // The User for whom the token was issued
	User      models.User    `gorm:"foreignKey: UserID"`                                               // The User object
	SessionID uuid.UUID      `gorm:"type:uuid; unique_index:idx_user_id_session_id" json:"session_id"` // The Session within which the token is valid
	Session   models.Session `gorm:"foreignKey: SessionID"`                                            // The Session object
}

// TableName set a name of table in database
func (Refresh) TableName() string {
	return "refresh_tokens"
}

// Generate JWT refresh token
func (r *Refresh) Generate(ttl time.Duration) (string, error) {
	var err error

	r.ID = uuid.New()
	r.CreatedAt = time.Now()
	r.UpdatedAt = r.CreatedAt
	r.ExpiredAt = r.CreatedAt.Add(ttl)

	r.Token, err = jwt.NewRefreshToken(r.User, r.CreatedAt, r.ExpiredAt, r.ID, r.Session.ID)
	if err != nil {
		return "", err
	}

	return r.Token, nil
}

// Refresh JWT refresh token
func (r *Refresh) Refresh(ttl time.Duration) error {
	var err error

	r.UpdatedAt = time.Now()
	r.ExpiredAt = r.UpdatedAt.Add(ttl)

	r.Token, err = jwt.NewRefreshToken(r.User, r.UpdatedAt, r.ExpiredAt, r.ID, r.Session.ID)
	if err != nil {
		return err
	}

	return nil
}
