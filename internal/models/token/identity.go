package token

import (
	"github.com/google/uuid"
	"sso/internal/models"
	"sso/internal/pkg/jwt"
	"time"
)

// Identity model of identity token in app
type Identity struct {
	BaseToken
	UserID    uuid.UUID      `gorm:"type:uuid; unique_index:idx_user_id_session_id" json:"user_id"`    // The User for whom the token was issued
	User      models.User    `gorm:"foreignKey: UserID"`                                               // The User object
	SessionID uuid.UUID      `gorm:"type:uuid; unique_index:idx_user_id_session_id" json:"session_id"` // The Session within which the token is valid
	Session   models.Session `gorm:"foreignKey: SessionID"`                                            // The Session object
}

// TableName set a name of table in database
func (Identity) TableName() string {
	return "identity_tokens"
}

// Generate JWT identity token
func (i *Identity) Generate(ttl time.Duration) error {
	var err error

	i.ID = uuid.New()
	i.CreatedAt = time.Now()
	i.UpdatedAt = i.CreatedAt
	i.ExpiredAt = i.CreatedAt.Add(ttl)

	i.Token, err = jwt.NewAccessToken(i.User, i.CreatedAt, i.ExpiredAt, i.ID, i.Session.ID)
	if err != nil {
		return err
	}

	return nil
}

// Refresh JWT identity token
func (i *Identity) Refresh(ttl time.Duration) error {
	var err error

	i.UpdatedAt = time.Now()
	i.ExpiredAt = i.UpdatedAt.Add(ttl)

	i.Token, err = jwt.NewAccessToken(i.User, i.UpdatedAt, i.ExpiredAt, i.ID, i.Session.ID)
	if err != nil {
		return err
	}

	return nil
}
