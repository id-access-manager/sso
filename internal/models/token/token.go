package token

import (
	"github.com/google/uuid"
	"gorm.io/gorm"
	"time"
)

// BaseToken is a basic model for tokens in app
type BaseToken struct {
	ID        uuid.UUID `gorm:"primaryKey; type:uuid" json:"id"`             // Unique uuid.UUID ID
	Token     string    `gorm:"type:text"`                                   // Generated token
	CreatedAt time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"created_at"` // Date and Time of object create
	UpdatedAt time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"updated_at"` // Date and time when token was updated
	ExpiredAt time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"expired_at"` // Date and time when token expired
}

// BeforeCreate generate unique row uuid.UUID ID
func (m *BaseToken) BeforeCreate(tx *gorm.DB) (err error) {
	m.ID = uuid.New()
	m.CreatedAt = time.Now()
	m.UpdatedAt = time.Now()
	return nil
}
