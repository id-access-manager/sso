package models

import (
	"github.com/google/uuid"
	"sso/internal/pkg/database/models"
)

// Permission is a model of permission for Object with Action
type Permission struct {
	models.SQLModel
	Name     string    `json:"name" gorm:"type: varchar(127)"`             // Name of Permission
	ObjectID uuid.UUID `json:"-" gorm:"unique"`                            // Object id in Permission
	Object   Object    `json:"object" gorm:"foreignKey:ObjectID" json:"-"` // Object instance (not for database)
	ActionID uuid.UUID `json:"-" gorm:"unique"`                            // Action for Object in Permission
	Action   Action    `json:"action" gorm:"foreignKey:ActionID" json:"-"` // Action instance (not for database)
}
