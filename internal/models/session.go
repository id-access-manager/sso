package models

import (
	"github.com/google/uuid"
	"gorm.io/gorm"
	"time"
)

// Session is a model of User session in app
type Session struct {
	ID        uuid.UUID `gorm:"primaryKey; type:uuid" json:"id"`                // Unique uuid.UUID ID
	CreatedAt time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"created_at"`    // Date and Time of object create
	UpdatedAt time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"updated_at"`    // Date and time when Session was updated
	ExpiredAt time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"expired_at"`    // Date and time when session expired
	UserID    uuid.UUID `gorm:"type:uuid" json:"user_id"`                       // The user id who owns the session
	User      User      `gorm:"foreignKey:UserID; OnDelete:SET NULL;" json:"-"` // The User who owns the session
}

// BeforeCreate generate unique row uuid.UUID ID
func (s *Session) BeforeCreate(tx *gorm.DB) (err error) {
	s.ID = uuid.New()
	s.CreatedAt = time.Now()
	return nil
}

func (s *Session) BeforeUpdate(tx *gorm.DB) error {
	s.UpdatedAt = time.Now()
	return nil
}
