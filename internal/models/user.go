package models

import (
	"golang.org/x/crypto/bcrypt"
	"sso/internal/pkg/database/models"
)

// Email is a type of email with custom validation
type Email string

// User is a model of user in app
type User struct {
	models.SQLModel
	Email         Email      `json:"email" gorm:"unique_index" json:"email"`       // Unique User Email
	Username      string     `json:"username" gorm:"unique_index" json:"username"` // Unique Username
	Password      []byte     `json:"-" gorm:"" json:"-"`                           // Encrypted User Password
	IsTmpPassword bool       `json:"-" gorm:"default: false" json:"-"`             // Is a one-time Password set for the User
	Resources     []Resource `json:"resources" gorm:"many2many:resource_user;"`    // Resource for this User
	Roles         []Role     `json:"roles" gorm:"many2many:role_user;"`            // Roles for this User
}

// Validate email address
func (e *Email) Validate() error {
	return nil
}

// String convert Email type to string type
func (e *Email) String() string {
	return string(*e)
}

// SetPassword encrypt and set password for User`s object
func (u *User) SetPassword(password string) (err error) {
	u.Password, err = bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	return err
}

// CheckPassword check compare of User`s object password and newPassword
func (u *User) CheckPassword(password string) error {
	return bcrypt.CompareHashAndPassword(u.Password, []byte(password))
}
