package models

import (
	"sso/internal/pkg/database/models"
)

// Role is a model of a user role in app
type Role struct {
	models.SQLModel
	Name        string       `json:"name" gorm:"type:varchar(50); not null"`        // Name of Role
	Resources   []Resource   `json:"-" gorm:"many2many:role_resource;"`             // List of Resource for this Role
	Users       []User       `json:"-" gorm:"many2many:role_user;"`                 // Users with this Role
	Permissions []Permission `json:"permissions" gorm:"many2many:permission_role;"` // List of Permission for Role
}
