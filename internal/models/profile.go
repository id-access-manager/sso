package models

import (
	"github.com/google/uuid"
	"sso/internal/pkg/database/models"
)

// Gender type for gender enum in Profile
type Gender string

// Value for Gender type in Profile
var (
	MaleGender   Gender = "male"   // Male Gender enum in Profile model
	FemaleGender Gender = "female" // Female Gender enum in Profile model
)

// Profile sql model for database
type Profile struct {
	models.SQLModel
	FirstName  string    `gorm:"type: varchar(128); not null" json:"first_name"` // Name
	LastName   string    `gorm:"type: varchar(128); not null" json:"last_name"`  // Last name
	MiddleName string    `gorm:"type: varchar(128)" json:"middle_name"`          // Middle name
	Gender     Gender    `gorm:"type: gender; not null" json:"gender"`           // Gender
	Phone      string    `gorm:"type: varchar(11); unique" json:"phone"`         // Phone
	UserID     uuid.UUID `gorm:"unique" json:"user_id"`                          // User id from auth service
	User       User      `gorm:"foreignKey:UserID" json:"-"`                     // User instance (not for database)
}
