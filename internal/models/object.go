package models

import (
	"sso/internal/pkg/database/models"
)

// Object is a model witch control with app
type Object struct {
	models.SQLModel
	Name string `json:"name" gorm:"type: varchar(127)"` // Name of Object ('user', 'profile')
}
