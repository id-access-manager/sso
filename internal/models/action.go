package models

import (
	apperr "sso/internal/consts/errors"
	"sso/internal/pkg/database/models"
)

// ActionType is a type of action with Object in app
type ActionType string

var (
	CreateAction ActionType = "create" // Create object action in Action model
	ReadAction   ActionType = "read"   // Read object action in Action model
	UpdateAction ActionType = "update" // Update object action in Action model
	DeleteAction ActionType = "delete" // Delete object action in Action model
)

// Action is a model of ActionType with Object in app
type Action struct {
	models.SQLModel
	Name       string     `json:"name" gorm:"type: varchar(127)"` // Name of Action
	ActionType ActionType `json:"action" gorm:"column: action"`   // Action name can do with Object
}

func (a *ActionType) Validate() error {
	switch *a {
	case CreateAction, ReadAction, UpdateAction, DeleteAction:
		return nil
	default:
		return apperr.ErrUnknownActionType
	}
}
