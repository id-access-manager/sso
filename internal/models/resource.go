package models

import (
	"sso/internal/pkg/database/models"
)

// Resource is a model of a registered Resource in app
type Resource struct {
	models.SQLModel
	Name        string `json:"name" gorm:"not null; unique"`          // Name of Resource
	Description string `json:"description" gorm:"type:text"`          // Resource description
	Domain      string `json:"domain" gorm:"not null; unique"`        // Resource domain
	Users       []User `json:"-" gorm:"many2many:resource_user;"`     // Users in this Resource
	Roles       []Role `json:"roles" gorm:"many2many:role_resource;"` // List of Role for this Resource
}
