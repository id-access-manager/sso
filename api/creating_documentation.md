# Создание API документации

Для генерации используется библиотеки [swag](https://github.com/swaggo/swag?tab=readme-ov-file)

## Генерация документации

```shell
swag init -g internal/app/mock_services/app.go
```

## Генерация Swagger API документации для API

```shell
swag init -o ./api -g internal/transport/mock_services/*.go
```

## Отформатировать строки с документацией с сгененрировать документацию

```shell
swag fmt && swag init -o ./api -g internal/transport/http/*.go
```
