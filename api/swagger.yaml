definitions:
  http.actionRequest:
    properties:
      action:
        type: string
      name:
        type: string
    required:
    - action
    - name
    type: object
  http.loginRequest:
    properties:
      password:
        type: string
      username:
        type: string
    type: object
  http.objectRequest:
    properties:
      name:
        type: string
    type: object
  http.permissionRequest:
    properties:
      action_id:
        type: string
      name:
        type: string
      object_id:
        type: string
    type: object
  http.refreshRequest:
    properties:
      refresh:
        type: string
    type: object
  http.registerRequest:
    properties:
      email:
        type: string
      password:
        type: string
      username:
        type: string
    type: object
  http.resourceRequest:
    properties:
      description:
        type: string
      name:
        type: string
      roles:
        items:
          type: string
        type: array
    type: object
  http.roleRequest:
    properties:
      name:
        type: string
      permissions:
        items:
          type: string
        type: array
    type: object
  models.Action:
    properties:
      action:
        description: Action name can do with Object
        type: string
      id:
        description: Unique uuid.UUID ID
        type: string
      name:
        description: Name of Action
        type: string
    type: object
  models.Object:
    properties:
      id:
        description: Unique uuid.UUID ID
        type: string
      name:
        description: Name of Object ('user', 'profile')
        type: string
    type: object
  models.Permission:
    properties:
      action:
        allOf:
        - $ref: '#/definitions/models.Action'
        description: Action instance (not for database)
      id:
        description: Unique uuid.UUID ID
        type: string
      name:
        description: Name of Permission
        type: string
      object:
        allOf:
        - $ref: '#/definitions/models.Object'
        description: Object instance (not for database)
    type: object
  models.Resource:
    properties:
      description:
        description: Resource description
        type: string
      id:
        description: Unique uuid.UUID ID
        type: string
      name:
        description: Name of Resource
        type: string
      roles:
        description: List of Role for this Resource
        items:
          $ref: '#/definitions/models.Role'
        type: array
    type: object
  models.Role:
    properties:
      id:
        description: Unique uuid.UUID ID
        type: string
      name:
        description: Name of Role
        type: string
      permissions:
        description: List of Permission for Role
        items:
          $ref: '#/definitions/models.Permission'
        type: array
    type: object
  models.User:
    properties:
      email:
        description: Unique User Email
        type: string
      id:
        description: Unique uuid.UUID ID
        type: string
      resources:
        description: Resource for this User
        items:
          $ref: '#/definitions/models.Resource'
        type: array
      roles:
        description: Roles for this User
        items:
          $ref: '#/definitions/models.Role'
        type: array
      username:
        description: Unique Username
        type: string
    type: object
  token.Tokens:
    properties:
      access:
        type: string
      identity:
        type: string
      refresh:
        type: string
    type: object
  utils.HTTPError:
    properties:
      message:
        example: status bad request
        type: string
    type: object
info:
  contact: {}
paths:
  /admin/action/:
    post:
      consumes:
      - application/json
      description: Создание действия
      parameters:
      - description: Action
        in: body
        name: Action
        required: true
        schema:
          $ref: '#/definitions/http.actionRequest'
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/models.Action'
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/utils.HTTPError'
        "404":
          description: Not Found
          schema:
            $ref: '#/definitions/utils.HTTPError'
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/utils.HTTPError'
      summary: Создание действия
      tags:
      - action
  /admin/action/{action_id}:
    delete:
      consumes:
      - application/json
      description: Создание действия
      parameters:
      - description: Action ID
        in: path
        name: action_id
        required: true
        type: string
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/models.Action'
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/utils.HTTPError'
        "404":
          description: Not Found
          schema:
            $ref: '#/definitions/utils.HTTPError'
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/utils.HTTPError'
      summary: Удалить действие
      tags:
      - action
    get:
      consumes:
      - application/json
      description: Получение действия по иентификатору
      parameters:
      - description: Action ID
        in: path
        name: action_id
        required: true
        type: string
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/models.Action'
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/utils.HTTPError'
        "404":
          description: Not Found
          schema:
            $ref: '#/definitions/utils.HTTPError'
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/utils.HTTPError'
      summary: Получить действие
      tags:
      - action
    patch:
      consumes:
      - application/json
      description: Создание действия по идентификатору
      parameters:
      - description: Action ID
        in: path
        name: action_id
        required: true
        type: string
      - description: Action
        in: body
        name: Action
        required: true
        schema:
          $ref: '#/definitions/http.actionRequest'
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/models.Action'
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/utils.HTTPError'
        "404":
          description: Not Found
          schema:
            $ref: '#/definitions/utils.HTTPError'
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/utils.HTTPError'
      summary: Обновить действия
      tags:
      - action
  /admin/action/list:
    get:
      consumes:
      - application/json
      description: Создание действия
      parameters:
      - description: Page Number
        in: query
        name: page
        type: integer
      - description: Page Size
        in: query
        name: page_size
        type: integer
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/models.Action'
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/utils.HTTPError'
        "404":
          description: Not Found
          schema:
            $ref: '#/definitions/utils.HTTPError'
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/utils.HTTPError'
      summary: Получить все действия
      tags:
      - action
  /admin/object/:
    post:
      consumes:
      - application/json
      parameters:
      - description: Object
        in: body
        name: object
        required: true
        schema:
          $ref: '#/definitions/http.objectRequest'
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/models.Object'
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/utils.HTTPError'
        "404":
          description: Not Found
          schema:
            $ref: '#/definitions/utils.HTTPError'
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/utils.HTTPError'
      summary: Создать объект
      tags:
      - object
  /admin/object/{obj_id}:
    delete:
      consumes:
      - application/json
      parameters:
      - description: Object ID
        in: path
        name: obj_id
        required: true
        type: string
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/models.Object'
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/utils.HTTPError'
        "404":
          description: Not Found
          schema:
            $ref: '#/definitions/utils.HTTPError'
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/utils.HTTPError'
      summary: Удалить объект
      tags:
      - object
    get:
      consumes:
      - application/json
      parameters:
      - description: Object ID
        in: path
        name: obj_id
        required: true
        type: string
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/models.Object'
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/utils.HTTPError'
        "404":
          description: Not Found
          schema:
            $ref: '#/definitions/utils.HTTPError'
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/utils.HTTPError'
      summary: Получить объект
      tags:
      - object
    patch:
      consumes:
      - application/json
      parameters:
      - description: Object ID
        in: path
        name: obj_id
        required: true
        type: string
      - description: Object
        in: body
        name: object
        required: true
        schema:
          $ref: '#/definitions/http.objectRequest'
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/models.Object'
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/utils.HTTPError'
        "404":
          description: Not Found
          schema:
            $ref: '#/definitions/utils.HTTPError'
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/utils.HTTPError'
      summary: Обновить объект
      tags:
      - object
  /admin/object/list:
    get:
      consumes:
      - application/json
      parameters:
      - description: Page size
        in: query
        name: limit
        type: integer
      - description: Page
        in: query
        name: page
        type: integer
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/models.Object'
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/utils.HTTPError'
        "404":
          description: Not Found
          schema:
            $ref: '#/definitions/utils.HTTPError'
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/utils.HTTPError'
      summary: Получить список объектов
      tags:
      - object
  /admin/permission/:
    post:
      consumes:
      - application/json
      parameters:
      - description: Permission
        in: body
        name: permission
        required: true
        schema:
          $ref: '#/definitions/http.permissionRequest'
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/models.Permission'
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/utils.HTTPError'
        "404":
          description: Not Found
          schema:
            $ref: '#/definitions/utils.HTTPError'
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/utils.HTTPError'
      summary: Получить разрешение
      tags:
      - permission
  /admin/permission/{permission_id}:
    patch:
      consumes:
      - application/json
      parameters:
      - description: Permission ID
        in: path
        name: permission_id
        required: true
        type: string
      - description: Permission
        in: body
        name: permission
        required: true
        schema:
          $ref: '#/definitions/http.permissionRequest'
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/models.Permission'
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/utils.HTTPError'
        "404":
          description: Not Found
          schema:
            $ref: '#/definitions/utils.HTTPError'
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/utils.HTTPError'
      summary: Получить разрешение
      tags:
      - permission
  /admin/permission/list:
    get:
      consumes:
      - application/json
      parameters:
      - description: Page number
        in: query
        name: page
        required: true
        type: string
      - description: Page Size
        in: query
        name: page_size
        required: true
        type: string
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/models.Permission'
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/utils.HTTPError'
        "404":
          description: Not Found
          schema:
            $ref: '#/definitions/utils.HTTPError'
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/utils.HTTPError'
      summary: Получить разрешение
      tags:
      - permission
  /admin/permission{permission_id}:
    delete:
      consumes:
      - application/json
      parameters:
      - description: Object ID
        in: path
        name: permission_id
        required: true
        type: string
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/models.Permission'
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/utils.HTTPError'
        "404":
          description: Not Found
          schema:
            $ref: '#/definitions/utils.HTTPError'
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/utils.HTTPError'
      summary: Удалить разрешение
      tags:
      - permission
    get:
      consumes:
      - application/json
      parameters:
      - description: Object ID
        in: path
        name: permission_id
        required: true
        type: string
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/models.Permission'
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/utils.HTTPError'
        "404":
          description: Not Found
          schema:
            $ref: '#/definitions/utils.HTTPError'
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/utils.HTTPError'
      summary: Получить разрешение
      tags:
      - permission
  /admin/resource/:
    post:
      consumes:
      - application/json
      parameters:
      - description: Resource
        in: body
        name: resource
        required: true
        schema:
          $ref: '#/definitions/http.resourceRequest'
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/models.Permission'
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/utils.HTTPError'
        "404":
          description: Not Found
          schema:
            $ref: '#/definitions/utils.HTTPError'
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/utils.HTTPError'
      summary: Создать ресурс
      tags:
      - resource
  /admin/resource/{resource_id}:
    delete:
      consumes:
      - application/json
      parameters:
      - description: Resource ID
        in: path
        name: resource_id
        required: true
        type: string
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/models.Permission'
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/utils.HTTPError'
        "404":
          description: Not Found
          schema:
            $ref: '#/definitions/utils.HTTPError'
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/utils.HTTPError'
      summary: Удалить ресурс
      tags:
      - resource
    get:
      consumes:
      - application/json
      parameters:
      - description: Resource ID
        in: path
        name: resource_id
        required: true
        type: string
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/models.Permission'
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/utils.HTTPError'
        "404":
          description: Not Found
          schema:
            $ref: '#/definitions/utils.HTTPError'
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/utils.HTTPError'
      summary: Получить ресурс
      tags:
      - resource
    patch:
      consumes:
      - application/json
      parameters:
      - description: Resource ID
        in: path
        name: resource_id
        required: true
        type: string
      - description: Resource
        in: body
        name: resource
        required: true
        schema:
          $ref: '#/definitions/http.resourceRequest'
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/models.Resource'
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/utils.HTTPError'
        "404":
          description: Not Found
          schema:
            $ref: '#/definitions/utils.HTTPError'
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/utils.HTTPError'
      summary: Обновить ресурс
      tags:
      - resource
  /admin/resource/list:
    get:
      consumes:
      - application/json
      parameters:
      - description: Page number
        in: query
        name: page
        required: true
        type: string
      - description: Page Size
        in: query
        name: page_size
        required: true
        type: string
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/models.Permission'
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/utils.HTTPError'
        "404":
          description: Not Found
          schema:
            $ref: '#/definitions/utils.HTTPError'
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/utils.HTTPError'
      summary: Получить список ресурсов
      tags:
      - resource
  /admin/role/:
    post:
      consumes:
      - application/json
      parameters:
      - description: Role
        in: body
        name: role
        required: true
        schema:
          $ref: '#/definitions/http.roleRequest'
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/models.Role'
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/utils.HTTPError'
        "404":
          description: Not Found
          schema:
            $ref: '#/definitions/utils.HTTPError'
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/utils.HTTPError'
      summary: Создать роль
      tags:
      - role
  /admin/role/{role_id}:
    delete:
      consumes:
      - application/json
      parameters:
      - description: Role ID
        in: path
        name: role_id
        required: true
        type: string
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/models.Role'
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/utils.HTTPError'
        "404":
          description: Not Found
          schema:
            $ref: '#/definitions/utils.HTTPError'
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/utils.HTTPError'
      summary: Удалить роль
      tags:
      - role
    get:
      consumes:
      - application/json
      parameters:
      - description: Role ID
        in: path
        name: role_id
        required: true
        type: string
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/models.Role'
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/utils.HTTPError'
        "404":
          description: Not Found
          schema:
            $ref: '#/definitions/utils.HTTPError'
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/utils.HTTPError'
      summary: Получить роль
      tags:
      - role
    patch:
      consumes:
      - application/json
      parameters:
      - description: Role
        in: body
        name: role
        required: true
        schema:
          $ref: '#/definitions/http.roleRequest'
      - description: Role ID
        in: path
        name: role_id
        required: true
        type: string
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/models.Role'
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/utils.HTTPError'
        "404":
          description: Not Found
          schema:
            $ref: '#/definitions/utils.HTTPError'
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/utils.HTTPError'
      summary: Обновить роль
      tags:
      - role
  /admin/role/list:
    get:
      consumes:
      - application/json
      parameters:
      - description: Page number
        in: query
        name: page
        required: true
        type: string
      - description: Page Size
        in: query
        name: page_size
        required: true
        type: string
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/models.Role'
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/utils.HTTPError'
        "404":
          description: Not Found
          schema:
            $ref: '#/definitions/utils.HTTPError'
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/utils.HTTPError'
      summary: Получить список ролей
      tags:
      - role
  /admin/user/:
    post:
      consumes:
      - application/json
      deprecated: true
      description: Не реализовано!!!
      parameters:
      - description: User
        in: body
        name: user
        required: true
        schema:
          $ref: '#/definitions/http.roleRequest'
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/models.Role'
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/utils.HTTPError'
        "404":
          description: Not Found
          schema:
            $ref: '#/definitions/utils.HTTPError'
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/utils.HTTPError'
      summary: Создать пользователя
      tags:
      - user
  /admin/user/{role_id}:
    patch:
      consumes:
      - application/json
      deprecated: true
      description: Не реализовано!!!
      parameters:
      - description: Role
        in: body
        name: user
        required: true
        schema:
          $ref: '#/definitions/http.roleRequest'
      - description: Role ID
        in: path
        name: user_id
        required: true
        type: string
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/models.Role'
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/utils.HTTPError'
        "404":
          description: Not Found
          schema:
            $ref: '#/definitions/utils.HTTPError'
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/utils.HTTPError'
      summary: Обновить пользователя
      tags:
      - user
  /admin/user/{user_id}:
    delete:
      consumes:
      - application/json
      deprecated: true
      description: Не реализовано!!!
      parameters:
      - description: Role ID
        in: path
        name: user_id
        required: true
        type: string
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/models.Role'
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/utils.HTTPError'
        "404":
          description: Not Found
          schema:
            $ref: '#/definitions/utils.HTTPError'
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/utils.HTTPError'
      summary: Удалить пользователя
      tags:
      - user
    get:
      consumes:
      - application/json
      parameters:
      - description: User ID
        in: path
        name: user_id
        required: true
        type: string
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/models.Role'
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/utils.HTTPError'
        "404":
          description: Not Found
          schema:
            $ref: '#/definitions/utils.HTTPError'
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/utils.HTTPError'
      summary: Получить пользователя
      tags:
      - user
  /admin/user/list:
    get:
      consumes:
      - application/json
      parameters:
      - description: Page number
        in: query
        name: page
        required: true
        type: string
      - description: Page Size
        in: query
        name: page_size
        required: true
        type: string
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/models.Role'
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/utils.HTTPError'
        "404":
          description: Not Found
          schema:
            $ref: '#/definitions/utils.HTTPError'
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/utils.HTTPError'
      summary: Получить список пользователей
      tags:
      - user
  /api/auth/:
    post:
      consumes:
      - application/json
      parameters:
      - description: Login Data
        in: body
        name: user
        required: true
        schema:
          $ref: '#/definitions/http.loginRequest'
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/token.Tokens'
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/utils.HTTPError'
        "404":
          description: Not Found
          schema:
            $ref: '#/definitions/utils.HTTPError'
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/utils.HTTPError'
      summary: Вход пользователя
      tags:
      - auth
  /api/auth/register:
    post:
      consumes:
      - application/json
      parameters:
      - description: Refresh Data
        in: body
        name: user
        required: true
        schema:
          $ref: '#/definitions/http.refreshRequest'
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/token.Tokens'
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/utils.HTTPError'
        "404":
          description: Not Found
          schema:
            $ref: '#/definitions/utils.HTTPError'
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/utils.HTTPError'
      summary: Обновление токенов
      tags:
      - auth
  /api/user/register:
    post:
      consumes:
      - application/json
      parameters:
      - description: User
        in: body
        name: user
        required: true
        schema:
          $ref: '#/definitions/http.registerRequest'
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/models.User'
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/utils.HTTPError'
        "404":
          description: Not Found
          schema:
            $ref: '#/definitions/utils.HTTPError'
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/utils.HTTPError'
      summary: Регистрация пользователя
      tags:
      - user
swagger: "2.0"
